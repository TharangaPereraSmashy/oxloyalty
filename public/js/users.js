$(document).ready(function() {

    $('#btnToggleSlideUpSize').click(function() {
        $('#modalSlideUp').modal('show')
    });

    $('#btn-add-permission').click(function() {
        $('#add-permission-modal').modal('show')
    });

});

function editUserProfile(id) {
    jQuery.ajax({
        url: '/profile/get_profile_by_id/'+id,
        type: 'GET',
        dataType: "json",
        success: function(data){
            $('#edit_description').val(data.description);
            $('#edit_type').val(data.type);
            $('#edit_id').val(data.id);
            $('#edit_profile').modal('show')
        }
    });
}

function editUserPermission(id, pageDesc) {
    jQuery.ajax({
        url: '/permission/get_permission_by_id/'+id,
        type: 'GET',
        dataType: "json",
        success: function(data) {
            $('#id').val(data.id);
            $("#page_desc").html(pageDesc);
            $('#pageId').val(data.pageId);
            $("#edit-create").prop( "checked", data.create );
            $("#edit-read").prop( "checked", data.read );
            $("#edit-update").prop( "checked", data.update );
            $("#edit-delete").prop( "checked", data.delete );
            $('#edit-permission-modal').modal('show')
        }
    });
}