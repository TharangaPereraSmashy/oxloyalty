jQuery(document).ready(function(){

    jQuery('select[name="country_id"]').change(function(){

        var country = jQuery('select[name="country_id"]').val();

        getStatesByCountry(country);

    });

    jQuery('select[name="type"]').change(function(){

        var type = jQuery('select[name="type"]').val();

        getProfilesByType(type);

    });

    jQuery('.alert[data-remove-delay]').each(function () {
       var slideUpDuration = $(this).data('slideup-duration') ?  parseInt($(this).data('slideup-duration')) : 2000;
       $(this).delay(parseInt($(this).data('remove-delay'))).slideUp(slideUpDuration);
    });

    $(document).on('keypress', '.int-only', function (){
        var r=/^[1-9]\d*$/;
        var v=String.fromCharCode(event.charCode);
        if(v!=0){if(!r.test(v)&&(event.charCode!=0))return false;}
    });

    $(document).on('keypress', '.float-only', function (){
        var r=/^[0-9]+\.?[0-9]*$/;
        var v=this.value+String.fromCharCode(event.charCode);
        if(!r.test(v)&&(event.charCode!=0))return false;
    });

    $(document).on('keypress', '.int-plus-minus-only', function (){
        var r=/^[-+]?\d*$/;
        var v=this.value+String.fromCharCode(event.charCode);
        if(!r.test(v)&&(event.charCode!=0))return false;
    });

    $(document).on('keypress', '.no-spacecat', function (){
        var r=/^[A-Za-z0-9]*$/;
        var v=this.value+String.fromCharCode(event.charCode);
        if(!r.test(v)&&(event.charCode!=0))return false;
    });

});


function getStatesByCountry(country){

    jQuery('select[name="state_id"]').find('option').remove().end().append('<option value="">Select State</option>').val('');

    jQuery.ajax({
        url: '/cemeteries/get_states_by_country/'+country,
        type: 'GET',
        dataType: "json",
        success: function(data){
            $.each(data, function(key, value) {
                jQuery('select[name="state_id"]').append(jQuery('<option value="'+key+'">'+value+'</option>', {
                }));
            });
        }
    });

}

function getProfilesByType(type){

    jQuery('select[name="profile"]').find('option').remove().end().append('<option value="">Select Profile</option>').val('');

    jQuery.ajax({
        url: '/registeruser/get_profiles_by_type/'+type,
        type: 'GET',
        dataType: "json",
        success: function(data){
            $.each(data, function(key, value) {
                jQuery('select[name="profile"]').append(jQuery('<option value="'+key+'">'+value+'</option>', {
                }));
            });
        }
    });

}