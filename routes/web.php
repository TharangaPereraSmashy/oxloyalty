<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/registeruser/get_profiles_by_type/{id}', 'RegisterUserController@getProfilesByType');

Route::resource('registeruser','RegisterUserController');

Route::resource('profile','UserProfileController');

Route::get('profile/get_profile_by_id/{id}', 'UserProfileController@getProfileById');

Route::resource('profile','UserProfileController');


// Start Change Password Routes

Route::get('/changePassword','HomeController@showChangePasswordForm');

Route::post('/changePassword','HomeController@changePassword')->name('changePassword');

// End Change Password Routes


// Start Cemetery Module Routes

Route::get('/cemeteries/get_states_by_country/{id}', 'CemeteryController@getStatesByCountry');

Route::get('/cemeteries/search_cemeteries', 'CemeteryController@searchCemeteries');

Route::post('/cemeteries/search_cemeteries', 'CemeteryController@searchCemeteries');

Route::resource('cemeteries','CemeteryController');

// End Cemetery Module Routes


// Start Item Module Routes

Route::get('/items/import_export_items_view', 'ItemController@importExportItemsView')->name('import.export.view');

Route::post('/items/import_items', 'ItemController@importItems')->name('import.items');

Route::post('/items/select_sheet', 'ItemController@selectSheetForImport')->name('select.sheet');

Route::post('/items/start_import_items', 'ItemController@startImportItems')->name('start.import.items');

Route::get('/items/export_items/{type}', 'ItemController@exportItems')->name('export.items');

Route::get('/items/search_items', 'ItemController@searchItems');

Route::post('/items/search_items', 'ItemController@searchItems');

Route::resource('items','ItemController');

// End Item Module Routes


// Start Special Offer Module Routes

Route::get('/special_offers/search_special_offers', 'SpecialOfferController@searchSpecialOffers');

Route::post('/special_offers/search_special_offers', 'SpecialOfferController@searchSpecialOffers');

Route::get('/special_offers/create_special_offer/{item}', ['uses' =>'SpecialOfferController@createSpecialOfferByItem'])->name('create_special_offer_by_item');

Route::post('/special_offers/store_special_offer/{item}', ['uses' =>'SpecialOfferController@storeSpecialOfferByItem'])->name('store_special_offer_by_item');

Route::get('/special_offers/edit_special_offer/{id}/{item}', ['uses' =>'SpecialOfferController@editSpecialOfferByItem'])->name('edit_special_offer_by_item');

Route::put('/special_offers/update_special_offer/{id}/{item}', ['uses' =>'SpecialOfferController@updateSpecialOfferByItem'])->name('update_special_offer_by_item');

Route::delete('/special_offers/destroy_special_offer/{id}/{item}', ['uses' =>'SpecialOfferController@destroySpecialOfferByItem'])->name('destroy_special_offer_by_item');

Route::resource('special_offers','SpecialOfferController');

// End Special Offer Module Routes


// Start Cemetery Item Module Routes

Route::get('/cemetery_items/import_export_loyalty_items_view', 'CemeteryItemController@importExportLoyaltyItemsView')->name('import.export.loyalty.items.view');

Route::post('/cemetery_items/import_loyalty_items', 'CemeteryItemController@importLoyaltyItems')->name('import.loyalty.items');

Route::post('/cemetery_items/select_sheet_loyalty_items', 'CemeteryItemController@selectSheetForLoyaltyItemsImport')->name('select.sheet.loyalty.items');

Route::post('/cemetery_items/start_import_loyalty_items', 'CemeteryItemController@startImportLoyaltyItems')->name('start.import.loyalty.items');

Route::get('/cemetery_items/export_items/{type}', 'CemeteryItemController@exportLoyaltyItems')->name('export.loyalty.items');

Route::get('/cemetery_items/search_cemetery_items', 'CemeteryItemController@searchCemeteryItems');

Route::post('/cemetery_items/search_cemetery_items', 'CemeteryItemController@searchCemeteryItems');

Route::get('/cemetery_items/create_cemetery_item/{item}', ['uses' =>'CemeteryItemController@createCemeteryItemByItem'])->name('create_cemetery_item_by_item');

Route::post('/cemetery_items/store_cemetery_item/{item}', ['uses' =>'CemeteryItemController@storeCemeteryItemByItem'])->name('store_cemetery_item_by_item');

Route::get('/cemetery_items/edit_cemetery_item/{id}/{item}', ['uses' =>'CemeteryItemController@editCemeteryItemByItem'])->name('edit_cemetery_item_by_item');

Route::put('/cemetery_items/update_cemetery_item/{id}/{item}', ['uses' =>'CemeteryItemController@updateCemeteryItemByItem'])->name('update_cemetery_item_by_item');

Route::delete('/cemetery_items/destroy_cemetery_item/{id}/{item}', ['uses' =>'CemeteryItemController@destroyCemeteryItemByItem'])->name('destroy_cemetery_item_by_item');

Route::resource('cemetery_items','CemeteryItemController');

// End Cemetery Item Module Routes


// Start Permission Module Routes

Route::get('permission/get_permission_by_id/{id}', 'UserPermissionController@getPermissionById');

Route::resource('permission','UserPermissionController');

// End Permission Module Routes


// Start Funeral Director Module Routes

Route::post('/funeral_directors/delete_loyalty_dollar_revaluation_request/{id}', 'FuneralDirectorController@deleteLoyaltyDollarRevaluationRequest')->name('delete.revaluation.request');

Route::post('/funeral_directors/update_loyalty_dollar_revaluation_request/{id}', 'FuneralDirectorController@updateLoyaltyDollarRevaluationRequest')->name('update.revaluation.request');

Route::get('/funeral_directors/loyalty_dollar_revaluation_requests', 'FuneralDirectorController@loyaltyDollarRevaluationRequests');

Route::post('/funeral_directors/create_loyalty_dollar_revaluation_request', 'FuneralDirectorController@createLoyaltyDollarRevaluationRequest')->name('create.revaluation.request');

Route::get('/funeral_directors/funeral_director_welcome_page', 'FuneralDirectorController@viewFuneralDirectorWelcomePage');

Route::post('/funeral_directors/approve_ld_transfer_request/{id}', 'FuneralDirectorController@approveLoyaltyDollarTransferRequest');

Route::post('/funeral_directors/reject_ld_transfer_request/{id}', 'FuneralDirectorController@rejectLoyaltyDollarTransferRequest');

Route::get('/funeral_directors/get_ld_balance_by_fd_id/{id}', 'FuneralDirectorController@getLdBalanceByFdId');

Route::get('/funeral_directors/search_loyalty_dollar_transfer_requests', 'FuneralDirectorController@searchLoyaltyDollarTransferRequests');

Route::post('/funeral_directors/search_loyalty_dollar_transfer_requests', 'FuneralDirectorController@searchLoyaltyDollarTransferRequests');

Route::get('/funeral_directors/search_funeral_directors', 'FuneralDirectorController@searchFuneralDirectors');

Route::post('/funeral_directors/search_funeral_directors', 'FuneralDirectorController@searchFuneralDirectors');

Route::post('/funeral_directors/set_loyalty_dollar_adjustment', 'FuneralDirectorController@setLoyaltyDollarAdjustment');

Route::post('/funeral_directors/create_loyalty_dollar_transfer_request', 'FuneralDirectorController@createLoyaltyDollarTransferRequest');

Route::resource('funeral_directors','FuneralDirectorController');

// End Funeral Director Module Routes


// Start Staff Module Routes

Route::get('/staff/staff_welcome_page', 'StaffController@viewStaffWelcomePage');

Route::get('/staff/get_loyalty_dollar_by_fd_id/{id}/{type}', 'StaffController@getLoyaltyDollarByFdId');

Route::get('/staff/get_points_by_cemetery_item/{id}/{type}/{cid}/{iid}', 'StaffController@getPointsByCemeteryItem');

Route::get('/staff/get_items_by_cemetery/{id}', 'StaffController@getItemsByCemetery');

// End Staff Module Routes


// Start Audit Module Routes

Route::get('/audits/search_audits', 'AuditController@searchAudits');

Route::post('/audits/search_audits', 'AuditController@searchAudits');

Route::resource('audits','AuditController')->only([
    'index', 'show'
]);

// End Audit Module Routes


// Start Point Report Module Routes

Route::get('/point_reports/point_reports_PDF/{fd}/{from}/{to}', ['uses' =>'PointReportController@generatePointReportsPDF'])->name('generate_point_reports_pdf');

Route::get('/point_reports/point_reports_fd/{id}', 'PointReportController@pointReportsFD')->name('view.point.reports.fd');

Route::get('/point_reports/view_point_reports_fd', 'PointReportController@viewPointReportsFD');

Route::post('/point_reports/view_point_reports_fd', 'PointReportController@viewPointReportsFD');

Route::get('/point_reports/view_point_reports', 'PointReportController@viewPointReports');

Route::post('/point_reports/view_point_reports', 'PointReportController@viewPointReports');

Route::resource('point_reports','PointReportController')->only([
    'index', 'show'
]);

// End Point Report Module Routes


// Start Configuration Module Routes

Route::resource('configurations','ConfigurationController')->only([
    'create', 'store'
]);

// End Configuration Module Routes


// Start Background Process Module Routes

Route::get('/background_process/view_error_details', 'BackgroundProcessController@viewErrorDetails')->name('view.error.details');

Route::post('/background_process/ignore_cms_trans_errors/{id}', 'BackgroundProcessController@ignoreCmsTransErrors')->name('ignore.cms.trans.errors');

Route::post('/background_process/unignore_cms_trans_errors/{id}', 'BackgroundProcessController@unignoreCmsTransErrors')->name('unignore.cms.trans.errors');

Route::get('/background_process/view_status_and_parameters', 'BackgroundProcessController@viewStatusParameters')->name('view.status.parameters');

Route::post('/background_process/update_parameters', 'BackgroundProcessController@updateParameters')->name('update.parameters');

// End Background Process Module Routes