@extends('layouts.master')

@section('title', 'Import Loyalty Items Mapping')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('items.index') }}">Item</a></li>
                    <li class="breadcrumb-item active">Import Export Loyalty Items</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('import.export.loyalty.items.view') }}" class="fa fa-angle-left go-back"></a> Import Loyalty Items Mapping</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="card card-white card-shadow card-special">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        <form method="POST" action="{{ route('start.import.loyalty.items') }}" id="form-work" class="form-horizontal" role="form" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Item Code</label>
                                <div class="col-md-6">
                                    <select class="full-width" data-placeholder="Select Excel Header Name" data-init-plugin="select2" name="mapping_field_item_code" required>
                                        <option value="{{ old('mapping_field_item_code') }}" ></option>
                                        @foreach($excel_headers as $excel_header )
                                            <option value="{{ $excel_header }}">{{ $excel_header }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mapping_field_item_code'))
                                        <div class="error">{{ $errors->first('mapping_field_item_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Cemetery Code</label>
                                <div class="col-md-6">
                                    <select class="full-width" data-placeholder="Select Excel Header Name" data-init-plugin="select2" name="mapping_field_cemetery_code" required>
                                        <option value="{{ old('mapping_field_cemetery_code') }}" ></option>
                                        @foreach($excel_headers as $excel_header )
                                            <option value="{{ $excel_header }}">{{ $excel_header }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mapping_field_cemetery_code'))
                                        <div class="error">{{ $errors->first('mapping_field_cemetery_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Status</label>
                                <div class="col-md-6">
                                    <select class="full-width" data-placeholder="Select Excel Header Name" data-init-plugin="select2" name="mapping_field_status" required>
                                        <option value="{{ old('mapping_field_status') }}" ></option>
                                        @foreach($excel_headers as $excel_header )
                                            <option value="{{ $excel_header }}">{{ $excel_header }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mapping_field_status'))
                                        <div class="error">{{ $errors->first('mapping_field_status') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">To Earn</label>
                                <div class="col-md-6">
                                    <select class="full-width" data-placeholder="Select Excel Header Name" data-init-plugin="select2" name="mapping_field_to_earn" required>
                                        <option value="{{ old('mapping_field_to_earn') }}" ></option>
                                        @foreach($excel_headers as $excel_header )
                                            <option value="{{ $excel_header }}">{{ $excel_header }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mapping_field_to_earn'))
                                        <div class="error">{{ $errors->first('mapping_field_to_earn') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">To Redeem</label>
                                <div class="col-md-6">
                                    <select class="full-width" data-placeholder="Select Excel Header Name" data-init-plugin="select2" name="mapping_field_to_redeem" required>
                                        <option value="{{ old('mapping_field_to_redeem') }}" ></option>
                                        @foreach($excel_headers as $excel_header )
                                            <option value="{{ $excel_header }}">{{ $excel_header }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mapping_field_to_redeem'))
                                        <div class="error">{{ $errors->first('mapping_field_to_redeem') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Start Date</label>
                                <div class="col-md-6">
                                    <select class="full-width" data-placeholder="Select Excel Header Name" data-init-plugin="select2" name="mapping_field_start_date" required>
                                        <option value="{{ old('mapping_field_start_date') }}" ></option>
                                        @foreach($excel_headers as $excel_header )
                                            <option value="{{ $excel_header }}">{{ $excel_header }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mapping_field_start_date'))
                                        <div class="error">{{ $errors->first('mapping_field_start_date') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">End Date</label>
                                <div class="col-md-6">
                                    <select class="full-width" data-placeholder="Select Excel Header Name" data-init-plugin="select2" name="mapping_field_end_date" required>
                                        <option value="{{ old('mapping_field_end_date') }}" ></option>
                                        @foreach($excel_headers as $excel_header )
                                            <option value="{{ $excel_header }}">{{ $excel_header }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mapping_field_end_date'))
                                        <div class="error">{{ $errors->first('mapping_field_end_date') }}</div>
                                    @endif
                                </div>
                            </div>
                            <br>
                            <div class="row clearfix padding-bottom-10">
                                <input type="hidden" name="excel_file_path" value="{{ $file_path }}" />
                                <input type="hidden" name="sheet_index" value="{{ $sheet_index }}" />
                                <div class="col-md-6">
                                    <a class="btn btn-light btn-lg" href="{{ route('import.export.loyalty.items.view') }}">Back to Import Export Loyalty Items</a>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button class="btn btn-primary btn-lg" type="submit">Import Loyalty Items</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection