@extends('layouts.master')

@section('title', 'Select Excel Sheet to Import')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('items.index') }}">Item</a></li>
                    <li class="breadcrumb-item active">Import Export Loyalty Items</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('import.export.loyalty.items.view') }}" class="fa fa-angle-left go-back"></a> Select Excel Sheet to Import</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{ route('select.sheet.loyalty.items') }}" id="form-personal" role="form" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group form-group-default required">
                                        <label>Select Excel Sheet to Import</label>
                                        <select class="full-width" data-placeholder="Select Excel Sheet to Import" data-init-plugin="select2" name="sheet_index">
                                            <option value="{{ old('sheet_index') }}" ></option>
                                            <?php $i = 0; ?>
                                            @foreach($excel_sheets as $excel_sheet )
                                                <option value="{{ $i }}">{{ $excel_sheet }}</option>
                                                <?php $i++; ?>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('sheet_index'))
                                            <div class="error">{{ $errors->first('sheet_index') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix padding-bottom-10">
                        <input type="hidden" name="excel_file_path" value="{{ $file_path }}" />
                        <div class="col-md-6">
                            <a class="btn btn-light btn-lg" href="{{ route('import.export.loyalty.items.view') }}">Back to Import Export Loyalty Items</a>
                        </div>
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary btn-lg" type="submit">Select Sheet</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection