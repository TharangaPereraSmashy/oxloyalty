@extends('layouts.master')

@section('title', 'Edit Item Loyalty')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('items.show',$item_id) }}">Item</a></li>
                    <li class="breadcrumb-item active">Edit Item Loyalty</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('items.show',$item_id) }}" class="fa fa-angle-left go-back"></a> Edit Item Loyalty</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{ route('update_cemetery_item_by_item', array('id' => $cemetery_item->id, 'item' => $item_id)) }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    @method('PUT')
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>Item</label>
                                        <select class="full-width" data-placeholder="Select Item" data-init-plugin="select2" name="item_id" required>
                                            @foreach($items as $item )
                                                <option value="{{ $item->id }}" @if($item->id==$cemetery_item->item_id) selected @endif>{{ $item->item_name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('item_id'))
                                            <div class="error">{{ $errors->first('item_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>Cemetery</label>
                                        <select class="full-width" data-placeholder="Select Cemetery" data-init-plugin="select2" name="cemetery_id" required>
                                            <option value="" ></option>
                                            @foreach($cemeteries as $cemetery )
                                                <option value="{{ $cemetery->id }}" @if($cemetery->id==$cemetery_item->cemetery_id) selected @endif>{{ $cemetery->cemetery_name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('cemetery_id'))
                                            <div class="error">{{ $errors->first('cemetery_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Status</label>
                                        <div class="radio radio-success radio-padding">
                                            <div class="row">
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="1" name="status" id="active" @if($cemetery_item->status == '1') checked @endif>
                                                    <label for="active">Active</label>
                                                </div>
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="0" name="status" id="inactive" @if($cemetery_item->status == '0') checked @endif>
                                                    <label for="inactive">Inactive</label>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($errors->has('status'))
                                            <div class="error">{{ $errors->first('status') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Earns Loyalty Dollars</label>
                                        <input type="text" class="form-control float-only" value="{{ $cemetery_item->to_earn }}" id="to_earn" placeholder="Earns Loyalty Dollars" name="to_earn" required>
                                        @if ($errors->has('to_earn'))
                                            <div class="error">{{ $errors->first('to_earn') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Loyalty Dollars to Redeem</label>
                                        <input type="text" class="form-control float-only" value="{{ $cemetery_item->to_redeem }}" id="to_redeem" placeholder="Loyalty Dollars to Redeem" name="to_redeem" required>
                                        @if ($errors->has('to_redeem'))
                                            <div class="error">{{ $errors->first('to_redeem') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Start Date</label>
                                        <input type="text" class="form-control" value="{{ date('d-m-Y', strtotime($cemetery_item->start_date)) }}" id="start_date" placeholder="Start Date" name="start_date">
                                        @if ($errors->has('start_date'))
                                            <div class="error">{{ $errors->first('start_date') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>End Date</label>
                                        <input type="text" class="form-control" value="{{ date('d-m-Y', strtotime($cemetery_item->end_date)) }}" id="end_date" placeholder="End Date" name="end_date">
                                        @if ($errors->has('end_date'))
                                            <div class="error">{{ $errors->first('end_date') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix padding-bottom-20">
                        {{-- <div class="col-md-6">
                            <a class="btn btn-light btn-lg" href="{{ route('cemetery_items.index') }}">View all Item Loyalty</a>
                        </div> --}}
                        <div class="col-md-12 text-right">
                            <input type="hidden" name="updated_at" value="{{ $cemetery_item->updated_at }}">
                            @if(Session::get('update_item'))
                                <button class="btn btn-complete btn-lg" type="submit">Save</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script type="text/javascript">

        $("#start_date").datepicker({
            //startDate: new Date(),
            format: 'dd-mm-yyyy',
            autoclose: true
        }).on('changeDate', function (selected) {
            $("#end_date").val("").datepicker("update");
            var minDate = new Date(selected.date.valueOf());
            minDate.setDate(minDate.getDate() + 1);
            $('#end_date').datepicker('setStartDate', minDate);
        });

        $("#end_date").datepicker({
            //startDate: '+1d',
            format: 'dd-mm-yyyy',
            autoclose: true
        });

    </script>

@endsection