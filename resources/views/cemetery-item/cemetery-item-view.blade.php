@extends('layouts.master')

@section('title', 'View Item Loyalty')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('cemetery_items.index') }}">Item Loyalty</a></li>
                    <li class="breadcrumb-item active">View Item Loyalty</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="page-header">
            <div class="card-title"><a href="{{ route('cemetery_items.index') }}" class="fa fa-angle-left"></a> View Item Loyalty</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="#" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Item</label>
                                        <select class="full-width" data-placeholder="Select Item" data-init-plugin="select2" name="item_id" disabled>
                                            <option value="" ></option>
                                            @foreach($items as $item )
                                                <option value="{{ $item->id }}" @if($item->id==$cemetery_item->item_id) selected @endif>{{ $item->item_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Cemetery</label>
                                        <select class="full-width" data-placeholder="Select Cemetery" data-init-plugin="select2" name="cemetery_id" disabled>
                                            <option value="" ></option>
                                            @foreach($cemeteries as $cemetery )
                                                <option value="{{ $cemetery->id }}" @if($cemetery->id==$cemetery_item->cemetery_id) selected @endif>{{ $cemetery->cemetery_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Status</label>
                                        <div class="radio radio-success radio-padding">
                                            <div class="row">
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="1" name="status" id="active" @if($cemetery_item->status == '1') checked @endif disabled>
                                                    <label for="active">Active</label>
                                                </div>
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="0" name="status" id="inactive" @if($cemetery_item->status == '0') checked @endif disabled>
                                                    <label for="inactive">Inactive</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Points To Earn</label>
                                        <input type="text" class="form-control float-only" value="{{ $cemetery_item->to_earn }}" id="to_earn" placeholder="Points To Earn" name="to_earn" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Points To Redeem</label>
                                        <input type="text" class="form-control float-only" value="{{ $cemetery_item->to_redeem }}" id="to_redeem" placeholder="Points To Redeem" name="to_redeem" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Start Date</label>
                                        <input type="text" class="form-control" value="{{ date('d-m-Y', strtotime($cemetery_item->start_date)) }}" id="start_date" placeholder="Start Date" name="start_date" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>End Date</label>
                                        <input type="text" class="form-control" value="{{ date('d-m-Y', strtotime($cemetery_item->end_date)) }}" id="end_date" placeholder="End Date" name="end_date" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix padding-bottom-20">
                        <div class="col-md-12 text-right">
                            <a class="btn btn-success btn-lg" href="{{ route('cemetery_items.index') }}"> Ok</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>    
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection