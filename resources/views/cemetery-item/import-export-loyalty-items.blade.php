@extends('layouts.master')

@section('title', 'Import Export Loyalty Items')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('items.index') }}">Item</a></li>
                    <li class="breadcrumb-item active">Import Export Loyalty Items</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('items.index') }}" class="fa fa-angle-left go-back"></a> Import Export Loyalty Items</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="row">
            <div class="col-md-12">
              <a href="{{ route('export.loyalty.items',['type'=>'xls']) }}">Download Excel xls</a> |
              <a href="{{ route('export.loyalty.items',['type'=>'xlsx']) }}">Download Excel xlsx</a> |
              <a href="{{ route('export.loyalty.items',['type'=>'csv']) }}">Download CSV</a>
            </div>
        </div> 
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{ route('import.loyalty.items') }}" id="form-personal" role="form" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group form-group-default required">
                                        <label>Select File to Import</label>
                                        <input type="file" class="form-control" id="import_file" placeholder="Select File to Import" name="import_file">
                                        {{--  <input type="hidden" name="import_file_path" id="import_file_path" />  --}}
                                        @if ($errors->has('import_file'))
                                            <div class="error">{{ $errors->first('import_file') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix padding-bottom-20">
                        <div class="col-md-6">
                            <a class="btn btn-light btn-lg" href="{{ route('items.index') }}">View all Items</a>
                        </div>
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary btn-lg" type="submit">Upload</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script type="text/javascript">

        //jQuery('input[type=file]').change(function(){

        //    var import_file = $(this).val();

        //    getExcelFilePath(import_file);

        //});

        //function getExcelFilePath(import_file){
        //    var startIndex = (import_file.indexOf('\\') >= 0 ? import_file.lastIndexOf('\\') : import_file.lastIndexOf('/'));
        //    var filename = import_file.substring(startIndex);
        //    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
        //        filename = filename.substring(1);
        //    }
        //    $('#import_file_path').val(filename);
        //}

    </script>

@endsection