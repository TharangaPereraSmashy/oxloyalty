@extends('layouts.master')

@section('title', 'Item Loyalty')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('cemetery_items.index') }}">Item Loyalty</a></li>
                    <li class="breadcrumb-item active">All Item Loyalty</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="page-header">
            <div class="card-title pull-left" style="padding-top: 10px;">All Item Loyalty</div>
            <div class="pull-right">
                <form method="POST" action="{{ action('CemeteryItemController@searchCemeteryItems') }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="input-group">
                        <a class="btn btn-primary btn-rounded" style="margin-right: 30px;" href="{{ route('cemetery_items.create') }}"><span class="fa fa-plus"></span>&nbsp;Add Item Loyalty</a>
                        <input type="text" class="form-control form-search" name="search_value" value="{{ old('search_value') }}" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-search btn-sm" type="submit"><span class="fa fa-search"></span></button>
                        </span>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="card card-white card-shadow card-special">

            @if(count($cemetery_items)==0)
                <div class="card-block text-center">No Results Available</div>
            @else

            <div class="card-block">
                <table class="table table-hover demo-table-search table-responsive-block">
                    <thead>
                    <tr>
                        <th style="width: 10% !important;">Item Code</th>
                        <th style="width: 18% !important;">Item</th>
                        <th style="width: 12% !important;">Cemetery</th>
                        <th style="width: 10% !important;">To Earn</th>
                        <th style="width: 10% !important;">To Redeem</th>
                        <th style="width: 10% !important;">Start Date</th>
                        <th style="width: 10% !important;">End Date</th>
                        <th style="width: 20% !important;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($cemetery_items as $cemetery_item)

                        <tr class="gradeA">
                            <td>{{ $cemetery_item->item_code }}</td>
                            <td>{{ $cemetery_item->item }}</td>
                            <td>{{ $cemetery_item->cemetery }}</td>
                            <td>{{ $cemetery_item->to_earn }}</td>
                            <td>{{ $cemetery_item->to_redeem }}</td>
                            <td>{{ date('d-m-Y', strtotime($cemetery_item->start_date)) }}</td>
                            <td>{{ date('d-m-Y', strtotime($cemetery_item->end_date)) }}</td>
                            <td>
                                <form action="{{ route('cemetery_items.destroy',$cemetery_item->id) }}" method="POST" class="delete_record">

                                    @if(Session::get('read_cemetery_item'))
                                        <a class="btn btn-xs btn-rounded btn-complete" href="{{ route('cemetery_items.show',$cemetery_item->id) }}"><span class="fa fa-eye"></span></a>
                                    @endif

                                    @if(Session::get('update_cemetery_item'))
                                        <a class="btn btn-xs btn-rounded btn-success" href="{{ route('cemetery_items.edit',$cemetery_item->id) }}"><span class="fa fa-edit"></span></a>
                                    @endif

                                    @csrf
                                    @method('DELETE')

                                    @if(Session::get('delete_cemetery_item'))
                                        <button type="submit" class="btn btn-xs btn-rounded btn-danger"><span class="fa fa-trash"></span></button>
                                    @endif

                                </form>
                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
            </div>

            @endif

        </div>
        <!-- END card -->

        <div class="row pagination-div">
            <div class="col-md-12">
                <div class="pull-right">
                    {!! $cemetery_items->links() !!}
                </div>
            </div>
        </div>

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script>
        $(".delete_record").on("submit", function(){
            return confirm("Do you want to delete this record ?");
        });
    </script>

@endsection