@extends('layouts.master')

@section('title', 'Edit Configurations')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('configurations.create') }}">Configurations</a></li>
                    <li class="breadcrumb-item active">Edit Configurations</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('configurations.create') }}" class="fa fa-angle-left go-back"></a> Edit Configurations</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{ route('configurations.store') }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Validity Period Days</label>
                                        <input type="hidden" name="parameter1" value={{ $config1[0]->parameter }} />
                                        <input type="text" class="form-control int-only" id="validity_period_days" placeholder="Validity Period Days" name="validity_period_days" value={{ $config1[0]->value }} required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Maximum Inactivity Days</label>
                                        <input type="hidden" name="parameter2" value={{ $config2[0]->parameter }} />
                                        <input type="text" class="form-control int-only" id="maximum_inactivity_days" placeholder="Maximum Inactivity Days" name="maximum_inactivity_days" value={{ $config2[0]->value }} required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix padding-bottom-20">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-success btn-lg" type="submit">Update Configurations</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection