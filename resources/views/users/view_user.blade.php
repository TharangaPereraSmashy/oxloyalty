@extends('layouts.master')

@section('title', 'User Details')

@section('content')
    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('registeruser.index') }}">Users</a></li>
                    <li class="breadcrumb-item active">View User</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('registeruser.index') }}" class="fa fa-angle-left go-back"></a> User Details</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <form id="form-work" method="post" action="#" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Name</label>
                                        <input type="text" class="form-control" id="name" placeholder="Full Name" name="name" value="{{ $user->name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="email" class="form-control" id="email" placeholder="E-mail" name="email" value="{{ $user->email }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" id="phone" placeholder="Phone no" name="phone"  value="{{ $user->phone }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>Type</label>
                                        <select class="full-width" data-placeholder="Select Type" data-init-plugin="select2" name="type" disabled>
                                            <option value="">""</option>
                                            @foreach($types as $type)
                                                <option value="{{ $type->id }}" @if($type->id==$user->type) selected @endif>{{ $type->description }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>Profile</label>
                                        <select class="full-width" data-placeholder="Select Profile" data-init-plugin="select2" name="profile" disabled>
                                            <option value="">""</option>
                                            @foreach($profiles as $profile)
                                                <option value="{{ $profile->id }}" @if($profile->id==$user->profile) selected @endif>{{ $profile->description }}
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>Default Cemetery</label>
                                        <select class="full-width" data-placeholder="Select Cemetery" data-init-plugin="select2" name="default_cemetery" disabled>
                                            <option value="">""</option>
                                            @foreach($cemeteries as $cemetery)
                                                <option value="{{ $cemetery->id }}" @if($cemetery->id==$user->default_cemetery) selected @endif>{{ $cemetery->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>FD Account</label>
                                        <select class="full-width" data-placeholder="Select FD Account" data-init-plugin="select2" name="fdaccount" disabled>
                                            <option value="">""</option>
                                            @foreach($fdirectors as $fdirector)
                                                <option value="{{ $fdirector->id }}" @if($fdirector->id==$user->fd_id) selected @endif>{{ $fdirector->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Status</label>
                                        <div class="radio radio-success">
                                            <div class="row">
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="1" name="status" id="active" @if($user->status == '1') checked @endif disabled>
                                                    <label for="active">Active</label>
                                                </div>
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="0" name="status" id="inactive" @if($user->status == '0') checked @endif disabled>
                                                    <label for="inactive">Inactive</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix padding-bottom-20">
                        <div class="col-md-12 text-right">
                            <a class="btn btn-success btn-lg" href="{{ route('registeruser.index') }}"> Ok</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection