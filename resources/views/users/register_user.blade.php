@extends('layouts.master')

@section('title', 'Users')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('registeruser.index') }}">Users</a></li>
                    <li class="breadcrumb-item active">All Users</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="page-header padding-20-top">
            <div class="card-title pull-left"><a href="{{ route('registeruser.index') }}" class="fa fa-angle-left go-back"></a> All Users</div>
            <div class="pull-right">
                @if(Session::get('create_register'))
                <div class="input-group">
                    <a class="btn btn-primary btn-rounded" style="margin-right: 30px;" href="{{ route('registeruser.create') }}"><span class="fa fa-plus"></span>&nbsp;Create New User</a>
                </div>
                @endif
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="card card-white card-shadow card-special">

            @if(count($users)==0)
                <div class="card-block text-center">No Results Available</div>
            @else
    
            <div class="card-block">
                <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($users as $user)

                        <tr class="gradeA">
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>

                                <form action="{{ route('registeruser.destroy',$user->id) }}" method="POST" class="delete_record">

                                    @if(Session::get('read_register'))
                                        <a class="btn btn-xs btn-rounded btn-complete" href="{{ route('registeruser.show',$user->id) }}"><span class="fa fa-eye"></span></a>
                                    @endif

                                    @if(Session::get('update_register'))
                                        <a class="btn btn-xs btn-rounded btn-success" href="{{ route('registeruser.edit',$user->id) }}"><span class="fa fa-edit"></span></a>
                                    @endif

                                    @csrf
                                    @method('DELETE')

                                    @if(Session::get('delete_register'))
                                        <button type="submit" class="btn btn-xs btn-rounded btn-danger"><span class="fa fa-trash"></span></button>
                                    @endif

                                </form>

                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
            </div>

            @endif

        </div>

        <div class="row pagination-div">
            <div class="col-md-12">
                <div class="pull-right">
                    {!! $users->links() !!}
                </div>
            </div>
        </div>
        <!-- END card -->

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script>
        $(".delete_record").on("submit", function(){
            return confirm("Do you want to delete this record ?");
        });
    </script>

@endsection