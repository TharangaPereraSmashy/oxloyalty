<div class="modal fade slide-up disable-scroll" id="edit-permission-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5> <span class="semi-bold">Edit Permission</span></h5>
                    <p class="p-b-10"></p>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{action('UserPermissionController@update', 0)}}" role="form">
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="PATCH">
                        <input type="hidden" id="id" name="id" >
                        <input type="hidden" id="pageId" name="pageId" >
                        <input type="hidden" id="profileId" name="profileId" value="{{$profile['id']}}">
                        <div class="form-group">
                            <div class="row">
                                <label for="type" class="col-md-3 control-label" id="page_desc" style="font-size:14px"></label>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <div class="checkbox check-primary">
                                            <input type="checkbox" value="1" id="edit-create" name="create">
                                            <label for="edit-create">Create</label>
                                        </div>
                                        <div class="checkbox check-primary">
                                            <input type="checkbox" value="1" id="edit-read" name="read">
                                            <label for="edit-read">Read</label>
                                        </div>
                                        <div class="checkbox check-primary">
                                            <input type="checkbox" value="1" id="edit-update" name="update">
                                            <label for="edit-update">Update</label>
                                        </div>
                                        <div class="checkbox check-primary">
                                            <input type="checkbox" value="1" id="edit-delete" name="delete">
                                            <label for="edit-delete">Delete</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4 m-t-10 sm-m-t-10">
                                    <button type="submit" class="btn btn-primary btn-block m-t-5">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
