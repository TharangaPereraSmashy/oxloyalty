@extends('layouts.master')

@section('title', 'Add User')

@section('content')
    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('registeruser.index') }}">Users</a></li>
                    <li class="breadcrumb-item active">Add User</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('registeruser.index') }}" class="fa fa-angle-left go-back"></a> Add User</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <form id="form-work" method="POST" action="{{url('registeruser')}}" id="form-personal" role="form" autocomplete="off">
                    @csrf

                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>Name</label>
                                        <input type="text" class="form-control" id="name" placeholder="Full Name" name="name" value="{{ old('name') }}" required>
                                        @if ($errors->has('name'))
                                            <div class="error">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>Email</label>
                                        <input type="email" class="form-control" id="email" placeholder="E-mail" name="email" value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                            <div class="error">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Password</label>
                                        <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
                                        @if ($errors->has('password'))
                                            <div class="error">{{ $errors->first('password') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Confirm Password</label>
                                        <input type="password" class="form-control" id="conf_password" placeholder="Confirm Password" name="conf_password" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" id="phone" placeholder="Phone no" name="phone"  value="{{ old('phone') }}">
                                        @if ($errors->has('phone'))
                                            <div class="error">{{ $errors->first('phone') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>Type</label>
                                        <select class="full-width" data-placeholder="Select User Type" data-init-plugin="select2" name="type" id="type" required>
                                            <option value="">""</option>
                                            @foreach($types as $type)
                                                <option value="{{ $type->id }}">{{ $type->description }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('type'))
                                            <div class="error">{{ $errors->first('type') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>Profile</label>
                                        <select class="full-width" data-placeholder="Select User Profile" data-init-plugin="select2" name="profile" id="profile" required>
                                            <option value="">""</option>
                                            @foreach($profiles as $profile)
                                                <option value="{{ $profile->id }}">{{ $profile->description }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('profile'))
                                            <div class="error">{{ $errors->first('profile') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>Default Cemetery</label>
                                        <select class="full-width" data-placeholder="Select Cemetery" data-init-plugin="select2" name="default_cemetery">
                                            <option value="">""</option>
                                            @foreach($cemeteries as $cemetery)
                                                <option value="{{ $cemetery->id }}">{{ $cemetery->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('default_cemetery'))
                                            <div class="error">{{ $errors->first('default_cemetery') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>FD Account</label>
                                        <select class="full-width" data-placeholder="Select FD Account" data-init-plugin="select2" name="fdaccount">
                                            <option value="">""</option>
                                            @foreach($fdirectors as $fdirector)
                                                <option value="{{ $fdirector->id }}">{{ $fdirector->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('fdaccount'))
                                            <div class="error">{{ $errors->first('fdaccount') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Status</label>
                                        <div class="radio radio-success">
                                            <div class="row">
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="1" name="status" id="active" checked="checked">
                                                    <label for="active">Active</label>
                                                </div>
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="0" name="status" id="inactive">
                                                    <label for="inactive">Inactive</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix padding-bottom-20">
                        <div class="col-md-6">
                            <a class="btn btn-light btn-lg" href="{{ route('registeruser.index') }}">View all Users</a>
                        </div>
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary btn-lg" type="submit">Add User</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection