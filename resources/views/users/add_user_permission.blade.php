<div class="modal fade slide-up disable-scroll" id="add-permission-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5> <span class="semi-bold">Add Permission</span></h5>
                    <p class="p-b-10"></p>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{url('permission')}}" role="form">
                        {{ csrf_field() }}
                        <input type="hidden" id="profileId" name="profileId" value="{{$profile['id']}}">
                        <div class="form-group">
                            <div class="row">
                                <label for="type" class="col-md-3 control-label">Page</label>
                                <div class="col-md-6">
                                    <select name="pageId" style="width:100%;" required>
                                        <option value="">-- Please Select --</option>
                                        @foreach($pages as $page)
                                            <option value="{{ $page->id }}">{{ $page->description }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('type'))
                                        <div class="alert alert-danger" role="alert">
                                            <button class="close" data-dismiss="alert"></button>
                                            {{ $errors->first('pageId') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <div class="checkbox check-primary">
                                            <input type="checkbox" value="1" id="create" name="create">
                                            <label for="create">Create</label>
                                        </div>
                                        <div class="checkbox check-primary">
                                            <input type="checkbox" value="1" id="read" name="read">
                                            <label for="read">Read</label>
                                        </div>
                                        <div class="checkbox check-primary">
                                            <input type="checkbox" value="1" id="update" name="update">
                                            <label for="update">Update</label>
                                        </div>
                                        <div class="checkbox check-primary">
                                            <input type="checkbox" value="1" id="delete" name="delete">
                                            <label for="delete">Delete</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4 m-t-10 sm-m-t-10">
                                    <button type="submit" class="btn btn-primary btn-block m-t-5">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
