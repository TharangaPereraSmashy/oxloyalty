@extends('layouts.master')

@section('title', 'User Profile Permissions')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('profile.index') }}">Profiles</a></li>
                    <li class="breadcrumb-item active">Permission</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
    <!-- START card -->
        <div class="page-header padding-20-top">
            <div class="card-title pull-left"><a href="{{ route('profile.index') }}" class="fa fa-angle-left go-back"></a> User Profile Permissions</div>
            <div class="pull-right">
                    @include('users.add_user_permission')
                    <button class="btn btn-rounded btn-primary pull-left btn-cons m-b-10" type="button" id="btn-add-permission"><i class="fa fa-plus"></i> <span class="bold">Add New Permission</span></button>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="card card-white card-shadow card-special">
            <div class="card-block">
                @include('users.edit_user_permission')
                <input type="hidden" id="profile_id" name="profile_id" value="{{$profile['id']}}">
                <input type="hidden" id="profile_desc" name="profile_desc" value="{{$profile['description']}}">
                <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                    <thead>
                    <tr>
                        <th>Page Description</th>
                        <th>Create</th>
                        <th>Read</th>
                        <th>Update</th>
                        <th>Delete</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($permissions as $permission)
                        <tr class="gradeA">
                            <td>{{$permission->page_description}}</td>

                            @if($permission->create == 1)
                                <td><span class="fa fa-check"></span></td>
                            @else
                                <td><span class="fa fa-remove"></span></td>
                            @endif

                            @if($permission->read == 1)
                                <td><span class="fa fa-check"></span></td>
                            @else
                                <td><span class="fa fa-remove"></span></td>
                            @endif

                            @if($permission->update == 1)
                                <td><span class="fa fa-check"></span></td>
                            @else
                                <td><span class="fa fa-remove"></span></td>
                            @endif

                            @if($permission->delete == 1)
                                <td><span class="fa fa-check"></span></td>
                            @else
                                <td><span class="fa fa-remove"></span></td>
                            @endif

                            <td>
                                <form action="{{action('UserPermissionController@destroy', $permission->id)}}" method="post">
                                    <input type="hidden" id="type_id" name="type_id">
                                    <a href="#" class="btn btn-xs btn-rounded btn-success" onclick="editUserPermission({{$permission->id.',"'.$permission->page_description.'"'}})" ><span class="fa fa-edit"></span></a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-rounded btn-danger"><span class="fa fa-remove"></span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END card -->
@endsection

