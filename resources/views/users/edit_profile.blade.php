<div class="modal fade slide-up disable-scroll" id="edit_profile" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5> <span class="semi-bold">Edit Profile</span></h5>
                    <p class="p-b-10"></p>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{action('UserProfileController@update', 0)}}">
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="PATCH">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        <label>User Type</label>
                                        <select name="type" id="edit_type" class="form-control" required>
                                            <option value="" >-- Please Select --</option>
                                            @foreach($types as $type )
                                                <option value="{{ $type->id }}">{{ $type->description }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('type'))
                                            <div class="error">{{ $errors->first('type') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        <label>Profile Description</label>
                                        <input type="hidden" id="edit_id" name="id">
                                        <input type="text" id="edit_description" name="description" class="form-control" required>
                                        @if ($errors->has('description'))
                                            <div class="error">{{ $errors->first('description') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4 m-t-10 sm-m-t-10">
                                    <button type="submit" class="btn btn-primary btn-block m-t-5">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
