@extends('layouts.master')

@section('title', 'User Profiles')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('profile.index') }}">Profiles</a></li>
                    <li class="breadcrumb-item active">All Profiles</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="page-header padding-20-top">
            <div class="card-title pull-left"><a href="{{ route('profile.index') }}" class="fa fa-angle-left go-back"></a> User Profiles</div>
            <div class="pull-right">
                @include('users.add_profile')
                <button class="btn btn-rounded btn-primary pull-left btn-cons m-b-10" type="button" id="btnToggleSlideUpSize"><i class="fa fa-plus"></i> <span class="bold">Add Profile</span></button>
            </div>
            <div class="clearfix"></div>
        </div>
        @include('users.edit_profile')
        <div class="card card-white card-shadow card-special">

            @if(count($profiles)==0)
                <div class="card-block text-center">No Results Available</div>
            @else

            <div class="card-block">
                <table class="table table-hover demo-table-search table-responsive-block" id="user_profile_table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Description</th>
                            <th>Type</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($profiles as $profile)
                            <tr class="gradeA">
                                <td>{{$profile['id']}}</td>
                                <td>{{$profile['description']}}</td>
                                <td>{{$profile['type_name']}}</td>
                                <td>
                                    <form action="{{action('UserProfileController@destroy', $profile['id'])}}" method="post" class="delete_record">
                                        <a href="#" class="btn btn-xs btn-rounded btn-success" onclick="editUserProfile({{$profile['id']}})" ><span class="fa fa-edit"></span></a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-xs btn-rounded btn-danger"><span class="fa fa-trash"></span></button>
                                        <a class="btn btn-xs btn-rounded btn-info" href="{{ route('permission.show', $profile['id']) }}"><span class="fa fa-lock"></span>&nbsp;Permission</a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @endif

        </div>
    </div>
    <!-- END card -->
@endsection

@section('script')

    <script>
        jQuery('#user_profile_table').dataTable({
            "bLengthChange":false,
            "info":false,
            "columnDefs": [
                { "orderable": false, "targets": 3 }
            ]
        });

        $(".delete_record").on("submit", function(){
            return confirm("Do you want to delete this record ?");
        });
    </script>

@endsection