@extends('layouts.master')

@section('title', 'Audits')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('audits.index') }}">Audit</a></li>
                    <li class="breadcrumb-item active">All Audits</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('audits.index') }}" class="fa fa-angle-left go-back"></a> All Audits</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="card card-white card-shadow card-special">
            <div class="card-block">
                <form method="POST" action="{{ action('AuditController@searchAudits') }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="row clearfix">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Component</label>
                                <select class="form-control" data-placeholder="Select Component" data-init-plugin="select2" name="auditable_type">
                                    <option value="{{ old('auditable_type') }}" ></option>
                                    @foreach($components as $component )
                                        <option value="{{ $component->auditable_type }}">{{ $component->auditable_type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>From Date</label>
                                <input type="text" class="form-control" placeholder="Select From Date" name="from_date" value="{{ old('from_date') }}" id="from_date">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>To Date</label>
                                <input type="text" class="form-control" placeholder="Select To Date" name="to_date" value="{{ old('to_date') }}" id="to_date">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-info pull-right" type="submit"><span class="fa fa-refresh"></span>&nbsp;Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- START card -->
        <div class="card card-white card-shadow card-special">
            <div class="card-header ">
                <div class="card-title">Search Results</div>
                <div class="clearfix"></div>
            </div>

            @if(count($audits)==0)
                <div class="card-block text-center">No Results Available</div>
            @else

            <div class="card-block">
                <table class="table table-hover demo-table-search table-responsive-block" id="audit_grid">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>User</th>
                            <th>Event</th>
                            <th>Component</th>
                            {{--  <th>Record ID</th>  --}}
                            <th>Record</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($audits as $audit)

                        <tr class="gradeA">
                            <td>{{ date('d-m-Y h:i:s A', strtotime($audit->created_at)) }}</td>
                            <td>{{ $audit->user_name }}</td>
                            <td>{{ $audit->event }}</td>
                            <td>{{ $audit->auditable_type }}</td>
                            {{--  <td>{{ $audit->auditable_code }}</td>  --}}
                            <td> 
                                @if($audit->event=="updated" || $audit->event=="deleted")

                                    @if (strlen($audit->old_values) >= 40)
                                        {{ substr($audit->old_values, 0, 40) }} <a href='#' data-toggle='tooltip' title='{{ $audit->old_values }}'> {{" . . . "}}</a>
                                    @else
                                        {{ $audit->old_values }}
                                    @endif
                                
                                @else

                                    @if (strlen($audit->new_values) >= 40)
                                        {{ substr($audit->new_values, 0, 40) }} <a href='#' data-toggle='tooltip' title='{{ $audit->new_values }}'> {{" . . . "}}</a>
                                    @else
                                        {{ $audit->new_values }}
                                    @endif

                                @endif
                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>

            </div>

            @endif

        </div>
        <!-- END card -->

        <div class="row pagination-div">
            <div class="col-md-12">
                <div class="pull-right">
                    {!! $audits->links() !!}
                </div>
            </div>
        </div>

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip(); 
        });

        /*$("#from_date").datepicker({
            format: {
                toDisplay: function (date, format, language) {
                    var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
            
                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                
                    return [day, month, year].join('/');
                },
                toValue: function (date, format, language) {
                    var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
            
                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                
                    return [year, month, day].join('-');
                }
            },
            autoclose: true
        });*/

        $("#from_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });

        $("#to_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });

    </script>

@endsection