@extends('layouts.master')


@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('audits.index') }}">Audit</a></li>
                    <li class="breadcrumb-item active">View Audit</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="card card-transparent">
            <div class="card-header ">
                <div class="card-title">View Audit
                </div>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-10">
                        <!--<h3>Description</h3>-->
                        <form method="POST" action="#" id="form-work" class="form-horizontal" role="form" autocomplete="off">
                            @csrf
                            <div class="form-group row">
                                <label for="user_type" class="col-md-3 control-label">User Type</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" id="code" placeholder="User Type" name="user_type" value="{{ $audit->user_type }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_id" class="col-md-3 control-label">User ID</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="user_id" placeholder="User ID" name="user_id" value="{{ $audit->user_id }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="event" class="col-md-3 control-label">Event</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="event" placeholder="Event" name="event" value="{{ $audit->event }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="auditable_type" class="col-md-3 control-label">Auditable Type</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="auditable_type" placeholder="Auditable Type" name="auditable_type" value="{{ $audit->auditable_type }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="auditable_id" class="col-md-3 control-label">Auditable ID</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="auditable_id" placeholder="Auditable ID" name="auditable_id" value="{{ $audit->auditable_id }}" disabled>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="offset-md-3 col-md-9">
                                    <a class="btn btn-complete" href="{{ route('audits.index') }}"> Ok</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection