@extends('layouts.master')

@section('title', 'Funeral Directors')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('funeral_directors.index') }}">Funeral Director</a></li>
                    <li class="breadcrumb-item active">All Funeral Directors</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="page-header padding-20-top">
            <div class="card-title pull-left"><a href="{{ route('funeral_directors.index') }}" class="fa fa-angle-left go-back"></a> Search Results @if($search_value!="")&nbsp;&nbsp;-&nbsp;&nbsp;<span style="font-size:14px;font-weight: bold;"> "{{ $search_value }}"</span> @endif </div>
            <a class="btn btn-warning btn-rounded" style="margin-left: 30px;" href="{{ route('funeral_directors.index') }}"><span class="fa fa-remove"></span>&nbsp;Clear</a>
                        
            <div class="pull-right">
                <form method="POST" action="{{ action('FuneralDirectorController@searchFuneralDirectors') }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="input-group">
                        <input type="text" class="form-control form-search" name="search_value" value="{{ old('search_value') }}" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-search btn-sm" type="submit"><span class="fa fa-search"></span></button>
                        </span>
                    </div>
                </form>
            </div>
            
            <div class="clearfix"></div>
        </div>
        <div class="card card-white card-shadow card-special">

            @if(count($funeral_directors)==0)
                <div class="card-block text-center">No Results Available</div>
            @else

            <div class="card-block">

                <table class="table table-hover demo-table-search table-responsive-block">

                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Parent</th>
                            <th>phone</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    <tbody>

                    @foreach($funeral_directors as $funeral_director)

                        <tr class="gradeA">
                            <td>{{ $funeral_director->code }}</td>
                            <td>{{ $funeral_director->name }}</td>
                            <td>
                                @if($funeral_director->parent_id != '')
                                    <a class="label label-danger" href="{{ route('funeral_directors.show',$funeral_director->parent_id) }}">{{ $funeral_director->parent_fd_code }}</a>
                                @endif
                            </td>
                            <td>{{ $funeral_director->phone }}</td>
                            <td>{{ $funeral_director->email }}</td>
                            <td>
                                <form action="{{ route('funeral_directors.destroy',$funeral_director->id) }}" method="POST">

                                    @if(Session::get('read_funeral_director'))
                                        <a class="btn btn-xs btn-rounded btn-complete" href="{{ route('funeral_directors.show',$funeral_director->id) }}"><span class="fa fa-eye"></span></a>
                                    @endif

                                    @if(Session::get('update_funeral_director'))
                                        <a class="btn btn-xs btn-rounded btn-success" href="{{ route('funeral_directors.edit',$funeral_director->id) }}"><span class="fa fa-edit"></span></a>
                                    @endif

                                    @csrf
                                    @method('DELETE')

                                    @if(Session::get('delete_funeral_director'))
                                        <button type="submit" class="btn btn-xs btn-rounded btn-danger"><span class="fa fa-trash"></span></button>
                                    @endif

                                </form>
                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
            </div>

            @endif

        </div>
        <!-- END card -->

        <div class="row pagination-div">
            <div class="col-md-12">
                <div class="pull-right">
                    {!! $funeral_directors->links() !!}
                </div>
            </div>
        </div>

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection