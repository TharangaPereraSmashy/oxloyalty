@extends('layouts.master')


@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('funeral_directors.index') }}">Funeral Director</a></li>
                    <li class="breadcrumb-item active">Add Funeral Director</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="card card-transparent">
            <div class="card-header ">
                <div class="card-title">Add Funeral Director
                </div>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-10">
                        <!--<h3>Description</h3>-->
                        <form method="POST" action="{{ route('funeral_directors.store') }}" id="form-work" class="form-horizontal" role="form" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="code" class="col-md-3 control-label">Funeral Director Code</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control int-only" value="{{ old('code') }}" id="code" placeholder="Funeral Director Code" name="code" required>
                                    @if ($errors->has('code'))
                                        <div class="error">{{ $errors->first('code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-3 control-label">Funeral Director Name</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" value="{{ old('name') }}" id="name" placeholder="Funeral Director Name" name="name" required>
                                    @if ($errors->has('name'))
                                        <div class="error">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address1" class="col-md-3 control-label">Address 1</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" value="{{ old('address1') }}" id="address1" placeholder="Address 1" name="address1">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address2" class="col-md-3 control-label">Address 2</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" value="{{ old('address2') }}" id="address2" placeholder="Address 2" name="address2">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="suburb" class="col-md-3 control-label">City/Suburb</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" value="{{ old('suburb') }}" id="suburb" placeholder="City/Suburb" name="suburb">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="postcode" class="col-md-3 control-label">Postcode</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" value="{{ old('postcode') }}" id="postcode" placeholder="Postcode" name="postcode">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="country_id" class="col-md-3 control-label">Country</label>
                                <div class="col-md-4">
                                    <select class="full-width" data-placeholder="Select Country" data-init-plugin="select2" name="country_id">
                                        <option value="{{ old('country_id') }}" ></option>
                                        @foreach($countries as $country )
                                            <option value="{{ $country->id }}">{{ $country->country_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="state_id" class="col-md-3 control-label">State</label>
                                <div class="col-md-4">
                                    <select class="full-width" data-placeholder="Select State" data-init-plugin="select2" name="state_id">
                                        <option value="{{ old('state_id') }}" ></option>
                                        @foreach($states as $state )
                                            <option value="{{ $state->id }}">{{ $state->state_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Is Parent</label>
                                <div class="col-md-9">
                                    <div class="radio radio-success">
                                        <input type="radio" value="1" name="is_parent" id="is_parent_yes">
                                        <label for="is_parent_yes">Yes</label>
                                        <input type="radio" value="0" name="is_parent" id="is_parent_no" checked="checked">
                                        <label for="is_parent_no">No</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="parent_id" class="col-md-3 control-label">Parent</label>
                                <div class="col-md-4">
                                    <select class="full-width" data-placeholder="Select Parent" data-init-plugin="select2" name="parent_id">
                                        <option value="{{ old('parent_id') }}" ></option>
                                        @foreach($parents as $parent )
                                            <option value="{{ $parent->id }}">{{ $parent->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-md-3 control-label">Phone</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" value="{{ old('phone') }}" id="phone" placeholder="Phone" name="phone">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-3 control-label">Email</label>
                                <div class="col-md-9">
                                    <input type="email" class="form-control" value="{{ old('email') }}" id="email" placeholder="Email" name="email">
                                    @if ($errors->has('email'))
                                        <div class="error">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="contact_person_name" class="col-md-3 control-label">Contact Person Name</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" value="{{ old('contact_person_name') }}" id="contact_person_name" placeholder="Contact Person Name" name="contact_person_name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Is On Hold</label>
                                <div class="col-md-9">
                                    <div class="radio radio-success">
                                        <input type="radio" value="1" name="is_on_hold" id="is_on_hold_yes">
                                        <label for="is_on_hold_yes">Yes</label>
                                        <input type="radio" value="0" name="is_on_hold" id="is_on_hold_no" checked="checked">
                                        <label for="is_on_hold_no">No</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="gl_liability_code" class="col-md-3 control-label">GL Liability Code</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" value="{{ old('gl_liability_code') }}" id="gl_liability_code" placeholder="GL Liability Code" name="gl_liability_code">
                                    @if ($errors->has('gl_liability_code'))
                                        <div class="error">{{ $errors->first('gl_liability_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="gl_cost_code" class="col-md-3 control-label">GL Cost Code</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" value="{{ old('gl_cost_code') }}" id="gl_cost_code" placeholder="GL Cost Code" name="gl_cost_code">
                                    @if ($errors->has('gl_cost_code'))
                                        <div class="error">{{ $errors->first('gl_cost_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="logo" class="col-md-3 control-label">Logo</label>
                                <div class="col-md-4">
                                    <input type="file" class="form-control" value="{{ old('logo') }}" id="logo" placeholder="Logo" name="logo">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="offset-md-3 col-md-9">
                                    @if(Session::get('create_funeral_director'))
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    @endif
                                    <a class="btn btn-complete" href="{{ route('funeral_directors.index') }}">Back to Funeral Director</a>
                                    <button class="btn btn-default" type="reset">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script type="text/javascript">

        $("#start_date").datepicker({
            startDate: new Date(),
            format: 'yyyy-mm-dd',
            autoclose: true
        }).on('changeDate', function (selected) {
            $("#end_date").val("").datepicker("update");
            var minDate = new Date(selected.date.valueOf());
            minDate.setDate(minDate.getDate() + 1);
            $('#end_date').datepicker('setStartDate', minDate);
        });

        $("#end_date").datepicker({
            startDate: '+1d',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

    </script>

@endsection