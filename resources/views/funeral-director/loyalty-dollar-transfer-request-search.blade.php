@extends('layouts.master')

@section('title', 'Loyalty Dollar Transfer Requests')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Loyalty Dollar Transfer Requests</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="#" class="fa fa-angle-left go-back"></a>Loyalty Dollar Transfer Requests</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="card card-white card-shadow card-special">
            <div class="card-block">
                <form method="POST" action="{{ action('FuneralDirectorController@searchLoyaltyDollarTransferRequests') }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="row clearfix">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>From FD Name</label>
                                <select class="full-width" data-placeholder="Select From FD Name" data-init-plugin="select2" name="from_fd_id">
                                    <option value="{{ old('from_fd_id') }}" ></option>
                                    @foreach($from_funeral_directors as $from_funeral_director )
                                        <option value="{{ $from_funeral_director->id }}">{{ $from_funeral_director->fd_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>To FD Name</label>
                                <select class="full-width" data-placeholder="Select To FD Name" data-init-plugin="select2" name="to_fd_id">
                                    <option value="{{ old('to_fd_id') }}" ></option>
                                    @foreach($to_funeral_directors as $to_funeral_director )
                                        <option value="{{ $to_funeral_director->id }}">{{ $to_funeral_director->fd_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="full-width" data-placeholder="Select Status" data-init-plugin="select2" name="status">
                                    <option value="{{ old('status') }}" ></option>
                                    <option value="unapproved" >Unapproved</option>
                                    <option value="approved" >Approved</option>
                                    <option value="rejected" >Rejected</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-info pull-right" type="submit"><span class="fa fa-refresh"></span>&nbsp;Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- START card -->
        <div class="card card-white card-shadow card-special">
            <div class="card-header ">
                <div class="card-title">Search Results</div>
                <div class="clearfix"></div>
            </div>

            @if(count($ld_transfer_requests)==0)
                <div class="card-block text-center">No Results Available</div>
            @else

            <div class="card-block">

                <table class="table table-hover demo-table-search table-responsive-block">

                    <thead>
                        <tr>
                            <th>Requested Date</th>
                            <th>From FD</th>
                            <th>To FD</th>
                            <th>Transfer Amount</th>
                            <th>Status</th>
                            <th>Approve</th>
                            <th>Reject</th>
                        </tr>
                    </thead>

                    <tbody>

                    @foreach($ld_transfer_requests as $ld_transfer_request)

                        <tr class="gradeA">
                            <td>{{ date('d-m-Y H:i:s', strtotime($ld_transfer_request->requested_date)) }}</td>
                            <td>{{ $ld_transfer_request->fd1_name }}</td>
                            <td>{{ $ld_transfer_request->fd2_name }}</td>
                            <td>{{ $ld_transfer_request->transfer_amount }}</td>
                            <td>
                                @if($ld_transfer_request->status == 'unapproved')
                                    <span class="label label-warning">Unapproved</span>
                                @elseif($ld_transfer_request->status == 'approved')
                                    <span class="label label-success">Approved</span>
                                @elseif($ld_transfer_request->status == 'rejected')
                                    <span class="label label-danger">Rejected</span>
                                @else
                                    <span class="label bg-black">Undefined</span>
                                @endif
                            </td>
                            <td>
                                @if($ld_transfer_request->status == 'unapproved')
                                    <a href="#" class="btn btn-xs btn-success approve-button" role="button" data-from-fd-timestamp="{{ $ld_transfer_request->from_fd_timestamp }}" data-to-fd-timestamp="{{ $ld_transfer_request->to_fd_timestamp }}" data-approve-link="{{ action('FuneralDirectorController@approveLoyaltyDollarTransferRequest',$ld_transfer_request->id) }}" data-toggle="modal" data-target="#modal-approve"><span class="fa fa-check"></span>&nbsp;Approve</a>
                                @endif
                            </td>
                            <td>
                                @if($ld_transfer_request->status == 'unapproved')
                                    <a href="#" class="btn btn-xs btn-danger reject-button" role="button" data-reject-link="{{ action('FuneralDirectorController@rejectLoyaltyDollarTransferRequest',$ld_transfer_request->id) }}" data-toggle="modal" data-target="#modal-reject"><span class="fa fa-times"></span>&nbsp;Reject</a>
                                @endif
                            </td>
                        </tr>

                    @endforeach

                    </tbody>

                </table>

            </div>

            @endif

        </div>
        <!-- END card -->

        <div class="row pagination-div">
            <div class="col-md-12">
                <div class="pull-right">
                    {!! $ld_transfer_requests->links() !!}
                </div>
            </div>
        </div>

        <div class="modal fade slide-down disable-scroll" id="modal-approve" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog modal-md" style="width: 100%;">
                <div class="modal-content-wrapper">
                    <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i></button>
                            <h5> <span class="semi-bold">Do you want to Approve this record ?</span></h5>
                            <p class="p-b-10"></p>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="" id="approve-form" role="form">
                                @csrf
                                <input type="hidden" name="from_fd_timestamp" value="" id="from_fd_timestamp" />
                                <input type="hidden" name="to_fd_timestamp" value="" id="to_fd_timestamp" />
                                <button type="submit" class="btn btn-success btn-cons pull-left inline">Approve</button>
                                <button type="button" class="btn btn-default btn-cons no-margin pull-left inline" data-dismiss="modal">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade slide-down disable-scroll" id="modal-reject" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog modal-md" style="width: 100%;">
                <div class="modal-content-wrapper">
                    <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i></button>
                            <h5> <span class="semi-bold">Do you want to Reject this record ?</span></h5>
                            <p class="p-b-10"></p>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="" id="reject-form" role="form">
                                @csrf
                                <button type="submit" class="btn btn-danger btn-cons pull-left inline">Reject</button>
                                <button type="button" class="btn btn-default btn-cons no-margin pull-left inline" data-dismiss="modal">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script>
        //$(".approve").on("submit", function(){
        //    return confirm("Do you want to Approve this record ?");
        //});
        //$(".reject").on("submit", function(){
        //    return confirm("Do you want to Reject this record ?");
        //});
        $('.approve-button').on('click', function () {
            $('#from_fd_timestamp').val($(this).data('from-fd-timestamp'));
            $('#to_fd_timestamp').val($(this).data('to-fd-timestamp'));
            $('#approve-form').attr('action', $(this).data('approve-link'));
        });

        $('.reject-button').on('click', function () {
            $('#reject-form').attr('action', $(this).data('reject-link'));
        });
    </script>

@endsection