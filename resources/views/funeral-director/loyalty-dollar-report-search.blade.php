@extends('layouts.master')

@section('title', 'Generate Loyalty $ Report')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('point_reports.index') }}">Loyalty $ Reports</a></li>
                    <li class="breadcrumb-item active">Generate Loyalty $ Reports</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('point_reports.index') }}" class="fa fa-angle-left go-back"></a> Generate Loyalty $ Report</div>
        </div>
        
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="card card-white card-shadow card-special">
            <div class="card-block">
                <form method="POST" action="{{ action('PointReportController@viewPointReportsFD') }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="row clearfix">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>From Date</label>
                                <input type="text" class="form-control" placeholder="Select From Date" name="from_date" id="from_date">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>To Date</label>
                                <input type="text" class="form-control" placeholder="Select To Date" name="to_date" id="to_date">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="fd_id" id="fd_id" value="{{ $fd_id }}">
                            <button class="btn btn-primary pull-right" type="submit" name="action">Generate Report</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- START card -->
        @if(!isset($results))
            
        @else
        
            <div class="card card-white card-shadow card-special">
                <div class="card-header">
                    <div class="pull-left">
                        <div class="report-title">Loyalty $ Report</div>
                        <div class="report-details">
                            <table style="border:none;">
                                @php if($funeral_director_name != ""){ @endphp
                                    <tr>
                                        <td>Funeral Director</td>
                                        <td>:</td>
                                        <td><b>{{ $funeral_director_name }}</b></td>
                                    </tr>
                                @php } @endphp
                                @php if($from_date_pdf != ""){ @endphp
                                    <tr>
                                        <td>From Date</td>
                                        <td>:</td>
                                        <td><b>{{ $from_date_pdf }}</b></td>
                                    </tr>
                                @php } @endphp
                                @php if($to_date_pdf != ""){ @endphp
                                    <tr>
                                        <td>To Date</td>
                                        <td>:</td>
                                        <td><b>{{ $to_date_pdf }}</b></td>
                                    </tr>
                                @php } @endphp
                                @php if($available_loyalty_dollars != ""){ @endphp
                                    <tr>
                                        <td>Available L$</td>
                                        <td>:</td>
                                        <td><b>{{ $available_loyalty_dollars }}</b></td>
                                    </tr>
                                @php } @endphp
                            </table>
                        </div>
                    </div>
                    <div class="pull-right">
                        @php
                        if($funeral_director_pdf == "" || $funeral_director_pdf == null){
                            $funeral_director_pdf = "0";
                        }
                        if($from_date_pdf == "" || $from_date_pdf == null){
                            $from_date_pdf = "0000-00-00";
                        }
                        if($to_date_pdf == "" || $to_date_pdf == null){
                            $to_date_pdf = "0000-00-00";
                        }
                        @endphp
                        <a class="btn btn-info btn-xs pull-right" href="{{ route('generate_point_reports_pdf',array('fd' => $funeral_director_pdf, 'from' => $from_date_pdf, 'to' => $to_date_pdf) ) }}"><span class="fa fa-download"></span>&nbsp;Download PDF</a>
                    </div>
                </div>
                
                @if(count($results)==0)
                    <div class="card-block text-center">No Results Available</div>
                @else

                <div class="card-block">
                    <table class="table table-hover demo-table-search table-responsive-block" id="reward_points_table">
                        <thead>
                            <tr>
                                <th>Funeral Director</th>
                                <th>Date</th>
                                <th>Type</th>
                                <th>Doc No</th>
                                <th>Doc Date</th>
                                <th>Item Number</th>
                                <th>Points</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($results as $result)

                            <tr class="gradeA">
                                <td>{{ $result->fd_name }}</td>
                                <td>{{ date('d-m-Y', strtotime($result->trans_date)) }}</td>
                                <td>{{ $result->trans_type }}</td>
                                <td>{{ $result->doc_number }}</td>
                                <td>{{ date('d-m-Y', strtotime($result->doc_date)) }}</td>
                                <td>{{ $result->item_number }}</td>
                                <td>{{ $result->points }}</td>
                            </tr>

                        @endforeach

                        </tbody>
                    </table>

                </div>

                @endif

            </div>

            <div class="row pagination-div">
                <div class="col-md-12">
                    <div class="pull-right">
                        {!! $results->links() !!}
                    </div>
                </div>
            </div>

        @endif
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script type="text/javascript">

        //jQuery('#reward_points_table').dataTable({
        //    "bLengthChange":false,
        //    "info":false,
        //    "bPaginate": false
        //});

        $("#from_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });

        $("#to_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });

    </script>

@endsection