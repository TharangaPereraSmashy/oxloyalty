@extends('layouts.master')

@section('title', 'Funeral Director Dashboard')

@section('content')

    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid  container-fixed-lg">
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="card card-white card-special m-t-20 feature-card">
            <div class="card-block ">

                <div class="padding-50 sm-padding-10">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="fd-logo-holder">
                                @if($funeral_director->logo!="")
                                    <img width="100%" height="auto" alt="" class="img-rounded img-responsive fd-logo" data-src-retina="/images/funeral_director_logos/{{ $funeral_director->logo }}" data-src="/images/funeral_director_logos/{{ $funeral_director->logo }}" src="/images/funeral_director_logos/{{ $funeral_director->logo }}">
                                @else
                                    <img width="100%" height="auto" alt="" class="img-rounded img-responsive fd-logo" data-src-retina="/images/no_image.jpg" data-src="/images/no_image.jpg" src="/images/no_image.jpg">
                                @endif
                            </div>
                            <div class="fd-info pull-left">
                                <p class="fd-name semi-bold">{{ $funeral_director->name }} ( <span class="fd-code"> {{ $funeral_director->code }} </span> )
                                    @if($funeral_director->is_on_hold == 0)
                                        <span class="label label-success fd-hold">ACTIVE</span>
                                    @elseif($funeral_director->is_on_hold == 1)
                                        <span class="label label-important fd-hold">INACTIVE</span>
                                    @endif
                                </p>
                                <p class="fd-address">
                                    @if($funeral_director->address1 != "")
                                        {{ $funeral_director->address1 }},
                                    @endif
                                    @if($funeral_director->address2 != "")
                                        {{ $funeral_director->address2 }},
                                    @endif
                                    @if($funeral_director->postcode != "")
                                        {{ $funeral_director->postcode }}
                                    @endif
                                    @if($funeral_director->address1 != "")
                                        <br>
                                    @endif
                                    @if($funeral_director->suburb != "")
                                        {{ $funeral_director->suburb }},
                                    @endif
                                    @if($funeral_director->state_name != "")
                                        {{ $funeral_director->state_name }},
                                    @endif
                                    @if($funeral_director->country_name != "")
                                        {{ $funeral_director->country_name }}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="col-md-3 text-right">
                            @if($funeral_director->is_parent==0 && $funeral_director->parent_id!="")
                                @if(Auth::user()->fd_id!=$funeral_director->id)
                                    <a class="btn btn-sm btn-info" href="{{ route('funeral_directors.show',$funeral_director->parent_id) }}">VIEW PARENT FD ACCOUNT</a>
                                @endif
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-3 hide">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pull-left">
                                        @if($funeral_director->logo!="")
                                            <img width="100%" height="auto" alt="" class="img-rounded img-responsive fd-logo" data-src-retina="/images/funeral_director_logos/{{ $funeral_director->logo }}" data-src="/images/funeral_director_logos/{{ $funeral_director->logo }}" src="/images/funeral_director_logos/{{ $funeral_director->logo }}">
                                        @else
                                            <img width="100%" height="auto" alt="" class="img-rounded img-responsive fd-logo" data-src-retina="/images/no_image.jpg" data-src="/images/no_image.jpg" src="/images/no_image.jpg">
                                        @endif
                                    </div>
                                </div>
                                
                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-xl-4 col-lg-12">
                                    <div class="card card-light-blue fd-highlight card-special card-shadow text-center" style="height: 100%;">
                                        <div class="card-header ">
                                            <div class="card-title">Available Loyalty Dollars</div>
                                        </div>
                                        <div class="card-block">
                                            <h1>${{ $available_loyalty_dollars}}</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-12">
                                    <div class="card card-light-yellow  fd-highlight card-special card-shadow text-center" style="height: 100%;">
                                        <div class="card-header ">
                                            <div class="card-title">Earned Loyalty Dollars</div>
                                        </div>
                                        <div class="card-block">
                                            <h1>${{ $earned_loyalty_dollars[0]->earned }}</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-12">
                                    <div class="card  card-pale-red fd-highlight card-special card-shadow text-center" style="height: 100%;">
                                        <div class="card-header ">
                                            <div class="card-title">L$ due to Expire in 30 days</div>
                                        </div>
                                        <div class="card-block">
                                            <h1>${{ $expired_loyalty_dollars[0]->expired }}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-white card-shadow card-special">   
                                <div class="card-header  d-flex justify-content-between">
                                    <div class="card-title">Combined Offers</div>
                                </div>                        
                                <div class="card-block">
                                    <table class="table table-hover demo-table-dynamic table-responsive-block" id="item_loyalty_detail_table">
                                        <thead>
                                            <tr style="border-bottom: 1px solid rgba(218, 222, 224, 0.7);">
                                                <td colspan="4" style="border:0">Loyalty Dollars</td>
                                                <td colspan="3" style="border:0; text-align:center">Special Offers</td>
                                            </tr>
                                            <tr>
                                                <th>Item</th>
                                                <th>Cemetery</th>
                                                <th>Earns L$</th>
                                                <th>To Redeem</th>
                                                <th style="color: #faa731;">Offer expires</th>
                                                <th style="color: #faa731;">Earns L$</th>
                                                <th style="color: #faa731;">To Redeem</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            @foreach($item_loyalty_details as $result)

                                                <tr class="odd gradeX">
                                                    <td>{{ $result->item }}</td>
                                                    <td>{{ $result->cemetery }}</td>
                                                    <td>{{ $result->ci_to_earn }}</td>
                                                    <td>{{ $result->ci_to_redeem }}</td>
                                                    <td>{{ $result->expiry_date }}</td>
                                                    <td>{{ $result->so_to_earn }}</td>
                                                    <td>{{ $result->so_to_redeem }}</td>
                                                </tr>
    
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    @if(!($funeral_director->is_parent==0 && count($parent_special_offer_details)!=0))
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-white card-shadow card-special">    
                                <div class="card-header  d-flex justify-content-between">
                                    <div class="card-title">Special Offers</div>
                                </div>        
                                @if(count($special_offer_details)==0)
                                    <div class="card-block text-center">No Results Available</div>
                                @else               
                                <div class="card-block">
                                    <table class="table table-hover demo-table-dynamic table-responsive-block" id="special_offer_detail_table">
                                        <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Cemetery</th>
                                                <th style="color: #faa731;">Offer expires</th>
                                                <th style="color: #faa731;">Earns L$</th>
                                                <th style="color: #faa731;">To Redeem</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            @foreach($special_offer_details as $result)

                                                <tr class="odd gradeX">
                                                    <td>{{ $result->item }}</td>
                                                    <td>{{ $result->cemetery }}</td>
                                                    <td>{{ $result->expiry_date }}</td>
                                                    <td>{{ $result->so_to_earn }}</td>
                                                    <td>{{ $result->so_to_redeem }}</td>
                                                </tr>
    
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endif

                    @if($funeral_director->is_parent==0 && !is_null($funeral_director->parent_id))
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-white card-shadow card-special">    
                                <div class="card-header  d-flex justify-content-between">
                                    <div class="card-title">Parent&apos;s Special Offers</div>
                                </div>    
                                @if(count($parent_special_offer_details)==0)
                                    <div class="card-block text-center">No Results Available</div>
                                @else                      
                                <div class="card-block">
                                    <table class="table table-hover demo-table-dynamic table-responsive-block" id="parent_special_offer_detail_table">
                                        <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Cemetery</th>
                                                <th style="color: #faa731;">Offer expires</th>
                                                <th style="color: #faa731;">Earns L$</th>
                                                <th style="color: #faa731;">To Redeem</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            @foreach($parent_special_offer_details as $result)

                                                <tr class="odd gradeX">
                                                    <td>{{ $result->item }}</td>
                                                    <td>{{ $result->cemetery }}</td>
                                                    <td>{{ $result->expiry_date }}</td>
                                                    <td>{{ $result->so_to_earn }}</td>
                                                    <td>{{ $result->so_to_redeem }}</td>
                                                </tr>
    
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endif
                    <hr>
                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-white card-special card-shadow dashboard-fd-child">
                                <div class="card-header  d-flex justify-content-between">
                                    <div class="card-title">Service Requests</div>
                                </div>
                                 

                                <div class="card-block fd-service-request">
                                    <div class="row footer-buttons">
                                        @if($funeral_director->is_parent == 0)
                                            @if(Auth::user()->type=="1" || Auth::user()->type=="2")
                                                <div class="col-lg-12">
                                                    @include('funeral-director.loyalty-dollar-adjustment')
                                                    <button class="btn btn-lg btn-complete" id="btn-loyalty-dollar-adjustment" style="white-space: normal;">Loyalty Dollar Adjustment</button>
                                                </div>
                                            @endif
                                            @if($funeral_director->parent_id!="")
                                                <div class="col-lg-12 text-right">
                                                    @include('funeral-director.loyalty-dollar-transfer-request')
                                                    <button class="btn btn-lg btn-success" id="btn-loyalty-dollar-transfer" style="white-space: normal;">Loyalty Dollar Transfer Request</button>
                                                </div>
                                            @endif
                                        @endif
                                        <div class="col-lg-12">
                                            <a href="{{ route('view.point.reports.fd',array('fd_id' => $funeral_director->id)) }}" class="btn btn-lg btn-danger" style="white-space: normal;">Generate Loyalty $ Report</a>
                                        </div>
                                        <!--<div class="col-lg-12">
                                            <button class="btn btn-lg btn-success" style="white-space: normal;">View Branch & Transaction Details</button>
                                        </div>-->
                                        <!--<div class="col-lg-12">
                                            <button class="btn btn-lg btn-complete" style="white-space: normal;">Request Point Transfer between Branches</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    @if($funeral_director->is_parent==1)
                        <div class="col-md-8">
                            <div class="card card-white card-special card-shadow dashboard-fd-child">
                                <div class="card-header  d-flex justify-content-between">
                                    <div class="card-title">Child information</div>
                                </div>

                                @if(count($childFDs)==0)
                                    <div class="card-block text-center">No Results Available</div>
                                @else    

                                <div class="card-block">
                        
                                    <table class="table table-hover demo-table-dynamic table-responsive-block" id="child_fds_table">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>L$ </th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            @foreach($childFDs as $childFD)

                                                <tr class="odd gradeX">
                                                    <td>{{ $childFD->code }}</td>
                                                    <td>{{ $childFD->name }}</td>
                                                    <td>{{ $childFD->available_points}}</td>
                                                    <td><a class="btn btn-xs btn-complete" href="{{ route('funeral_directors.show',$childFD->id) }}"><span class="fa fa-search"></span>&nbsp;View</a></td>
                                                </tr>

                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>

                                @endif    

                            </div>
                        </div>

                        @endif
                    </div>

                </div>
            </div>
        </div>
        <!-- END card -->
       
    </div>
    <!-- END CONTAINER FLUID -->

@endsection

@section('script')

    <script type="text/javascript">

        jQuery('#child_fds_table').dataTable({
            "bLengthChange":false,
            "info":false,
            "columnDefs": [
                { "orderable": false, "targets": 3 }
            ],
            "pageLength": 5
        });

        jQuery('#item_loyalty_detail_table').dataTable({
            "bLengthChange":false,
            "info":false,
            "pageLength": 5
        });

        $(document).ready(function() {

            $('#btn-loyalty-dollar-adjustment').click(function() {
                $('#loyalty-dollar-adjustment-modal').modal('show')
            });


            $('#btn-loyalty-dollar-transfer').click(function() {
                $('#loyalty-dollar-transfer-modal').modal('show')
            });


            $('#ld_transfer_req_form').submit(function (){

                var valid_all = true;

                var from_fd_id  = $('#from_fd_id').val();
                if (from_fd_id == "") {
                    $('#from_fd_id').css('background', '#ffdedd').css('border-color', '#ff5b57');
                    $('#from_fd_id_err').html('Please Select From FD.').css('color', '#F00').css('font-size', '11px');
                    valid_all = false;
                }else{
                    $('#from_fd_id').css('background', '').css('border-color', '');
                    $('#from_fd_id_err').html('').css('color', '').css('font-size', '');
                }

                var to_fd_id    = $('#to_fd_id').val();
                if (to_fd_id == "") {
                    $('#to_fd_id').css('background', '#ffdedd').css('border-color', '#ff5b57');
                    $('#to_fd_id_err').html('Please Select To FD.').css('color', '#F00').css('font-size', '11px');
                    valid_all = false;
                }else{
                    $('#to_fd_id').css('background', '').css('border-color', '');
                    $('#to_fd_id_err').html('').css('color', '').css('font-size', '');
                }

                if (from_fd_id != "" && to_fd_id != "") {
                    if (from_fd_id == to_fd_id) {
                        $('#from_fd_id').css('background', '#ffdedd').css('border-color', '#ff5b57');
                        $('#to_fd_id').css('background', '#ffdedd').css('border-color', '#ff5b57');
                        $('#fd_id_err').html('Sorry you can not transfer between same account.').css('color', '#F00').css('font-size', '11px');
                        valid_all = false;
                    }else{
                        $('#from_fd_id').css('background', '').css('border-color', '');
                        $('#to_fd_id').css('background', '').css('border-color', '');
                        $('#fd_id_err').html('').css('color', '').css('font-size', '');
                    }
                }

                var transfer_amount_val = $('#transfer_amount').val();
                var transfer_amount     = parseInt($('#transfer_amount').val());
                var from_fd_ld_balance  = parseInt($('#from_fd_ld_balance').val());

                if (transfer_amount_val == "") {
                    $('#transfer_amount').css('background', '#ffdedd').css('border-color', '#ff5b57');
                    $('#transfer_amount_err1').html('Please Enter Transfer Amount.').css('color', '#F00').css('font-size', '11px');
                    $('#transfer_amount_err2').html('').css('color', '').css('font-size', '');
                    valid_all = false;
                }else if (transfer_amount == 0) {
                    $('#transfer_amount').css('background', '#ffdedd').css('border-color', '#ff5b57');
                    $('#transfer_amount_err1').html('Transfer Amount should be greater than 0.').css('color', '#F00').css('font-size', '11px');
                    $('#transfer_amount_err2').html('').css('color', '').css('font-size', '');
                    valid_all = false;
                }else if (transfer_amount > from_fd_ld_balance) {
                    $('#transfer_amount').css('background', '#ffdedd').css('border-color', '#ff5b57');
                    $('#transfer_amount_err2').html('Transfer Amount Exceeds Available Loyalty Dollars.').css('color', '#F00').css('font-size', '11px');
                    $('#transfer_amount_err1').html('').css('color', '').css('font-size', '');
                    valid_all = false;
                }else{
                    $('#transfer_amount').css('background', '').css('border-color', '');
                    $('#transfer_amount_err1').html('').css('color', '').css('font-size', '');
                    $('#transfer_amount_err2').html('').css('color', '').css('font-size', '');
                }

                return valid_all;

            });

            $('#ld_adjustment_form').submit(function (){

                var valid_all = true;

                var adjustment_amount           =   $('#adjustment_amount').val();
                var available_loyalty_dollars   =   $('#available_loyalty_dollars').val();
              
                if (adjustment_amount == "") {
                    $('#adjustment_amount').css('background', '#ffdedd').css('border-color', '#ff5b57');
                    $('#adjustment_amount_err1').html('Please Enter Adjustment Amount.').css('color', '#F00').css('font-size', '11px');
                    $('#adjustment_amount_err2').html('').css('color', '').css('font-size', '');
                    valid_all = false;
                }else if (adjustment_amount < 0) {
                    if(available_loyalty_dollars == 0){
                        $('#adjustment_amount').css('background', '#ffdedd').css('border-color', '#ff5b57');
                        $('#adjustment_amount_err2').html('Available Loyalty Dollars Exceeds the given value.').css('color', '#F00').css('font-size', '11px');
                        $('#adjustment_amount_err1').html('').css('color', '').css('font-size', '');
                        valid_all = false;
                    }
                }else{
                    $('#adjustment_amount').css('background', '').css('border-color', '');
                    $('#adjustment_amount_err1').html('').css('color', '').css('font-size', '');
                    $('#adjustment_amount_err2').html('').css('color', '').css('font-size', '');
                }

                return valid_all;

            });

            jQuery('select[name="from_fd_id"]').change(function(){

                var from_fd_id = jQuery('select[name="from_fd_id"]').val();

                getLdBalanceByFromFdId(from_fd_id);

            });


            jQuery('select[name="to_fd_id"]').change(function(){

                var to_fd_id = jQuery('select[name="to_fd_id"]').val();

                getLdBalanceByToFdId(to_fd_id);

            });

        });

        function getLdBalanceByFromFdId(id){

            jQuery('input[name="from_fd_ld_balance"]').val('');

            jQuery.ajax({
                url: '/funeral_directors/get_ld_balance_by_fd_id/'+id,
                type: 'GET',
                dataType: "json",
                success: function(data){
                    console.log(data);
                    jQuery('input[name="from_fd_ld_balance"]').val(data);
                }
            });

        }

        function getLdBalanceByToFdId(id){

            jQuery('input[name="to_fd_ld_balance"]').val('');

            jQuery.ajax({
                url: '/funeral_directors/get_ld_balance_by_fd_id/'+id,
                type: 'GET',
                dataType: "json",
                success: function(data){
                    jQuery('input[name="to_fd_ld_balance"]').val(data);
                }
            });

        }

    </script>

@endsection