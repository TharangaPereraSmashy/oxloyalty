@extends('layouts.master')

@section('title', 'Loyalty Dollar Revaluation Requests')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Loyalty Dollar Revaluation Requests</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="#" class="fa fa-angle-left go-back"></a>Loyalty Dollar Revaluation Request</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="card card-white card-shadow card-special">
            <div class="card-block">
                <form method="POST" action="{{ route('create.revaluation.request') }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    @if($current_rate!="")
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label>Current Rate</label>
                                    <input type="text" class="form-control" value="{{ $current_rate->new_rate }}" placeholder="Current Rate" disabled>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(Session::get('create_ld_revaluation_request'))
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label>New Rate</label>
                                    <input type="text" class="form-control float-only" maxlength="8" value="{{ old('new_rate') }}" id="new_rate" placeholder="New Rate" name="new_rate" required>
                                    @if ($errors->has('new_rate'))
                                        <div class="error">{{ $errors->first('new_rate') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-group-default">
                                    <button class="btn btn-info pull-left" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- START card -->
        <div class="card card-white card-shadow card-special">
            <div class="card-header ">
                <div class="card-title">Loyalty Dollar Revaluation History</div>
                <div class="clearfix"></div>
            </div>

            @if(count($ld_revaluation_requests)==0)
                <div class="card-block text-center">No Results Available</div>
            @else

            <div class="card-block">

                <table class="table table-hover demo-table-search table-responsive-block" id="ld_revaluation_requests_table">

                    <thead>
                        <tr>
                            <th>Requested Date</th>
                            <th>Requested By</th>
                            <th>Rate</th>
                            <th>Processed Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>

                    @foreach($ld_revaluation_requests as $ld_revaluation_request)

                        <tr class="gradeA">
                            <td>{{ date('d-m-Y H:i:s', strtotime($ld_revaluation_request->requested_date)) }}</td>
                            <td>{{ $ld_revaluation_request->user_name }}</td>
                            <td>{{ $ld_revaluation_request->new_rate }}</td>
                            @php
                            $processed_date =   "";
                            if($ld_revaluation_request->processed_date != ""){
                                $processed_date =   date('d-m-Y H:i:s', strtotime($ld_revaluation_request->processed_date));
                            }else{
                                $processed_date =   "";
                            }
                            @endphp
                            <td>{{ $processed_date }}</td>
                            <td>
                                @if($ld_revaluation_request->is_processed == 0)
                                    <span class="label label-warning">New</span>
                                @elseif($ld_revaluation_request->is_processed == 1)
                                    <span class="label label-success">Processed</span>
                                @else
                                    <span class="label bg-black">Undefined</span>
                                @endif
                            </td>
                            <td>
                                @if($ld_revaluation_request->is_processed == 0)
                                    <form action="{{ route('delete.revaluation.request',$ld_revaluation_request->id) }}" method="POST" class="delete_record">
                                        @if(Session::get('update_ld_revaluation_request'))
                                            <a href="#" class="btn btn-xs btn-success btn-rounded update-button" role="button" data-current-rate="{{ $ld_revaluation_request->new_rate }}" data-update-link="{{ action('FuneralDirectorController@updateLoyaltyDollarRevaluationRequest',$ld_revaluation_request->id) }}" data-toggle="modal" data-target="#modal-update"><span class="fa fa-edit"></span>&nbsp;Edit</a>
                                        @endif
                                        @csrf
                                        @if(Session::get('delete_ld_revaluation_request'))
                                            <button type="submit" class="btn btn-xs btn-rounded btn-danger"><span class="fa fa-trash"></span>&nbsp;Delete</button>
                                        @endif
                                    </form>
                                @endif
                            </td>
                        </tr>

                    @endforeach

                    </tbody>

                </table>

            </div>

            @endif

        </div>
        <!-- END card -->

        <div class="modal fade slide-down disable-scroll" id="modal-update" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog modal-md" style="width: 100%;">
                <div class="modal-content-wrapper">
                    <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i></button>
                            <h5> <span class="semi-bold">Update Loyalty Dollar Revaluation Request</span></h5>
                            <p class="p-b-10"></p>
                        </div>
                        <form method="POST" action="" id="update-form" role="form" autocomplete="off">
                        @csrf
                            <div class="modal-body">
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Rate</label>
                                            <input type="text" class="form-control float-only" maxlength="8" value="{{ old('updated_rate') }}" id="updated_rate" placeholder="New Rate" name="updated_rate" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success btn-cons pull-left inline">Update</button>
                                        <button type="button" class="btn btn-default btn-cons no-margin pull-left inline" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script type="text/javascript">

        jQuery('#ld_revaluation_requests_table').dataTable({
            "bLengthChange":false,
            "info":false,
            "columnDefs": [
                { "orderable": false, "targets": 5 }
            ],
            "pageLength": 5,
            "order": [[ 0, "desc" ]]
        });

        $('.update-button').on('click', function () {
            $('#updated_rate').val($(this).data('current-rate'));
            $('#update-form').attr('action', $(this).data('update-link'));
        });

    </script>

@endsection