<div class="modal fade slide-up disable-scroll" id="loyalty-dollar-adjustment-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5> <span class="semi-bold">Loyalty Dollar Adjustment</span></h5>
                    <p class="p-b-10"></p>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ action('FuneralDirectorController@setLoyaltyDollarAdjustment') }}" id="ld_adjustment_form" role="form">
                        @csrf
                        <input type="hidden" id="fd_id" name="fd_id" value ="{{ $funeral_director->id }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-group-default">
                                    <label>Adjustment Amount</label>
                                    <input type="text" class="form-control int-plus-minus-only" name="adjustment_amount" id="adjustment_amount" placeholder="Adjustment Amount" value="">
                                    <span id="adjustment_amount_err1"></span>
                                    <span id="adjustment_amount_err2"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4 m-t-10 sm-m-t-10">
                                    <input type="hidden" name="available_loyalty_dollars" id="available_loyalty_dollars" value="{{ $available_loyalty_dollars }}">
                                    <button type="submit" class="btn btn-primary btn-block m-t-5">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
