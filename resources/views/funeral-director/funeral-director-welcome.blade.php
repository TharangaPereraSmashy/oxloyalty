@extends('layouts.master')

@section('title', 'Funeral Director Welcome')

@section('content')

    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid  container-fixed-lg">
        <!-- START card -->
        <div class="card card-white card-special m-t-20  feature-card">
            <div class="card-block">

                <div class="padding-50 sm-padding-10">

                    <div class="row">
                        <div class="col-lg-3">
                            <div class="pull-left">
                                @if($funeral_director->logo!="")
                                    <img width="100%" height="auto" alt="" class="img-rounded img-responsive" data-src-retina="/images/funeral_director_logos/{{ $funeral_director->logo }}" data-src="/images/funeral_director_logos/{{ $funeral_director->logo }}" src="/images/funeral_director_logos/{{ $funeral_director->logo }}">
                                @else
                                    <img width="100%" height="auto" alt="" class="img-rounded img-responsive" data-src-retina="/images/no_image.jpg" data-src="/images/no_image.jpg" src="/images/no_image.jpg">
                                @endif
                            </div>
                        </div>

                        <div class="col-lg-9">
                            <div class="pull-right">
                                <h2><span class="semi-bold">Welcome</span> {{ $funeral_director->name }} !</h2>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <hr>

                    <br>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-white card-shadow card-special">
                                {{--  <div class="card-header  d-flex justify-content-between">
                                    <div class="card-title bold">Loyalty $ Available : {{ $funeral_director->loyalty_dollar_balance }}</div>
                                </div>  --}}
                                <div class="card-block">
                                    <table class="table table-hover demo-table-dynamic table-responsive-block">
                                        <thead>
                                        <tr>
                                            <th colspan="4" class="bold"><h5>Loyalty $ Available : {{ $available_loyalty_dollars }}</h5></th>
                                            <th colspan="3" class="bold"><h5>Special Offers</h5></th>
                                        </tr>
                                        <tr>
                                            <th>Item</th>
                                            <th>Cemetery</th>
                                            <th>Earns L$</th>
                                            <th>To Redeem</th>
                                            <th>Offer expires</th>
                                            <th>Earns L$</th>
                                            <th>To Redeem</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($results as $result)

                                            <tr class="odd gradeX">
                                                <td>{{ $result->item }}</td>
                                                <td>{{ $result->cemetery }}</td>
                                                <td>{{ $result->ci_to_earn }}</td>
                                                <td>{{ $result->ci_to_redeem }}</td>
                                                <td>{{ $result->expiry_date }}</td>
                                                <td>{{ $result->so_to_earn }}</td>
                                                <td>{{ $result->so_to_redeem }}</td>
                                            </tr>

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--  <div class="row">
                        <div class="card card-transparent">
                            <div class="card-header  d-flex justify-content-between">
                                <div class="card-title bold">Special Offers</div>
                            </div>
                            <div class="card-block">
                                <table class="table table-hover demo-table-dynamic table-responsive-block">
                                    <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Cemetry</th>
                                        <th>Offer expires</th>
                                        <th>To Earn</th>
                                        <th>To Redeem</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($special_offers as $special_offer)

                                        <tr class="odd gradeX">
                                            <td>{{ $special_offer->item }}</td>
                                            <td>{{ $special_offer->cemetery }}</td>
                                            <td>{{ $special_offer->end_date }}</td>
                                            <td>{{ $special_offer->to_earn }}</td>
                                            <td>{{ $special_offer->to_redeem }}</td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>  --}}
                </div>
            </div>
        </div>
        <!-- END card -->

        <div class="row footer-buttons">
            <!--<div class="col-lg-4">
                <h1><button class="btn btn-lg btn-primary">View Branch & Transaction Details</button></h1>
            </div>-->
            <div class="col-lg-4 text-center">
                <h1><a href="{{ route('view.point.reports.fd') }}" class="btn btn-lg btn-success">Generate Loyalty $ Report</a></h1>
            </div>
            <div class="col-lg-4 text-right">
                <h1><button class="btn btn-lg btn-complete">Request Point Transfer between Branches</button></h1>
            </div>
        </div>

    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection