<div class="modal fade slide-up disable-scroll" id="loyalty-dollar-transfer-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg" style="width: 100%;">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5> <span class="semi-bold">Loyalty Dollar Transfer Request</span></h5>
                    <p class="p-b-10"></p>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ action('FuneralDirectorController@createLoyaltyDollarTransferRequest') }}" id="ld_transfer_req_form" role="form">
                        @csrf
                        <div class="card card-white card-shadow card-special">
                            <div class="card-block">
                                <div class="row clearfix">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                            <label>From FD Name</label>
                                            <select name="from_fd_id" id="from_fd_id" style="width:100%;">
                                                <option value="" >-- Please Select --</option>
                                                @foreach($from_funeral_directors as $from_funeral_director )
                                                    <option value="{{ $from_funeral_director->id }}">{{ $from_funeral_director->fd_name }}</option>
                                                @endforeach
                                            </select>
                                            <span id="from_fd_id_err"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                            <label>To FD Name</label>
                                            <select name="to_fd_id" id="to_fd_id" style="width:100%;">
                                                <option value="" >-- Please Select --</option>
                                                @foreach($to_funeral_directors as $to_funeral_director )
                                                    <option value="{{ $to_funeral_director->id }}">{{ $to_funeral_director->fd_name }}</option>
                                                @endforeach
                                            </select>
                                            <span id="to_fd_id_err"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                            <label>From FD LD Balance</label>
                                            <input type="text" class="form-control" name="from_fd_ld_balance" id="from_fd_ld_balance" placeholder="From FD LD Balance" value="" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                            <label>To FD LD Balance</label>
                                            <input type="text" class="form-control" name="to_fd_ld_balance" id="to_fd_ld_balance" placeholder="To FD LD Balance" value="" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group form-group-default">
                                            <label>Transfer Amount</label>
                                            <input type="text" class="form-control int-only" name="transfer_amount" id="transfer_amount" placeholder="Transfer Amount" value="">
                                            <span id="transfer_amount_err1"></span>
                                            <span id="transfer_amount_err2"></span>
                                        </div>
                                    </div>
                                </div>
                                <span id="fd_id_err"></span>
                                <input type="hidden" id="fd_id" name="fd_id" value="{{ $funeral_director->id }}">
                                <input type="hidden" id="is_parent" name="is_parent" value="{{ $funeral_director->is_parent }}">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-8">
                                        </div>
                                        <div class="col-md-4 m-t-10 sm-m-t-10">
                                            <button type="submit" class="btn btn-primary btn-block m-t-5">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
