@extends('layouts.master')


@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('funeral_directors.index') }}">Funeral Director</a></li>
                    <li class="breadcrumb-item active">All Funeral Directors</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="card card-transparent">
            <div class="card-header ">
                <div class="card-title">Search Form</div>
                <div class="clearfix"></div>
            </div>
            <div class="card-block">
                <form method="POST" action="{{ action('FuneralDirectorController@searchFuneralDirectors') }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="row clearfix">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Funeral Director Code</label>
                                <input type="text" class="form-control" name="code" value="{{ old('code') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Funeral Director Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Funeral Director Email</label>
                                <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if(Session::get('create_funeral_director'))
                                <a class="btn btn-primary pull-left" href="{{ route('funeral_directors.create') }}"><span class="fa fa-plus"></span>&nbsp;Create New Funeral Director</a>
                            @endif
                            <button class="btn btn-info pull-right" type="submit"><span class="fa fa-refresh"></span>&nbsp;Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- START card -->
        <div class="card card-transparent">
            <div class="card-header ">
                <div class="card-title">Search Results</div>
                <div class="clearfix"></div>
            </div>

            @if(count($funeral_directors)==0)
                <div class="card-block text-center">No Results Available</div>
            @else

            <div class="card-block">

                <table class="table table-hover demo-table-search table-responsive-block">

                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>phone</th>
                            <th>Email</th>
                            <th>Is Parent</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    <tbody>

                    @foreach($funeral_directors as $funeral_director)

                        <tr class="gradeA">
                            <td>{{ $funeral_director->code }}</td>
                            <td>{{ $funeral_director->name }}</td>
                            <td>{{ $funeral_director->phone }}</td>
                            <td>{{ $funeral_director->email }}</td>
                            <td>
                                @if($funeral_director->is_parent == '1')
                                    <span class="label label-info">Yes</span>
                                @elseif($funeral_director->is_parent == '0')
                                    <span class="label label-danger">No</span>
                                @else
                                    <span class="label bg-black">Undefined</span>
                                @endif
                            </td>
                            <td>
                                <form action="{{ route('funeral_directors.destroy',$funeral_director->id) }}" method="POST">

                                    @if(Session::get('read_funeral_director'))
                                        <a class="btn btn-xs btn-complete" href="{{ route('funeral_directors.show',$funeral_director->id) }}"><span class="fa fa-search"></span>&nbsp;View</a>
                                    @endif

                                    @if(Session::get('update_funeral_director'))
                                        <a class="btn btn-xs btn-success" href="{{ route('funeral_directors.edit',$funeral_director->id) }}"><span class="fa fa-edit"></span>&nbsp;Edit</a>
                                    @endif

                                    @csrf
                                    @method('DELETE')

                                    @if(Session::get('delete_funeral_director'))
                                        <button type="submit" class="btn btn-xs btn-danger"><span class="fa fa-remove"></span>&nbsp;Delete</button>
                                    @endif

                                </form>
                            </td>
                        </tr>

                    @endforeach

                    </tbody>

                </table>

                {!! $funeral_directors->links() !!}

            </div>

            @endif

        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection