@extends('layouts.master')

@section('title', 'Add Funeral Director')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('funeral_directors.index') }}">Funeral Director</a></li>
                    <li class="breadcrumb-item active">Add Funeral Director</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('funeral_directors.index') }}" class="fa fa-angle-left go-back"></a> Add Funeral Director</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->        
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{ route('funeral_directors.store') }}" id="form-personal" role="form" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>Funeral Director Code</label>
                                        <input type="text" class="form-control" value="{{ old('code') }}" id="code" placeholder="Funeral Director Code" name="code" required>
                                        @if ($errors->has('code'))
                                            <div class="error">{{ $errors->first('code') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>Funeral Director Name</label>
                                        <input type="text" class="form-control" value="{{ old('name') }}" id="name" placeholder="Funeral Director Name" name="name" required>
                                        @if ($errors->has('name'))
                                            <div class="error">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>Logo</label>
                                        <input type="file" class="form-control" value="{{ old('logo') }}" id="logo" placeholder="Logo" name="logo">
                                        @if ($errors->has('logo'))
                                            <div class="error">{{ $errors->first('logo') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Address 1</label>
                                        <input type="text" class="form-control" value="{{ old('address1') }}" id="address1" placeholder="Address 1" name="address1">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Address 2</label>
                                        <input type="text" class="form-control" value="{{ old('address2') }}" id="address2" placeholder="Address 2" name="address2">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>City/Suburb</label>
                                        <input type="text" class="form-control" value="{{ old('suburb') }}" id="suburb" placeholder="City/Suburb" name="suburb">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Postcode</label>
                                        <input type="text" class="form-control" value="{{ old('postcode') }}" id="postcode" placeholder="Postcode" name="postcode">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Country</label>
                                        <select class="full-width" data-placeholder="Select Country" data-init-plugin="select2" name="country_id">
                                            <option value="{{ old('country_id') }}" ></option>
                                            @foreach($countries as $country )
                                                <option value="{{ $country->id }}">{{ $country->country_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>State</label>
                                        <select class="full-width" data-placeholder="Select State" data-init-plugin="select2" name="state_id">
                                            <option value="{{ old('state_id') }}" ></option>
                                            @foreach($states as $state )
                                                <option value="{{ $state->id }}">{{ $state->state_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" value="{{ old('phone') }}" id="phone" placeholder="Phone" name="phone">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="email" class="form-control" value="{{ old('email') }}" id="email" placeholder="Email" name="email">
                                        @if ($errors->has('email'))
                                            <div class="error">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Contact Person Name</label>
                                        <input type="text" class="form-control" value="{{ old('contact_person_name') }}" id="contact_person_name" placeholder="Contact Person Name" name="contact_person_name">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Is Parent</label>
                                        <div class="radio radio-success radio-padding">
                                            <div class="row">
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="1" name="is_parent" id="is_parent_yes">
                                                    <label for="is_parent_yes">Yes</label>
                                                </div>
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="0" name="is_parent" id="is_parent_no" checked="checked">
                                                    <label for="is_parent_no">No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Parent</label>
                                        <select class="full-width" data-placeholder="Select Parent" data-init-plugin="select2" name="parent_id">
                                            <option value="{{ old('parent_id') }}" ></option>
                                            @foreach($parents as $parent )
                                                <option value="{{ $parent->id }}">{{ $parent->fd_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Is On Hold</label>
                                        <div class="radio radio-success radio-padding">
                                            <div class="row">
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="1" name="is_on_hold" id="is_on_hold_yes">
                                                    <label for="is_on_hold_yes">Yes</label>
                                                </div>
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="0" name="is_on_hold" id="is_on_hold_no" checked="checked">
                                                    <label for="is_on_hold_no">No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>GL Liability Code</label>
                                        <input type="text" class="form-control" value="{{ old('gl_liability_code') }}" id="gl_liability_code" placeholder="GL Liability Code" name="gl_liability_code" required>
                                        @if ($errors->has('gl_liability_code'))
                                            <div class="error">{{ $errors->first('gl_liability_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>GL Cost Code</label>
                                        <input type="text" class="form-control" value="{{ old('gl_cost_code') }}" id="gl_cost_code" placeholder="GL Cost Code" name="gl_cost_code" required>
                                        @if ($errors->has('gl_cost_code'))
                                            <div class="error">{{ $errors->first('gl_cost_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix padding-bottom-20">
                        <div class="col-md-6">
                            <a class="btn btn-light btn-lg" href="{{ route('funeral_directors.index') }}">View all Funeral Directors</a>
                        </div>
                        <div class="col-md-6 text-right">
                            @if(Session::get('create_funeral_director'))
                                <button class="btn btn-primary btn-lg" type="submit">Add Funeral Director</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection