@extends('layouts.master')


@section('content')

    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid  container-fixed-lg">
        <!-- START card -->
        <div class="card card-default m-t-20">
            <div class="card-block padding-50 sm-padding-10">
                <div class="page-header">
                    <div class="card-title">Welcome <span class="bold">{{ Auth::user()->name }}</span> !</div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="card card-default" style="height: 100%;">
                            <div class="card-header separator">
                                <div class="card-title bold text-primary">Funeral Director</div>
                            </div>
                            <div class="card-block" style="padding-top:6%;">
                                <form action="#" class="form-inline" role="form" autocomplete="off" style="width:100%;">
                                    <div class="form-group col-lg-9 mb-2">
                                        <select class="full-width" data-placeholder="Select Funeral Director" data-init-plugin="select2" name="fd_id">
                                            <option value="{{ old('fd_id') }}" ></option>
                                            @foreach($funeral_directors as $funeral_director )
                                                <option value="{{ $funeral_director->id }}" @if($funeral_director->id==$fd_id) selected @endif>{{ $funeral_director->fd_name }}</option>
                                            @endforeach
                                        </select>
                                        <span id="fd_id_err"></span>
                                    </div>
                                    <button type="button" id="search1" class="btn btn-info mb-2"><span class="fa fa-refresh"></span>&nbsp;Search</button>
                                </form>
                                <form action="#" class="form-horizontal" style="padding-left:3%; padding-top:3%;" role="form" autocomplete="off">
                                    <div class="form-group row" style="border-bottom:none;">
                                        <label class="col-md-5 control-label">Loyalty $ Available</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" value="" id="available_loyalty_dollars" name="available_loyalty_dollars" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-5 control-label">Loyalty $ Blanace</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" value="" id="loyalty_dollar_balance" name="loyalty_dollar_balance" readonly>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="card card-default" style="height: 100%;">
                            <div class="card-header separator">
                                <div class="card-title bold text-primary">Loyalty Item</div>
                            </div>
                            <div class="card-block" style="padding-top:4%;">
                                {{-- <form action="#" id="form-personal" role="form" autocomplete="off">
                                    <div class="row clearfix">
                                        <div class="col-md-5">
                                            <div class="form-group form-group-default">
                                                <label>Cemetery</label>
                                                <select class="full-width" data-placeholder="Select Cemetery" data-init-plugin="select2" name="cemetery_id">
                                                    <option value="{{ old('cemetery_id') }}" ></option>
                                                    @foreach($cemeteries as $cemetery )
                                                        <option value="{{ $cemetery->id }}" @if($cemetery->id==$cemetery_id) selected @endif>{{ $cemetery->name }}</option>
                                                    @endforeach
                                                </select>
                                                <span id="cemetery_id_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group form-group-default">
                                                <label>Item</label>
                                                <select class="full-width" data-placeholder="Select Item" data-init-plugin="select2" name="item_id">
                                                    <option value="{{ old('item_id') }}" ></option>
                                                    @foreach($cemetery_items as $cemetery_item )
                                                        <option value="{{ $cemetery_item->item_id }}">{{ $cemetery_item->item }}</option>
                                                    @endforeach
                                                </select>
                                                <span id="item_id_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 3%;">
                                                <button type="button" id="search2" class="btn btn-info mb-2"><span class="fa fa-refresh"></span>&nbsp;Search</button>
                                        </div>
                                    </div>
                                </form> --}}
                                <form action="#" class="form-inline" role="form" autocomplete="off" style="width:100%;">
                                    <div class="form-group col-lg-3 mb-2">
                                        <select class="full-width" data-placeholder="Select Cemetery" data-init-plugin="select2" name="cemetery_id">
                                            <option value="{{ old('cemetery_id') }}" ></option>
                                            @foreach($cemeteries as $cemetery )
                                                <option value="{{ $cemetery->id }}" @if($cemetery->id==$cemetery_id) selected @endif>{{ $cemetery->name }}</option>
                                            @endforeach
                                        </select>
                                        <span id="cemetery_id_err"></span>
                                    </div>
                                    <div class="form-group col-lg-7 mb-2">
                                        <select class="full-width" data-placeholder="Select Item" data-init-plugin="select2" name="item_id">
                                            <option value="{{ old('item_id') }}" ></option>
                                            @foreach($cemetery_items as $cemetery_item )
                                                <option value="{{ $cemetery_item->item_id }}">{{ $cemetery_item->item_name }}</option>
                                            @endforeach
                                        </select>
                                        <span id="item_id_err"></span>
                                    </div>
                                    <button type="button" id="search2" class="btn btn-info mb-2"><span class="fa fa-refresh"></span>&nbsp;Search</button>
                                </form>
                                <form action="#" class="form-horizontal" style="padding-left:3%; padding-top:2%;" role="form" autocomplete="off">
                                    <div class="form-group row" style="border-bottom:none;">
                                        <label class="col-md-3 control-label">To Redeem</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" value="" id="to_redeem" name="to_redeem" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">To Earn</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" value="" id="to_earn" name="to_earn" readonly>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <br>

                <h5>Recent Searches</h5>

                <div class="row">
                    <div class="col-lg-5">
                        <div class="card card-default" style="height: 100%;">
                            <div class="card-header separator">
                                <div class="card-title bold text-primary">Funeral Companies (L$ Available)</div>
                            </div>
                            <div class="card-block" style="padding-top:6%;">
                                <ul>
                                @foreach($search_history_fds as $search_history_fd )
                                    <li>{{ $search_history_fd->fdName }} ({{ $search_history_fd->avalld }})</li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="card card-default" style="height: 100%;">
                            <div class="card-header separator">
                                <div class="card-title bold text-primary">Item Maintenance (L$ Required)</div>
                            </div>
                            <div class="card-block" style="padding-top:4%;">
                                <ul>
                                @foreach($search_history_items as $search_history_item )
                                    <li>{{ $search_history_item->cemName }} - {{ $search_history_item->itemName }} ({{ $search_history_item->toRedeem }})</li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-lg-4">
                        <h1><button class="btn btn-lg btn-primary full-width">Funeral Companies Transaction Details</button></h1>
                    </div>
                    <div class="col-lg-4">
                        <h1><button class="btn btn-lg btn-success full-width">Item Maintainence Special Offers</button></h1>
                    </div>
                    <div class="col-lg-4">
                        <h1><button class="btn btn-lg btn-complete full-width">Administration</button></h1>
                    </div>
                </div>

            </div>
        </div>
        <!-- END card -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script type="text/javascript">

        $(document).ready(function() {

            var fd_id = jQuery('select[name="fd_id"]').val();
            var type = 'onload';
            getLoyaltyDollarByFdId(fd_id,type);

            $('#search1').click(function (){

                var valid_all = true;

                var fd_id = jQuery('select[name="fd_id"]').val();
                if (fd_id == "") {
                    $('#fd_id').css('background', '#ffdedd').css('border-color', '#ff5b57');
                    $('#fd_id_err').html('Select Funeral Director').css('color', '#F00').css('font-size', '11px');
                    valid_all = false;
                }else{
                    $('#fd_id').css('background', '').css('border-color', '');
                    $('#fd_id_err').html('').css('color', '').css('font-size', '');
                }

                if(valid_all){
                    var type = 'onclick';
                    getLoyaltyDollarByFdId(fd_id,type);
                }else{
                    return valid_all;
                }

            });

            $('#search2').click(function (){

                var valid_all = true;

                var cemetery_id = jQuery('select[name="cemetery_id"]').val();
                if (cemetery_id == "") {
                    $('#cemetery_id').css('background', '#ffdedd').css('border-color', '#ff5b57');
                    $('#cemetery_id_err').html('Select Cemetery').css('color', '#F00').css('font-size', '11px');
                    valid_all = false;
                }else{
                    $('#cemetery_id').css('background', '').css('border-color', '');
                    $('#cemetery_id_err').html('').css('color', '').css('font-size', '');
                }

                var item_id = jQuery('select[name="item_id"]').val();
                if (item_id == "") {
                    $('#item_id').css('background', '#ffdedd').css('border-color', '#ff5b57');
                    $('#item_id_err').html('Select Item').css('color', '#F00').css('font-size', '11px');
                    valid_all = false;
                }else{
                    $('#item_id').css('background', '').css('border-color', '');
                    $('#item_id_err').html('').css('color', '').css('font-size', '');
                }

                if(valid_all){
                    getPointsByCemeteryItem(cemetery_id,item_id);
                }else{
                    return valid_all;
                }

            });

            jQuery('select[name="cemetery_id"]').change(function(){

                var cemetery = jQuery('select[name="cemetery_id"]').val();
        
                getItemsByCemetery(cemetery);
        
            });

        });

        function getLoyaltyDollarByFdId(fd_id,type){
            
            jQuery('input[name="available_loyalty_dollars"]').val('');
            jQuery('input[name="loyalty_dollar_balance"]').val('');

            jQuery.ajax({
                url: '/staff/get_loyalty_dollar_by_fd_id/'+fd_id+'/'+type,
                type: 'GET',
                dataType: "json",
                success: function(data){
                    console.log(data);
                    jQuery('input[name="available_loyalty_dollars"]').val(data.available_loyalty_dollars);
                    jQuery('input[name="loyalty_dollar_balance"]').val(data.earned_loyalty_dollars[0].earned);
                }
            });

        }

        function getPointsByCemeteryItem(cemetery_id,item_id){

            jQuery('input[name="to_redeem"]').val('');
            jQuery('input[name="to_earn"]').val('');

            jQuery.ajax({
                url: '/staff/get_points_by_cemetery_item/'+cemetery_id+'/'+item_id,
                type: 'GET',
                dataType: "json",
                success: function(data){
                    jQuery('input[name="to_redeem"]').val(data[0].to_redeem);
                    jQuery('input[name="to_earn"]').val(data[0].to_earn);
                }
            });

        }

        function getItemsByCemetery(cemetery){

            jQuery('select[name="item_id"]').find('option').remove().end().append('<option value="">Select Item</option>').val('');
        
            jQuery.ajax({
                url: '/staff/get_items_by_cemetery/'+cemetery,
                type: 'GET',
                dataType: "json",
                success: function(data){
                    $.each(data, function(key, value) {
                        jQuery('select[name="item_id"]').append(jQuery('<option value="'+key+'">'+value+'</option>', {
                        }));
                    });
                }
            });
        
        }

    </script>

@endsection