@extends('layouts.master')

@section('title', 'Staff Dashboard')

@section('content')

    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid  container-fixed-lg">
        <!-- START card -->
        <div class="card card-default m-t-20">
            <div class="card-block padding-50 sm-padding-10">
                <div class="page-header">
                    <div class="card-title">Welcome <span class="bold">{{ Auth::user()->name }}</span> !</div>
                </div>

                <div class="card card-white card-shadow card-special">
                    <div class="card-block">
                        <form action="#" id="form-personal" role="form" autocomplete="off">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Funeral Director</label>
                                        <select class="full-width" data-placeholder="Select Funeral Director" data-init-plugin="select2" name="fd_id">
                                            <option value="{{ old('fd_id') }}" ></option>
                                            @foreach($funeral_directors as $funeral_director )
                                                <option value="{{ $funeral_director->id }}" @if($funeral_director->id==$fd_id) selected @endif>{{ $funeral_director->fd_name }}</option>
                                            @endforeach
                                        </select>
                                        <span id="fd_id_err"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cemetery</label>
                                        <select class="full-width" data-placeholder="Select Cemetery" data-init-plugin="select2" name="cemetery_id">
                                            <option value="{{ old('cemetery_id') }}" ></option>
                                            @foreach($cemeteries as $cemetery )
                                                <option value="{{ $cemetery->id }}" @if($cemetery->id==$cemetery_id) selected @endif>{{ $cemetery->name }}</option>
                                            @endforeach
                                        </select>
                                        <span id="cemetery_id_err"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Item</label>
                                        <select class="full-width" data-placeholder="Select Item" data-init-plugin="select2" name="item_id">
                                            <option value="{{ old('item_id') }}" ></option>
                                            @foreach($cemetery_items as $cemetery_item )
                                                <option value="{{ $cemetery_item->item_id }}">{{ $cemetery_item->item_name }}</option>
                                            @endforeach
                                        </select>
                                        <span id="item_id_err"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-info pull-right" id="search" type="button"><span class="fa fa-refresh"></span>&nbsp;Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-white card-shadow card-special" style="height: 100%;">
                            <div class="card-header separator">
                                <div class="card-title bold text-primary">Funeral Director Info</div>
                            </div>
                            <div class="card-block">
                                <form class="form-horizontal" action="#" id="form-personal" role="form" autocomplete="off">
                                    <div class="form-group row">
                                        <div class="col-md-12" id="is_on_hold"></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-7 control-label">Loyalty $ Available</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control card-input" value="" id="available_loyalty_dollars" name="available_loyalty_dollars" readonly>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-white card-shadow card-special" style="height: 100%;">
                            <div class="card-header separator">
                                <div class="card-title bold text-primary">Loyalty Item</div>
                            </div>
                            <div class="card-block">
                                <form method="POST" class="form-horizontal" action="#" id="form-personal" role="form" autocomplete="off">
                                    <div class="form-group row" style="border-bottom:none;">
                                        <label class="col-md-6 control-label">To Redeem</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control card-input" value="" id="ci_to_redeem" name="ci_to_redeem" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-6 control-label">To Earn</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control card-input" value="" id="ci_to_earn" name="ci_to_earn" readonly>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-white card-shadow card-special" style="height: 100%;">
                            <div class="card-header separator">
                                <div class="card-title bold text-primary">Special Offer</div>
                            </div>
                            <div class="card-block">
                                <form method="POST" class="form-horizontal" action="#" id="form-personal" role="form" autocomplete="off">
                                    <div class="form-group row" style="border-bottom:none;">
                                        <label class="col-md-6 control-label">To Redeem</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control card-input" value="" id="so_to_redeem" name="so_to_redeem" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-6 control-label">To Earn</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control card-input" value="" id="so_to_earn" name="so_to_earn" readonly>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="padding-top: 1%;">
                    <div class="col-md-5">
                        <div class="card card-white card-shadow card-special" style="height: 100%;">
                            <div class="card-header separator">
                                <div class="card-title bold text-primary">Funeral Directors</div>
                            </div>
                            <div class="card-block" style="padding-top:2%;">
                                <ul>
                                @foreach($fd_search_history_logs as $fd_search_history_log )
                                    <li>{{ $fd_search_history_log->fdCode }} - {{ $fd_search_history_log->fdName }} ({{ "$".$fd_search_history_log->available_ld }})</li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="card card-white card-shadow card-special" style="height: 100%;">
                            <div class="card-header separator">
                                <div class="card-title bold text-primary">Items - L$ to redeem</div>
                            </div>
                            <div class="card-block" style="padding-top:2%;">
                                <ul>
                                @foreach($item_search_history_logs as $item_search_history_log )
                                    <li>{{ $item_search_history_log->cemName }} - {{ $item_search_history_log->itemCode }} - {{ $item_search_history_log->itemName }} ({{ "$".$item_search_history_log->toRedeem }})</li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- END card -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script type="text/javascript">

        $(document).ready(function() {

            $('#search').click(function (){

                var valid_all = true;

                var fd_id = jQuery('select[name="fd_id"]').val();
                if (fd_id == "") {
                    $('#fd_id').css('background', '#ffdedd').css('border-color', '#ff5b57');
                    $('#fd_id_err').html('Select Funeral Director').css('color', '#F00').css('font-size', '11px');
                    valid_all = false;
                }else{
                    $('#fd_id').css('background', '').css('border-color', '');
                    $('#fd_id_err').html('').css('color', '').css('font-size', '');
                }

                var cemetery_id = jQuery('select[name="cemetery_id"]').val();
                if (cemetery_id == "") {
                    $('#cemetery_id').css('background', '#ffdedd').css('border-color', '#ff5b57');
                    $('#cemetery_id_err').html('Select Cemetery').css('color', '#F00').css('font-size', '11px');
                    valid_all = false;
                }else{
                    $('#cemetery_id').css('background', '').css('border-color', '');
                    $('#cemetery_id_err').html('').css('color', '').css('font-size', '');
                }

                var item_id = jQuery('select[name="item_id"]').val();
                if (item_id == "") {
                    $('#item_id').css('background', '#ffdedd').css('border-color', '#ff5b57');
                    $('#item_id_err').html('Select Item').css('color', '#F00').css('font-size', '11px');
                    valid_all = false;
                }else{
                    $('#item_id').css('background', '').css('border-color', '');
                    $('#item_id_err').html('').css('color', '').css('font-size', '');
                }

                if(valid_all){
                    var type = 'onclick';
                    getPointsByCemeteryItem(fd_id,type,cemetery_id,item_id);
                }else{
                    return valid_all;
                }

            });

            jQuery('select[name="cemetery_id"]').change(function(){

                var cemetery = jQuery('select[name="cemetery_id"]').val();
        
                getItemsByCemetery(cemetery);
        
            });

        });

        function getPointsByCemeteryItem(fd_id,type,cemetery_id,item_id){

            if(fd_id==""){
                fd_id   = "0";
            }
            if(cemetery_id==""){
                cemetery_id   = "0";
            }
            if(item_id==""){
                item_id   = "0";
            }

            jQuery('input[name="available_loyalty_dollars"]').val('');
            jQuery('#is_on_hold').text('');
            jQuery('input[name="ci_to_redeem"]').val('');
            jQuery('input[name="ci_to_earn"]').val('');
            jQuery('input[name="so_to_redeem"]').val('');
            jQuery('input[name="so_to_earn"]').val('');

            jQuery.ajax({
                url: '/staff/get_points_by_cemetery_item/'+fd_id+'/'+type+'/'+cemetery_id+'/'+item_id,
                type: 'GET',
                dataType: "json",
                success: function(data){
                    jQuery('input[name="available_loyalty_dollars"]').val(data.available_loyalty_dollars);
                    var is_on_hold_span = "";
                    if(data.fd_details[0].is_on_hold == false){
                        jQuery('#is_on_hold').append('<span class="label label-success fd-hold">ACTIVE</span>');
                    }else if(data.fd_details[0].is_on_hold == true){
                        jQuery('#is_on_hold').append('<span class="label label-important fd-hold">ON HOLD</span>');
                    }
                    jQuery('input[name="ci_to_redeem"]').val(data.results.ci_to_redeem);
                    jQuery('input[name="ci_to_earn"]').val(data.results.ci_to_earn);
                    jQuery('input[name="so_to_redeem"]').val(data.results.so_to_redeem);
                    jQuery('input[name="so_to_earn"]').val(data.results.so_to_earn);
                }
            });

        }

        function getItemsByCemetery(cemetery){

            jQuery('select[name="item_id"]').find('option').remove().end().append('<option value="">Select Item</option>').val('');
        
            jQuery.ajax({
                url: '/staff/get_items_by_cemetery/'+cemetery,
                type: 'GET',
                dataType: "json",
                success: function(data){
                    $.each(data, function(key, value) {
                        jQuery('select[name="item_id"]').append(jQuery('<option value="'+value.item_id+'">'+value.item+'</option>', {
                        }));
                    });
                }
            });
        
        }

    </script>

@endsection