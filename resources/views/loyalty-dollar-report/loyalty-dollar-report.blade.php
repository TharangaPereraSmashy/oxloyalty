<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body style="font-family: 'Poppins','Segoe UI', Arial, sans-serif;">
    <div>
        <div>
            <div style="font-size:16px; font-weight:bold;">Loyalty $ Report</div>
            <div style="font-size:14px;padding-top:2%;">
                <table style="border:none;">
                    @php if($fd_name != ""){ @endphp
                        <tr>
                            <td>Funeral Director</td>
                            <td>:</td>
                            <td><b>{{ $fd_name }}</b></td>
                        </tr>
                    @php } @endphp
                    @php if($from != ""){ @endphp
                        <tr>
                            <td>From Date</td>
                            <td>:</td>
                            <td><b>{{ $from }}</b></td>
                        </tr>
                    @php } @endphp
                    @php if($to != ""){ @endphp
                        <tr>
                            <td>To Date</td>
                            <td>:</td>
                            <td><b>{{ $to }}</b></td>
                        </tr>
                    @php } @endphp
                    @php if($available_loyalty_dollars != ""){ @endphp
                        <tr>
                            <td>Available L$</td>
                            <td>:</td>
                            <td><b>{{ $available_loyalty_dollars }}</b></td>
                        </tr>
                    @php } @endphp
                </table>
            </div>
        </div>
        <br>
        <div>
            <table style="margin-bottom: 0;
                margin-top: 5px;    
                width: 100%;
                max-width: 100%;
                border-collapse: collapse;
                background-color: transparent;
                display: table;
                border-spacing: 2px;
                border-color: grey;
                font-size:12px;">
                <thead>
                    <tr>
                        @php if($is_parent == 1){ @endphp
                        <th>Funeral Director</th>
                        @php } @endphp
                        <th>Transaction Date</th>
                        <th>Transaction Type</th>
                        <th>Doc No</th>
                        <th>Doc Date</th>
                        <th>Item Number</th>
                        <th style="text-align:right;">Points</th>
                    </tr>
                </thead>
                <tbody>

                @foreach($results as $result)

                    <tr>
                        @php if($is_parent == 1){ @endphp
                        <td>{{ $result->fd_name }}</td>
                        @php } @endphp
                        <td>{{ date('d-m-Y', strtotime($result->trans_date)) }}</td>
                        <td style="font-size:10px;text-transform: capitalize;">{{ $result->trans_type }}</td>
                        <td>{{ $result->doc_number }}</td>
                        <td>{{ date('d-m-Y', strtotime($result->doc_date)) }}</td>
                        <td>{{ $result->item_number }}</td>
                        <td style="text-align:right;">{{ $result->points }}</td>
                    </tr>

                @endforeach

                </tbody>
            </table>
        </div>
    </div>
  </body>
</html>