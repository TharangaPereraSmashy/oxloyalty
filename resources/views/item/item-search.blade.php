@extends('layouts.master')

@section('title', 'Items')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('items.index') }}">Item</a></li>
                    <li class="breadcrumb-item active">All Items</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="page-header padding-20-top">
            <div class="card-title pull-left"><a href="{{ route('items.index') }}" class="fa fa-angle-left go-back"></a> All Items</div>
            <div class="pull-right">
                <form method="POST" action="{{ action('ItemController@searchItems') }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="input-group">
                        <a class="btn btn-primary btn-rounded" style="margin-right: 30px;" href="{{ route('items.create') }}"><span class="fa fa-plus"></span>&nbsp;Add Item</a>
                        <input type="text" class="form-control form-search" name="search_value" value="{{ old('search_value') }}" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-search btn-sm" type="submit"><span class="fa fa-search"></span></button>
                        </span>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="card card-white card-shadow card-special">

            @if(count($items)==0)
                <div class="card-block text-center">No Results Available</div>
            @else

            <div class="card-block">
                <table class="table table-hover demo-table-search table-responsive-block">
                    <thead>
                    <tr>
                        <th>Code</th>
                        <th>Description</th>
                        <th>Special Offers</th>
                        <th>GL Liability Code</th>
                        <th>GL Cost Code</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($items as $item)

                        <tr class="gradeA">
                            <td>{{ $item->code }}</td>
                            <td>{{ $item->description }}</td>
                            <td>
                                @if($item->sp_count == 0)
                                    <span class="label label-danger">No</span>
                                @elseif($item->sp_count > 0)
                                    <span class="label label-info">Yes</span>
                                @else
                                    <span class="label bg-black">Undefined</span>
                                @endif
                            </td>
                            <td>{{ $item->gl_liability_code }}</td>
                            <td>{{ $item->gl_cost_code }}</td>
                            <td>
                                <form action="{{ route('items.destroy',$item->id) }}" method="POST" class="delete_record">

                                    @if(Session::get('read_item'))
                                        <a class="btn btn-xs btn-rounded btn-complete" href="{{ route('items.show',$item->id) }}"><span class="fa fa-eye"></span></a>
                                    @endif

                                    @if(Session::get('update_item'))
                                        <a class="btn btn-xs btn-rounded btn-success" href="{{ route('items.edit',$item->id) }}"><span class="fa fa-edit"></span></a>
                                    @endif

                                    @csrf
                                    @method('DELETE')

                                    @if(Session::get('delete_item'))
                                        <button type="submit" class="btn btn-xs btn-rounded btn-danger"><span class="fa fa-trash"></span></button>
                                    @endif

                                </form>
                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
            </div>

            @endif

        </div>
        <!-- END card -->

        <div class="row pagination-div">
            <div class="col-md-12">
                <div class="pull-right">
                    {!! $items->links() !!}
                </div>
            </div>
        </div>

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script>
        $(".delete_record").on("submit", function(){
            return confirm("Do you want to delete this record ?");
        });
    </script>

@endsection