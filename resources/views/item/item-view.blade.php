@extends('layouts.master')

@section('title', 'View Item')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('items.index') }}">Item</a></li>
                    <li class="breadcrumb-item active">View Item</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid  container-fixed-lg">
        <div class="page-header padding-20-top">
            {{-- <div class="card-title"><a href="{{ route('items.index') }}" class="fa fa-angle-left go-back"></a> <a href="{{ route('items.index') }}">Items</a> <span><i class="fa fa-chevron-right"></i></span> Item Details</div> --}}
            <div class="card-title"><a href="{{ route('items.index') }}" class="fa fa-angle-left go-back"></a> Item Details</div>
        </div>
        <!-- START card -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-white card-shadow card-special">
                    <div class="card-block item-card">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="item-name semi-bold">{{ $item->description }} ( <span class="item-code"> {{ $item->code }} </span> )
                                    @if($item->status == 1)
                                        <span class="label label-success item-status">ACTIVE</span>
                                    @elseif($item->status == 0)
                                        <span class="label label-important item-status">INACTIVE</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="item-details">
                                    @if($item->gl_liability_code != "")
                                        GL Liability Code <span class="item-code">{{ $item->gl_liability_code }}</span>
                                    @endif
                                    @if($item->gl_cost_code != "")
                                        <br>GL Cost Code <span class="item-code">{{ $item->gl_cost_code }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-white card-shadow card-special">
                    
                    @if(count($cemetery_items)==0)
                        
                        <div class="card-block">
                            <div class="page-header customized">
                                <div class="card-sub-title pull-left">Item Loyalty</div>
                                <div class="pull-right">
                                    <a class="btn btn-primary btn-sm btn-rounded" href="{{ route('create_cemetery_item_by_item', array('item' => $item->id)) }}"><span class="fa fa-plus"></span>&nbsp;Add Item Loyalty</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class=" card-block text-center">No Results Available</div>
                        </div>

                    @else    

                        <div class="card-block">
                            <div class="page-header customized">
                                <div class="card-sub-title pull-left">Item Loyalty</div>
                                <div class="pull-right">
                                    <a class="btn btn-primary btn-sm btn-rounded" href="{{ route('create_cemetery_item_by_item', array('item' => $item->id)) }}"><span class="fa fa-plus"></span>&nbsp;Add Item Loyalty</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <table class="table table-hover demo-table-dynamic table-responsive-block" id="item_loyalty_table">
                                <thead>
                                <tr>
                                    <th>Cemetery</th>
                                    <th>To Earn</th>
                                    <th>To Redeem</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($cemetery_items as $cemetery_item)
                                        <tr class="gradeA">
                                            <td>{{ $cemetery_item->cemetery }}</td>
                                            <td>{{ $cemetery_item->to_earn }}</td>
                                            <td>{{ $cemetery_item->to_redeem }}</td>
                                            <td>{{ date('d-m-Y', strtotime($cemetery_item->start_date)) }}</td>
                                            <td>{{ date('d-m-Y', strtotime($cemetery_item->end_date)) }}</td>
                                            <td>
                                                <form action="{{ route('destroy_cemetery_item_by_item', array('id' => $cemetery_item->id, 'item' => $item->id)) }}" method="POST" class="delete_cemetery_item">
                
                                                    @if(Session::get('update_item'))
                                                        <a class="btn btn-xs btn-rounded btn-success" href="{{ route('edit_cemetery_item_by_item', array('id' => $cemetery_item->id, 'item' => $item->id)) }}"><span class="fa fa-edit"></span></a>
                                                    @endif
                
                                                    @csrf
                                                    @method('DELETE')
                
                                                    @if(Session::get('delete_item'))
                                                        <button type="submit" class="btn btn-xs btn-rounded btn-danger"><span class="fa fa-trash"></span></button>
                                                    @endif
                
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    
                    @endif   

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-white card-special card-shadow">

                    @if(count($special_offers)==0)
                        
                        <div class="card-block">
                            <div class="page-header customized">
                                <div class="card-sub-title pull-left">Special Offers</div>
                                <div class="pull-right">
                                    <a class="btn btn-primary btn-sm btn-rounded" href="{{ route('create_special_offer_by_item', array('item' => $item->id)) }}"><span class="fa fa-plus"></span>&nbsp;Add Special Offer</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="card-block text-center">No Results Available</div>
                        </div>

                    @else    

                        <div class="card-block">
                            <div class="page-header customized">
                                <div class="card-sub-title pull-left">Special Offers</div>
                                <div class="pull-right">
                                    <a class="btn btn-primary btn-sm btn-rounded" href="{{ route('create_special_offer_by_item', array('item' => $item->id)) }}"><span class="fa fa-plus"></span>&nbsp;Add Special Offer</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <table class="table table-hover demo-table-dynamic table-responsive-block" id="special_offer_table">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Description</th>
                                        <th>Cemetery</th>
                                        <th>FD From</th>
                                        <th>FD To</th>
                                        <th>To Earn</th>
                                        <th>To Redeem</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($special_offers as $special_offer)
                                        <tr class="gradeA">
                                            <td>{{ $special_offer->code }}</td>
                                            <td>{{ $special_offer->description }}</td>
                                            <td>{{ $special_offer->cemetery }}</td>
                                            <td>{{ $special_offer->fd_from_code }}</td>
                                            <td>{{ $special_offer->fd_to_code }}</td>
                                            <td>{{ $special_offer->to_earn }}</td>
                                            <td>{{ $special_offer->to_redeem }}</td>
                                            <td>{{ date('d-m-Y', strtotime($special_offer->start_date)) }}</td>
                                            <td>{{ date('d-m-Y', strtotime($special_offer->end_date)) }}</td>
                                            <td>
                                                <form action="{{ route('destroy_special_offer_by_item', array('id' => $special_offer->id, 'item' => $item->id)) }}" method="POST" class="delete_special_offer">
            
                                                    @if(Session::get('update_item'))
                                                        <a class="btn btn-xs btn-rounded btn-success" href="{{ route('edit_special_offer_by_item', array('id' => $special_offer->id, 'item' => $item->id)) }}"><span class="fa fa-edit"></span>&nbsp;Edit</a>
                                                    @endif
                
                                                    @csrf
                                                    @method('DELETE')
                
                                                    @if(Session::get('delete_item'))
                                                        <button type="submit" class="btn btn-xs btn-rounded btn-danger"><span class="fa fa-trash"></span>&nbsp;Delete</button>
                                                    @endif
                
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    @endif  

                </div>
            </div>
        </div>
        <!-- END card -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script type="text/javascript">
        
        jQuery('#item_loyalty_table').dataTable({
            "bLengthChange":false,
            "info":false,
            "bFilter": false,
            "columnDefs": [
                { "orderable": false, "targets": 5 }
            ]
        });

        jQuery('#special_offer_table').dataTable({
            "bLengthChange":false,
            "info":false,
            "bFilter": false,
            "columnDefs": [
                { "orderable": false, "targets": 9 }
            ]
        });

        $(".delete_cemetery_item").on("submit", function(){
            return confirm("Do you want to delete this record ?");
        });

        $(".delete_special_offer").on("submit", function(){
            return confirm("Do you want to delete this record ?");
        });

    </script>

@endsection