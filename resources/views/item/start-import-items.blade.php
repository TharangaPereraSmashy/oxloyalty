@extends('layouts.master')

@section('title', 'Import Items Mapping')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('items.index') }}">Item</a></li>
                    <li class="breadcrumb-item active">Import Export Items</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('import.export.view') }}" class="fa fa-angle-left go-back"></a> Import Items Mapping</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="card card-white card-shadow card-special">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        <form method="POST" action="{{ route('start.import.items') }}" id="form-work" class="form-horizontal" role="form" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            {{--  @foreach($excel_headers as $excel_header )
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">{{ $excel_header }}</label>
                                    <input type="hidden" name="excel_header_name[]" value="{{ $excel_header }}" />
                                    <div class="col-md-4">
                                        <select class="full-width" data-placeholder="Select Database Column Name" data-init-plugin="select2" name="db_column_name[]">
                                            <option value="{{ old('db_column_name') }}" ></option>
                                            @foreach($db_columns as $db_column )
                                                <option value="{{ $db_column }}">{{ $db_column }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endforeach  --}}
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Code</label>
                                <div class="col-md-6">
                                    <select class="full-width" data-placeholder="Select Excel Header Name" data-init-plugin="select2" name="mapping_field_code" required>
                                        <option value="{{ old('mapping_field_code') }}" ></option>
                                        @foreach($excel_headers as $excel_header )
                                            <option value="{{ $excel_header }}">{{ $excel_header }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mapping_field_code'))
                                        <div class="error">{{ $errors->first('mapping_field_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Description</label>
                                <div class="col-md-6">
                                    <select class="full-width" data-placeholder="Select Excel Header Name" data-init-plugin="select2" name="mapping_field_description" required>
                                        <option value="{{ old('mapping_field_description') }}" ></option>
                                        @foreach($excel_headers as $excel_header )
                                            <option value="{{ $excel_header }}">{{ $excel_header }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mapping_field_description'))
                                        <div class="error">{{ $errors->first('mapping_field_description') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Status</label>
                                <div class="col-md-6">
                                    <select class="full-width" data-placeholder="Select Excel Header Name" data-init-plugin="select2" name="mapping_field_status" required>
                                        <option value="{{ old('mapping_field_status') }}" ></option>
                                        @foreach($excel_headers as $excel_header )
                                            <option value="{{ $excel_header }}">{{ $excel_header }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mapping_field_status'))
                                        <div class="error">{{ $errors->first('mapping_field_status') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Gl_liability_code</label>
                                <div class="col-md-6">
                                    <select class="full-width" data-placeholder="Select Excel Header Name" data-init-plugin="select2" name="mapping_field_gl_liability_code" required>
                                        <option value="{{ old('mapping_field_gl_liability_code') }}" ></option>
                                        @foreach($excel_headers as $excel_header )
                                            <option value="{{ $excel_header }}">{{ $excel_header }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mapping_field_gl_liability_code'))
                                        <div class="error">{{ $errors->first('mapping_field_gl_liability_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Gl_cost_code</label>
                                <div class="col-md-6">
                                    <select class="full-width" data-placeholder="Select Excel Header Name" data-init-plugin="select2" name="mapping_field_gl_cost_code" required>
                                        <option value="{{ old('mapping_field_gl_cost_code') }}" ></option>
                                        @foreach($excel_headers as $excel_header )
                                            <option value="{{ $excel_header }}">{{ $excel_header }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mapping_field_gl_cost_code'))
                                        <div class="error">{{ $errors->first('mapping_field_gl_cost_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <br>
                            <div class="row clearfix padding-bottom-10">
                                <input type="hidden" name="excel_file_path" value="{{ $file_path }}" />
                                <input type="hidden" name="sheet_index" value="{{ $sheet_index }}" />
                                <div class="col-md-6">
                                    <a class="btn btn-light btn-lg" href="{{ route('import.export.view') }}">Back to Import Export Items</a>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button class="btn btn-primary btn-lg" type="submit">Import Items</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection