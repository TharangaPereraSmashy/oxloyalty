@extends('layouts.master')

@section('title', 'Edit Item')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('items.index') }}">Item</a></li>
                    <li class="breadcrumb-item active">Edit Item</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('items.index') }}" class="fa fa-angle-left go-back"></a> Edit Item</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{ route('items.update',$item->id) }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    @method('PUT')
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Item Code</label>
                                        <input type="text" class="form-control" id="code" placeholder="Item Code" value="{{ $item->code }}" disabled required>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-group-default required">
                                        <label>Item Description</label>
                                        <input type="text" class="form-control" id="description" placeholder="Item Description" name="description" value="{{ $item->description }}" required>
                                        @if ($errors->has('description'))
                                            <div class="error">{{ $errors->first('description') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Status</label>
                                        <div class="radio radio-success">
                                            <div class="row">
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="1" name="status" id="active" @if($item->status == '1') checked @endif>
                                                    <label for="active">Active</label>
                                                </div>
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="0" name="status" id="inactive" @if($item->status == '0') checked @endif>
                                                    <label for="inactive">Inactive</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>GL Liability Code</label>
                                        <input type="text" class="form-control" id="gl_liability_code" placeholder="GL Liability Code" name="gl_liability_code" value="{{ $item->gl_liability_code }}" required>
                                        @if ($errors->has('gl_liability_code'))
                                            <div class="error">{{ $errors->first('gl_liability_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>GL Cost Code</label>
                                        <input type="text" class="form-control" id="gl_cost_code" placeholder="GL Cost Code" name="gl_cost_code" value="{{ $item->gl_cost_code }}" required>
                                        @if ($errors->has('gl_cost_code'))
                                            <div class="error">{{ $errors->first('gl_cost_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix padding-bottom-20">
                        <div class="col-md-6">
                            <a class="btn btn-light btn-lg" href="{{ route('items.index') }}">View all Items</a>
                        </div>
                        <div class="col-md-6 text-right">
                            <input type="hidden" name="code" value="{{ $item->code }}">
                            <input type="hidden" name="updated_at" value="{{ $item->updated_at }}">
                            @if(Session::get('update_item'))
                                <button class="btn btn-success btn-lg" type="submit">Save</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection