@extends('layouts.master')

@section('title', 'Edit Cemetery')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('cemeteries.index') }}">Cemetery</a></li>
                    <li class="breadcrumb-item active">Edit Cemetery</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('cemeteries.index') }}" class="fa fa-angle-left go-back"></a> Edit Cemetery</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{ route('cemeteries.update',$cemetery->id) }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    @method('PUT')
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Cemetery Code</label>
                                        <input type="text" class="form-control" id="code" placeholder="Cemetery Code" value="{{ $cemetery->code }}" disabled required>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-group-default required">
                                        <label>Cemetery Name</label>
                                        <input type="text" class="form-control" id="name" placeholder="Cemetery Name" name="name" value="{{ $cemetery->name }}" required>
                                        @if ($errors->has('name'))
                                            <div class="error">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Address 1</label>
                                        <input type="text" class="form-control" id="address1" placeholder="Address 1" name="address1" value="{{ $cemetery->address1 }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Address 2</label>
                                        <input type="text" class="form-control" id="address2" placeholder="Address 2" name="address2" value="{{ $cemetery->address2 }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Address 3</label>
                                        <input type="text" class="form-control" id="address3" placeholder="Address 3" name="address3" value="{{ $cemetery->address3 }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>City/Suburb</label>
                                        <input type="text" class="form-control" id="suburb" placeholder="City/Suburb" name="suburb" value="{{ $cemetery->suburb }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Postcode</label>
                                        <input type="text" class="form-control" id="postcode" placeholder="Postcode" name="postcode" value="{{ $cemetery->postcode }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Country</label>
                                        <select class="full-width" data-placeholder="Select Country" data-init-plugin="select2" name="country_id">
                                            <option value=""></option>
                                            @foreach($countries as $country )
                                                <option value="{{ $country->id }}" @if($country->id==$cemetery->country_id) selected @endif>{{ $country->country_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>State</label>
                                        <select class="full-width" data-placeholder="Select State" data-init-plugin="select2" name="state_id">
                                            <option value=""></option>
                                            @foreach($states as $state )
                                                <option value="{{ $state->id }}" @if($state->id==$cemetery->state_id) selected @endif>{{ $state->state_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" id="phone" placeholder="Phone" name="phone" value="{{ $cemetery->phone }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="{{ $cemetery->email }}">
                                        @if ($errors->has('email'))
                                            <div class="error">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Is Default Cemetery</label>
                                        <div class="radio radio-success radio-padding">
                                            <div class="row">
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="1" name="is_default_cemetery" id="yes" @if($cemetery->is_default_cemetery == '1') checked @endif>
                                                    <label for="yes">Yes</label>
                                                </div>
                                                <div class="col-md-3 radio-labels">
                                                    <input type="radio" value="0" name="is_default_cemetery" id="no" @if($cemetery->is_default_cemetery == '0') checked @endif>
                                                    <label for="no">No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>External Reference Code</label>
                                        <input type="text" class="form-control" id="external_ref_code" placeholder="External Reference Code" name="external_ref_code" value="{{ $cemetery->external_ref_code }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix padding-bottom-20">
                        <div class="col-md-6">
                            <a class="btn btn-light btn-lg" href="{{ route('cemeteries.index') }}">View all Cemeteries</a>
                        </div>
                        <div class="col-md-6 text-right">
                            <input type="hidden" name="updated_at" value="{{ $cemetery->updated_at }}">
                            @if(Session::get('update_cemetery'))
                                <button class="btn btn-success btn-lg" type="submit">Save</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection