@extends('layouts.master')

@section('title', 'Cemeteries')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('cemeteries.index') }}">Cemetery</a></li>
                    <li class="breadcrumb-item active">All Cemeteries</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="page-header padding-20-top">
            <div class="card-title pull-left"><a href="{{ route('cemeteries.index') }}" class="fa fa-angle-left go-back"></a> Search Results @if($search_value!="")&nbsp;&nbsp;-&nbsp;&nbsp;<span style="font-size:14px;font-weight: bold;"> "{{ $search_value }}"</span> @endif </div>
            <a class="btn btn-warning btn-sm btn-rounded" style="margin-left: 30px;" href="{{ route('cemeteries.index') }}"><span class="fa fa-remove"></span>&nbsp;Clear</a>
                        
            <div class="pull-right">
                <form method="POST" action="{{ action('CemeteryController@searchCemeteries') }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="input-group">
                        <input type="text" class="form-control form-search" name="search_value" value="{{ old('search_value') }}" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-search btn-sm" type="submit"><span class="fa fa-search"></span></button>
                        </span>
                    </div>
                </form>
            </div>
            
            <div class="clearfix"></div>
        </div>
        <div class="card card-white card-shadow card-special">

            @if(count($cemeteries)==0)
                <div class="card-block text-center">No Results Available</div>
            @else

            <div class="card-block">
                <table class="table table-hover demo-table-search table-responsive-block">
                    <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($cemeteries as $cemetery)

                        <tr class="gradeA">
                            <td>{{ $cemetery->code }}</td>
                            <td>{{ $cemetery->name }}</td>
                            <td>{{ $cemetery->phone }}</td>
                            <td>{{ $cemetery->email }}</td>
                            <td>

                                <form action="{{ route('cemeteries.destroy',$cemetery->id) }}" method="POST" class="delete_record">

                                    @if(Session::get('read_cemetery'))
                                        <a class="btn btn-xs btn-rounded btn-complete" href="{{ route('cemeteries.show',$cemetery->id) }}"><span class="fa fa-eye"></span>&nbsp;View</a>
                                    @endif

                                    @if(Session::get('update_cemetery'))
                                        <a class="btn btn-xs btn-rounded btn-success" href="{{ route('cemeteries.edit',$cemetery->id) }}"><span class="fa fa-edit"></span>&nbsp;Edit</a>
                                    @endif

                                    @csrf
                                    @method('DELETE')

                                    @if(Session::get('delete_cemetery'))
                                        <button type="submit" class="btn btn-xs btn-rounded btn-danger"><span class="fa fa-trash"></span>&nbsp;Delete</button>
                                    @endif

                                </form>

                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
            </div>

            @endif

        </div>
        <!-- END card -->

        <div class="row pagination-div">
            <div class="col-md-12">
                <div class="pull-right">
                    {!! $cemeteries->links() !!}
                </div>
            </div>
        </div>

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script>
        $(".delete_record").on("submit", function(){
            return confirm("Do you want to delete this record ?");
        });
    </script>

@endsection