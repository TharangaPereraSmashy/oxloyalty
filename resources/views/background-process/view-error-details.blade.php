@extends('layouts.master')

@section('title', 'Error Details')

@section('content')

    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid  container-fixed-lg">
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('view.error.details') }}" class="fa fa-angle-left go-back"></a>Error Details</div>
        </div>
        <!-- START card -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="card card-white card-shadow card-special">
                    
                    @if(count($cms_trans_errors)==0)
                        
                        <div class="card-block">
                            <div class="page-header customized">
                                <div class="card-sub-title pull-left">Errors</div>
                                <div class="clearfix"></div>
                            </div>
                            <div class=" card-block text-center">No Results Available</div>
                        </div>

                    @else    

                        <div class="card-block">
                            <div class="page-header customized">
                                <div class="card-sub-title pull-left">Errors</div>
                                <div class="clearfix"></div>
                            </div>
                            <table class="table table-hover demo-table-dynamic table-responsive-block" id="errors_table">
                                <thead>
                                <tr>
                                    <th>Rec</th>
                                    <th>Error</th>
                                    <th>DNo</th>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>FD</th>
                                    <th>Cemetery</th>
                                    <th>Item</th>
                                    <th>Qty</th>
                                    <th>RW</th>
                                    <th>RD</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($cms_trans_errors as $cms_trans_error)
                                        <tr class="gradeA">
                                            <td>{{ $cms_trans_error->record_id }}</td>
                                            <td>{{ $cms_trans_error->error }}</td>
                                            <td>{{ $cms_trans_error->doc_number }}</td>
                                            <td>{{ date('d-m-Y', strtotime($cms_trans_error->doc_date)) }}</td>
                                            <td>{{ $cms_trans_error->doc_type }}</td>
                                            <td>{{ $cms_trans_error->funeral_director }}</td>
                                            <td>{{ $cms_trans_error->cemetery }}</td>
                                            <td>{{ $cms_trans_error->item_number }}</td>
                                            <td>{{ $cms_trans_error->quantity }}</td>
                                            <td>{{ $cms_trans_error->rewards }}</td>
                                            <td>{{ $cms_trans_error->rewards_redeemed }}</td>
                                            <td>
                                                <form action="{{ route('ignore.cms.trans.errors', $cms_trans_error->id) }}" method="POST" class="ignore_error">
                                                    @csrf
                                                    <button type="submit" class="btn btn-xs btn-rounded btn-danger">Ignore</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    
                    @endif   

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-white card-shadow card-special">
                    
                    @if(count($ignored_cms_trans_errors)==0)
                        
                        <div class="card-block">
                            <div class="page-header customized">
                                <div class="card-sub-title pull-left">Ignored Errors</div>
                                <div class="clearfix"></div>
                            </div>
                            <div class=" card-block text-center">No Results Available</div>
                        </div>

                    @else    

                        <div class="card-block">
                            <div class="page-header customized">
                                <div class="card-sub-title pull-left">Ignored Errors</div>
                                <div class="clearfix"></div>
                            </div>
                            <table class="table table-hover demo-table-dynamic table-responsive-block" id="ignored_errors_table">
                                <thead>
                                <tr>
                                    <th>Rec</th>
                                    <th>Error</th>
                                    <th>DNo</th>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>FD</th>
                                    <th>Cemetery</th>
                                    <th>Item</th>
                                    <th>Qty</th>
                                    <th>RW</th>
                                    <th>RD</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($ignored_cms_trans_errors as $ignored_cms_trans_error)
                                        <tr class="gradeA">
                                            <td>{{ $ignored_cms_trans_error->record_id }}</td>
                                            <td>{{ $ignored_cms_trans_error->error }}</td>
                                            <td>{{ $ignored_cms_trans_error->doc_number }}</td>
                                            <td>{{ date('d-m-Y', strtotime($ignored_cms_trans_error->doc_date)) }}</td>
                                            <td>{{ $ignored_cms_trans_error->doc_type }}</td>
                                            <td>{{ $ignored_cms_trans_error->funeral_director }}</td>
                                            <td>{{ $ignored_cms_trans_error->cemetery }}</td>
                                            <td>{{ $ignored_cms_trans_error->item_number }}</td>
                                            <td>{{ $ignored_cms_trans_error->quantity }}</td>
                                            <td>{{ $ignored_cms_trans_error->rewards }}</td>
                                            <td>{{ $ignored_cms_trans_error->rewards_redeemed }}</td>
                                            <td>
                                                <form action="{{ route('unignore.cms.trans.errors', $ignored_cms_trans_error->id) }}" method="POST" class="include_error">
                                                    @csrf
                                                    <button type="submit" class="btn btn-xs btn-rounded btn-danger">Re-Include</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    
                    @endif   

                </div>
            </div>
        </div>
        <!-- END card -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script type="text/javascript">
        
        jQuery('#errors_table').dataTable({
            "bLengthChange":false,
            "info":false,
            "bFilter": false,
            "columnDefs": [
                { "orderable": false, "targets": 11 }
            ]
        });

        jQuery('#ignored_errors_table').dataTable({
            "bLengthChange":false,
            "info":false,
            "bFilter": false,
            "columnDefs": [
                { "orderable": false, "targets": 11 }
            ]
        });

        $(".ignore_error").on("submit", function(){
            return confirm("Do you want to ignore this error ?");
        });

        $(".include_error").on("submit", function(){
            return confirm("Do you want to include this error ?");
        });

    </script>

@endsection