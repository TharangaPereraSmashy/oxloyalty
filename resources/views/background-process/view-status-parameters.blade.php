@extends('layouts.master')

@section('title', 'Background Process Details')

@section('content')

    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid  container-fixed-lg">
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('view.status.parameters') }}" class="fa fa-angle-left go-back"></a>Status & Parameters</div>
        </div>
        <!-- START card -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="card card-white card-shadow card-special">
                    <div class="card-block">
                        <div class="page-header customized">
                            <div class="card-sub-title pull-left">Background Job Details</div>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-hover demo-table-dynamic table-responsive-block">
                            <thead>
                            <tr>
                                <th>Job</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr class="gradeA">
                                    <td>CMS -> LP Sync</td>
                                    <td>
                                        @if(count($cms_lp_sync)>0 && $cms_lp_sync[0]->run_start != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($cms_lp_sync[0]->run_start)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($cms_lp_sync)>0 && $cms_lp_sync[0]->run_end != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($cms_lp_sync[0]->run_end)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($cms_lp_sync)>0 && $cms_lp_sync[0]->is_error == 1)
                                            <span class="label label-danger item-status">ERROR</span>
                                        @elseif(count($cms_lp_sync)>0 && $cms_lp_sync[0]->is_error == 0)
                                            <span class="label label-success item-status">SUCCESS</span>
                                        @else
                                        @endif
                                    </td>
                                </tr>
                                <tr class="gradeA">
                                    <td>FD Sync</td>
                                    <td>
                                        @if(count($fd_sync)>0 && $fd_sync[0]->run_start != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($fd_sync[0]->run_start)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($fd_sync)>0 && $fd_sync[0]->run_end != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($fd_sync[0]->run_end)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($fd_sync)>0 && $fd_sync[0]->is_error == 1)
                                            <span class="label label-danger item-status">ERROR</span>
                                        @elseif(count($fd_sync)>0 && $fd_sync[0]->is_error == 0)
                                            <span class="label label-success item-status">SUCCESS</span>
                                        @else
                                        @endif
                                    </td>
                                </tr>
                                <tr class="gradeA">
                                    <td>Rewards</td>
                                    <td>
                                        @if(count($rewards)>0 && $rewards[0]->run_start != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($rewards[0]->run_start)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($rewards)>0 && $rewards[0]->run_end != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($rewards[0]->run_end)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($rewards)>0 && $rewards[0]->is_error == 1)
                                            <span class="label label-danger item-status">ERROR</span>
                                        @elseif(count($rewards)>0 && $rewards[0]->is_error == 0)
                                            <span class="label label-success item-status">SUCCESS</span>
                                        @else
                                        @endif
                                    </td>
                                </tr>
                                <tr class="gradeA">
                                    <td>Redeems</td>
                                    <td>
                                        @if(count($redeems)>0 && $redeems[0]->run_start != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($redeems[0]->run_start)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($redeems)>0 && $redeems[0]->run_end != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($redeems[0]->run_end)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($redeems)>0 && $redeems[0]->is_error == 1)
                                            <span class="label label-danger item-status">ERROR</span>
                                        @elseif(count($redeems)>0 && $redeems[0]->is_error == 0)
                                            <span class="label label-success item-status">SUCCESS</span>
                                        @else
                                        @endif
                                    </td>
                                </tr>
                                <tr class="gradeA">
                                    <td>CN on Rewards</td>
                                    <td>
                                        @if(count($cn_on_rewards)>0 && $cn_on_rewards[0]->run_start != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($cn_on_rewards[0]->run_start)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($cn_on_rewards)>0 && $cn_on_rewards[0]->run_end != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($cn_on_rewards[0]->run_end)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($cn_on_rewards)>0 && $cn_on_rewards[0]->is_error == 1)
                                            <span class="label label-danger item-status">ERROR</span>
                                        @elseif(count($cn_on_rewards)>0 && $cn_on_rewards[0]->is_error == 0)
                                            <span class="label label-success item-status">SUCCESS</span>
                                        @else
                                        @endif
                                    </td>
                                </tr>
                                <tr class="gradeA">
                                    <td>CN on Redeems</td>
                                    <td>
                                        @if(count($cn_on_redeems)>0 && $cn_on_redeems[0]->run_start != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($cn_on_redeems[0]->run_start)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($cn_on_redeems)>0 && $cn_on_redeems[0]->run_end != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($cn_on_redeems[0]->run_end)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($cn_on_redeems)>0 && $cn_on_redeems[0]->is_error == 1)
                                            <span class="label label-danger item-status">ERROR</span>
                                        @elseif(count($cn_on_redeems)>0 && $cn_on_redeems[0]->is_error == 0)
                                            <span class="label label-success item-status">SUCCESS</span>
                                        @else
                                        @endif
                                    </td>
                                </tr>
                                <tr class="gradeA">
                                    <td>Expiry</td>
                                    <td>
                                        @if(count($expiry)>0 && $expiry[0]->run_start != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($expiry[0]->run_start)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($expiry)>0 && $expiry[0]->run_end != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($expiry[0]->run_end)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($expiry)>0 && $expiry[0]->is_error == 1)
                                            <span class="label label-danger item-status">ERROR</span>
                                        @elseif(count($expiry)>0 && $expiry[0]->is_error == 0)
                                            <span class="label label-success item-status">SUCCESS</span>
                                        @else
                                        @endif
                                    </td>
                                </tr>
                                <tr class="gradeA">
                                    <td>LP -> CMS Sync</td>
                                    <td>
                                        @if(count($lp_cms_sync)>0 && $lp_cms_sync[0]->run_start != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($lp_cms_sync[0]->run_start)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($lp_cms_sync)>0 && $lp_cms_sync[0]->run_end != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($lp_cms_sync[0]->run_end)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($lp_cms_sync)>0 && $lp_cms_sync[0]->is_error == 1)
                                            <span class="label label-danger item-status">ERROR</span>
                                        @elseif(count($lp_cms_sync)>0 && $lp_cms_sync[0]->is_error == 0)
                                            <span class="label label-success item-status">SUCCESS</span>
                                        @else
                                        @endif
                                    </td>
                                </tr>
                                <tr class="gradeA">
                                    <td>GL -> CMS Sync</td>
                                    <td>
                                        @if(count($gl_cms_sync)>0 && $gl_cms_sync[0]->run_start != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($gl_cms_sync[0]->run_start)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($gl_cms_sync)>0 && $gl_cms_sync[0]->run_end != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($gl_cms_sync[0]->run_end)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($gl_cms_sync)>0 && $gl_cms_sync[0]->is_error == 1)
                                            <span class="label label-danger item-status">ERROR</span>
                                        @elseif(count($gl_cms_sync)>0 && $gl_cms_sync[0]->is_error == 0)
                                            <span class="label label-success item-status">SUCCESS</span>
                                        @else
                                        @endif
                                    </td>
                                </tr>
                                <tr class="gradeA">
                                    <td>Revaluation</td>
                                    <td>
                                        @if(count($revaluation)>0 && $revaluation[0]->run_start != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($revaluation[0]->run_start)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($revaluation)>0 && $revaluation[0]->run_end != "")
                                            {{ date('d-m-Y H:i:s A', strtotime($revaluation[0]->run_end)) }}
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($revaluation)>0 && $revaluation[0]->is_error == 1)
                                            <span class="label label-danger item-status">ERROR</span>
                                        @elseif(count($revaluation)>0 && $revaluation[0]->is_error == 0)
                                            <span class="label label-success item-status">SUCCESS</span>
                                        @else
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{ route('update.parameters') }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="page-header padding-bottom-20">
                                <div class="card-sub-title pull-left">Background Job Parameters</div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>{{ $config1[0]->description }}</label>
                                        <input type="hidden" name="param1" value={{ $config1[0]->parameter }} />
                                        <input type="text" class="form-control" id="val1" placeholder={{ $config1[0]->description }} name="val1" value={{ $config1[0]->value }} required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>{{ $config2[0]->description }}</label>
                                        <input type="hidden" name="param2" value={{ $config2[0]->parameter }} />
                                        <input type="text" class="form-control int-only" id="val2" placeholder={{ $config2[0]->description }} name="val2" value={{ $config2[0]->value }} required>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>{{ $config3[0]->description }}</label>
                                        <input type="hidden" name="param3" value={{ $config3[0]->parameter }} />
                                        <input type="text" class="form-control" id="val3" placeholder={{ $config3[0]->description }} name="val3" value={{ $config3[0]->value }} required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>{{ $config4[0]->description }}</label>
                                        <input type="hidden" name="param4" value={{ $config4[0]->parameter }} />
                                        <input type="password" class="form-control" id="val4" placeholder={{ $config4[0]->description }} name="val4" value={{ $config4[0]->value }} required>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>{{ $config5[0]->description }}</label>
                                        <input type="hidden" name="param5" value={{ $config5[0]->parameter }} />
                                        <input type="text" class="form-control" id="val5" placeholder={{ $config5[0]->description }} name="val5" value={{ $config5[0]->value }} required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>{{ $config6[0]->description }}</label>
                                        <input type="hidden" name="param6" value={{ $config6[0]->parameter }} />
                                        <input type="text" class="form-control" id="val6" placeholder={{ $config6[0]->description }} name="val6" value={{ $config6[0]->value }} required>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12 text-right">
                                    <div class="form-group form-group-default">
                                        <button class="btn btn-success btn-lg" type="submit">Update Parameters</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection