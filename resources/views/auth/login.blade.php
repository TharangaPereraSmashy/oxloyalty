<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('css/theme.css') }}" rel="stylesheet" type="text/css">
    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
        }
    </script>
</head>
<body class="fixed-header ">
<div style="background-color: #ffffff;" class="login-wrapper ">

    <div class="bg-pic">

        <img style="opacity: 1;" src="assets/img/demo/opusxentax-login-bg.png" data-src="assets/img/demo/opusxentax-login-bg.png" data-src-retina="assets/img/demo/opusxentax-login-bg.png" alt="" class="lazy">


        {{--<div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
            <h2 class="semi-bold text-white">
                Pages make it easy to enjoy what matters the most in the life</h2>
            <p class="small">
                images Displayed are solely for representation purposes only, All work copyright of respective
                owner, otherwise © 2013-2014 REVOX.
            </p>
        </div>--}}

    </div>

    <div class="login-container bg-white" style="padding-top: 4%;">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
            {{--<img src="assets/img/logo/logo_2x-black.png" alt="logo" data-src="assets/img/logo/logo_2x-black.png" data-src-retina="assets/img/logo/logo_2x-black.png" width="78" height="22">--}}
            <p class="p-t-35">Sign into your account</p>

            <form  method="POST" action="{{ route('login') }}" id="form-login" class="p-t-15" role="form">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="form-group form-group-default">
                        <label>Login</label>
                        <div class="controls">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   required  placeholder="E-mail">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                     <strong>{{ $errors->first('email') }}</strong>
                                 </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="form-group form-group-default">
                        <label>Password</label>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="controls">
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Credentials">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 no-padding sm-p-l-10">
                        <div class="checkbox ">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="checkbox1">
                            <label for="checkbox1">Keep Me Signed in</label>
                        </div>
                    </div>
                    {{--<div class="col-md-6 d-flex align-items-center justify-content-end">
                        <a href="{{ route('password.request') }}" class="text-info small">Forgot Your Password?</a>
                    </div>--}}
                </div>

                <button class="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>
            </form>

            {{--<div class="pull-bottom sm-pull-bottom">
                <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
                    <div class="col-sm-3 col-md-2 no-padding">
                        <img alt="" class="m-t-5" data-src="assets/img/demo/pages_icon.png" data-src-retina="assets/img/demo/pages_icon_2x.png" height="60" src="assets/img/demo/pages_icon.png" width="60">
                    </div>
                    <div class="col-sm-9 no-padding m-t-10">
                        <p>
                            <small>
                                Create a pages account. If you have a facebook account, log into it for this
                                process. Sign in with <a href="#" class="text-info">Facebook</a> or <a href="#" class="text-info">Google</a>
                            </small>
                        </p>
                    </div>
                </div>
            </div>--}}
        </div>
    </div>

</div>

<!-- BEGIN VENDOR JS -->
<script src="{{ asset('js/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/modernizr/modernizr.custom.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/tether/tether.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/bootstrap/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery-actual/jquery.actual.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/classie/classie.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/switchery/switchery.min.js') }}" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset('js/pages.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<!-- END CORE TEMPLATE JS -->


</body>

</html>