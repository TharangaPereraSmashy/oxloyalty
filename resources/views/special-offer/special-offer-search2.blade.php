@extends('layouts.master')

@section('title', 'Special Offers')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('special_offers.index') }}">Special Offer</a></li>
                    <li class="breadcrumb-item active">All Special Offers</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="page-header">
            <div class="card-title pull-left" style="padding-top: 10px;">Search Results @if($search_value!="")For "{{ $search_value }}" @endif </div>
            <a class="btn btn-warning btn-rounded" style="margin-left: 30px;" href="{{ route('special_offers.index') }}"><span class="fa fa-remove"></span>&nbsp;Clear</a>
                        
            <div class="pull-right">
                <form method="POST" action="{{ action('SpecialOfferController@searchSpecialOffers') }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="input-group">
                        <input type="text" class="form-control form-rounded" name="search_value" value="{{ old('search_value') }}" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-info btn-rounded btn-sm" type="submit"><span class="fa fa-search"></span></button>
                        </span>
                    </div>
                </form>
            </div>
            
            <div class="clearfix"></div>
        </div>
        <div class="card card-white card-shadow card-special">

            @if(count($special_offers)==0)
                <div class="card-block text-center">No Results Available</div>
            @else

            <div class="card-block">
                <table class="table table-hover demo-table-search table-responsive-block">
                    <thead>
                    <tr>
                        <th style="width: 10% !important;">Code</th>
                        <th style="width: 16% !important;">Description</th>
                        <th style="width: 10% !important;">Item Code</th>
                        <th style="width: 16% !important;">Item</th>
                        <th style="width: 10% !important;">Cemetery</th>
                        <th style="width: 8% !important;">Start Date</th>
                        <th style="width: 8% !important;">End Date</th>
                        <th style="width: 22% !important;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($special_offers as $special_offer)

                        <tr class="gradeA">
                            <td>{{ $special_offer->code }}</td>
                            <td>{{ $special_offer->description }}</td>
                            <td>{{ $special_offer->item_code }}</td>
                            <td>{{ $special_offer->item }}</td>
                            <td>{{ $special_offer->cemetery }}</td>
                            <td>{{ date('d-m-Y', strtotime($special_offer->start_date)) }}</td>
                            <td>{{ date('d-m-Y', strtotime($special_offer->end_date)) }}</td>
                            <td>
                                <form action="{{ route('special_offers.destroy',$special_offer->id) }}" method="POST" class="delete_record">

                                    @if(Session::get('read_special_offer'))
                                        <a class="btn btn-xs btn-rounded btn-complete" href="{{ route('special_offers.show',$special_offer->id) }}"><span class="fa fa-eye"></span></a>
                                    @endif

                                    @if(Session::get('update_special_offer'))
                                        <a class="btn btn-xs btn-rounded btn-success" href="{{ route('special_offers.edit',$special_offer->id) }}"><span class="fa fa-edit"></span></a>
                                    @endif

                                    @csrf
                                    @method('DELETE')

                                    @if(Session::get('delete_special_offer'))
                                        <button type="submit" class="btn btn-xs btn-rounded btn-danger"><span class="fa fa-trash"></span></button>
                                    @endif

                                </form>
                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
            </div>

            @endif

        </div>
        <!-- END card -->

        <div class="row pagination-div">
            <div class="col-md-12">
                <div class="pull-right">
                    {!! $special_offers->links() !!}
                </div>
            </div>
        </div>

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script>
        $(".delete_record").on("submit", function(){
            return confirm("Do you want to delete this record ?");
        });
    </script>

@endsection