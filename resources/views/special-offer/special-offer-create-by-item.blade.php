@extends('layouts.master')

@section('title', 'Add Special Offer')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron hide" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('items.show',$item_id) }}">Item</a></li>
                    <li class="breadcrumb-item active">Add Special Offer</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="page-header padding-20-top">
            <div class="card-title"><a href="{{ route('items.show',$item_id) }}" class="fa fa-angle-left go-back"></a> Add Special Offer</div>
        </div>
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        @if(isset($status_inactive))
            <div class="alert alert-warning">{{ $status_inactive }}</div>
        @endif
        <!-- START card -->
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{ route('store_special_offer_by_item', array('item' => $item_id)) }}" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Special Offer Code</label>
                                        <input type="text" class="form-control" value="{{ old('code') }}" id="code" placeholder="Special Offer Code" name="code" required>
                                        @if ($errors->has('code'))
                                            <div class="error">{{ $errors->first('code') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-group-default required">
                                        <label>Special Offer Description</label>
                                        <input type="text" class="form-control" value="{{ old('description') }}" id="description" placeholder="Special Offer Description" name="description" required>
                                        @if ($errors->has('description'))
                                            <div class="error">{{ $errors->first('description') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-5">
                                    <div class="form-group form-group-default required">
                                        <label>Item</label>
                                        <select class="full-width" data-placeholder="Select Item" data-init-plugin="select2" name="item_id" required>
                                            @foreach($items as $item )
                                                <option value="{{ $item->id }}" @if($item->id==old('item_id')) selected @endif>{{ $item->item_name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('item_id'))
                                            <div class="error">{{ $errors->first('item_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Cemetery</label>
                                        <select class="full-width" data-placeholder="Select Cemetery" data-init-plugin="select2" name="cemetery_id" required>
                                            <option value="" ></option>
                                            @foreach($cemeteries as $cemetery )
                                                <option value="{{ $cemetery->id }}" @if($item->id==old('cemetery_id')) selected @endif>{{ $cemetery->cemetery_name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('cemetery_id'))
                                            <div class="error">{{ $errors->first('cemetery_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>Status</label>
                                        <div class="radio radio-success radio-padding">
                                            <div class="row">
                                                <div class="col-md-4 radio-labels">
                                                    <input type="radio" value="1" name="status" id="active" checked="checked">
                                                    <label for="active">Active</label>
                                                </div>
                                                <div class="col-md-4 radio-labels">
                                                    <input type="radio" value="0" name="status" id="inactive">
                                                    <label for="inactive">Inactive</label>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($errors->has('status'))
                                            <div class="error">{{ $errors->first('status') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>FD From Code</label>
                                        <select class="full-width" data-placeholder="Select FD From Code" data-init-plugin="select2" name="fd_from_code" required>
                                            <option value="" ></option>
                                            @foreach($fd_from_codes as $fd_from_code )
                                                <option value="{{ $fd_from_code->code }}" @if($fd_from_code->code==old('fd_from_code')) selected @endif>{{ $fd_from_code->fd_name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('fd_from_code'))
                                            <div class="error">{{ $errors->first('fd_from_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required">
                                        <label>FD To Code</label>
                                        <select class="full-width" data-placeholder="Select FD To Code" data-init-plugin="select2" name="fd_to_code" required>
                                            <option value="" ></option>
                                            @foreach($fd_to_codes as $fd_to_code )
                                                <option value="{{ $fd_to_code->code }}" @if($fd_to_code->code==old('fd_to_code')) selected @endif>{{ $fd_to_code->fd_name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('fd_to_code'))
                                            <div class="error">{{ $errors->first('fd_to_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>Earns Loyalty Dollars</label>
                                        <input type="text" class="form-control float-only" value="{{ old('to_earn') }}" id="to_earn" placeholder="Earns Loyalty Dollars" name="to_earn" required>
                                        @if ($errors->has('to_earn'))
                                            <div class="error">{{ $errors->first('to_earn') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>Loyalty Dollars to Redeem</label>
                                        <input type="text" class="form-control float-only" value="{{ old('to_redeem') }}" id="to_redeem" placeholder="Loyalty Dollars to Redeem" name="to_redeem" required>
                                        @if ($errors->has('to_redeem'))
                                            <div class="error">{{ $errors->first('to_redeem') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>Start Date</label>
                                        <input type="text" class="form-control" value="{{ old('start_date') }}" id="start_date" placeholder="Start Date" name="start_date" required>
                                        @if ($errors->has('start_date'))
                                            <div class="error">{{ $errors->first('start_date') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>End Date</label>
                                        <input type="text" class="form-control" value="{{ old('end_date') }}" id="end_date" placeholder="End Date" name="end_date" required>
                                        @if ($errors->has('end_date'))
                                            <div class="error">{{ $errors->first('end_date') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix padding-bottom-20">
                        {{-- <div class="col-md-6">
                            <a class="btn btn-light btn-lg" href="{{ route('special_offers.index') }}">View all Special Offers</a>
                        </div> --}}
                        <div class="col-md-12 text-right">
                            @if(Session::get('create_item'))
                                <button class="btn btn-primary btn-lg" type="submit">Add Special Offer</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

    <script type="text/javascript">

        $("#start_date").datepicker({
            startDate: new Date(),
            format: 'dd-mm-yyyy',
            autoclose: true
        }).on('changeDate', function (selected) {
            $("#end_date").val("").datepicker("update");
            var minDate = new Date(selected.date.valueOf());
            minDate.setDate(minDate.getDate() + 1);
            $('#end_date').datepicker('setStartDate', minDate);
        });

        $("#end_date").datepicker({
            startDate: '+1d',
            format: 'dd-mm-yyyy',
            autoclose: true
        });

    </script>

@endsection