@extends('layouts.master')

@section('title', 'View Special Offer')

@section('content')

    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('special_offers.index') }}">Special Offer</a></li>
                    <li class="breadcrumb-item active">View Special Offer</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        @if(\Session::has('success'))
            <div class="alert alert-success" id="success_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('success')}}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger" id="error_msg" data-remove-delay="3000">
                <button class="close" data-dismiss="alert"></button>
                {{\Session::get('error')}}
            </div>
        @endif
        <!-- START card -->
        <div class="page-header">
            <div class="card-title"><a href="{{ route('special_offers.index') }}" class="fa fa-angle-left"></a> View Special Offer</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="#" id="form-personal" role="form" autocomplete="off">
                    @csrf
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Special Offer Code</label>
                                        <input type="text" class="form-control" id="code" placeholder="Special Offer Code" value="{{ $special_offer->code }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-group-default required">
                                        <label>Special Offer Description</label>
                                        <input type="text" class="form-control" id="description" placeholder="Special Offer Description" name="description" value="{{ $special_offer->description }}" disabled>
                                        @if ($errors->has('description'))
                                            <div class="error">{{ $errors->first('description') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-5">
                                    <div class="form-group form-group-default required">
                                        <label>Item</label>
                                        <select class="full-width" data-placeholder="Select Item" data-init-plugin="select2" name="item_id" disabled>
                                            <option value="" ></option>
                                            @foreach($items as $item )
                                                <option value="{{ $item->id }}" @if($item->id==$special_offer->item_id) selected @endif>{{ $item->item_name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('item_id'))
                                            <div class="error">{{ $errors->first('item_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default required">
                                        <label>Cemetery</label>
                                        <select class="full-width" data-placeholder="Select Cemetery" data-init-plugin="select2" name="cemetery_id" disabled>
                                            <option value="" ></option>
                                            @foreach($cemeteries as $cemetery )
                                                <option value="{{ $cemetery->id }}" @if($cemetery->id==$special_offer->cemetery_id) selected @endif>{{ $cemetery->cemetery_name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('cemetery_id'))
                                            <div class="error">{{ $errors->first('cemetery_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>Status</label>
                                        <div class="radio radio-success radio-padding">
                                            <div class="row">
                                                <div class="col-md-4 radio-labels">
                                                    <input type="radio" value="1" name="status" id="active" @if($special_offer->status == '1') checked @endif disabled>
                                                    <label for="active">Active</label>
                                                </div>
                                                <div class="col-md-4 radio-labels">
                                                    <input type="radio" value="0" name="status" id="inactive" @if($special_offer->status == '0') checked @endif disabled>
                                                    <label for="inactive">Inactive</label>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($errors->has('status'))
                                            <div class="error">{{ $errors->first('status') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>FD From Code</label>
                                        <select class="full-width" data-placeholder="Select FD From Code" data-init-plugin="select2" name="fd_from_code" disabled>
                                            <option value="" ></option>
                                            @foreach($fd_from_codes as $fd_from_code )
                                                <option value="{{ $fd_from_code->code }}" @if($fd_from_code->code==$special_offer->fd_from_code) selected @endif>{{ $fd_from_code->fd_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>FD To Code</label>
                                        <select class="full-width" data-placeholder="Select FD To Code" data-init-plugin="select2" name="fd_to_code" disabled>
                                            <option value="" ></option>
                                            @foreach($fd_to_codes as $fd_to_code )
                                                <option value="{{ $fd_to_code->code }}" @if($fd_to_code->code==$special_offer->fd_to_code) selected @endif>{{ $fd_to_code->fd_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-white card-shadow card-special">
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>Earns Loyalty Dollars</label>
                                        <input type="text" class="form-control float-only" value="{{ $special_offer->to_earn }}" id="to_earn" placeholder="Points To Earn" name="to_earn" disabled>
                                        @if ($errors->has('to_earn'))
                                            <div class="error">{{ $errors->first('to_earn') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>Loyalty Dollars to Redeem</label>
                                        <input type="text" class="form-control float-only" value="{{ $special_offer->to_redeem }}" id="to_redeem" placeholder="Points To Redeem" name="to_redeem" disabled>
                                        @if ($errors->has('to_redeem'))
                                            <div class="error">{{ $errors->first('to_redeem') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>Start Date</label>
                                        <input type="text" class="form-control" value="{{ date('d-m-Y', strtotime($special_offer->start_date)) }}" id="start_date" placeholder="Start Date" name="start_date" disabled>
                                        @if ($errors->has('start_date'))
                                            <div class="error">{{ $errors->first('start_date') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default required">
                                        <label>End Date</label>
                                        <input type="text" class="form-control" value="{{ date('d-m-Y', strtotime($special_offer->end_date)) }}" id="end_date" placeholder="End Date" name="end_date" disabled>
                                        @if ($errors->has('end_date'))
                                            <div class="error">{{ $errors->first('end_date') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix padding-bottom-20">
                        <div class="col-md-12 text-right">
                            <a class="btn btn-success btn-lg" href="{{ route('special_offers.index') }}"> Ok</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card -->
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

@endsection


@section('script')

@endsection