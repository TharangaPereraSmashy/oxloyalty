<div class="header hide">
    <!-- START MOBILE SIDEBAR TOGGLE -->
    <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
    </a>
    <!-- END MOBILE SIDEBAR TOGGLE -->
    <div class="">
        <div class="brand inline   ">
            <img src="{{ asset('assets/img/logo/logo_2x-black.png') }}" alt="logo" data-src="{{ asset('assets/img/logo/logo_2x-black.png') }}" data-src-retina="{{ asset('assets/img/logo/logo_2x-black.png') }}" width="100" height="30">
        </div>
    </div>
    <div class="d-flex align-items-center">
        <!-- START User Info-->
        <div class="pull-left p-r-10 fs-14 font-heading hidden-md-down">
            {{--<span class="semi-bold">David</span> <span class="text-master">Nest</span>--}}
            <span class="text-master">{{ Auth::user()->name }}</span>
        </div>
        <div class="dropdown pull-right hidden-md-down">
            <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="thumbnail-wrapper d32 circular inline">
              <img src="{{ asset('assets/img/profiles/default-user.png') }}" alt="" data-src="{{ asset('assets/img/profiles/default-user.png') }}" data-src-retina="{{ asset('assets/img/profiles/default-user.png') }}" width="32" height="32">
              </span>
            </button>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                <a href="#" class="dropdown-item"><i class="pg-settings_small"></i> Settings</a>
                <a href="{{ route('changePassword') }}" class="dropdown-item"><i class="fa fa-refresh"></i> Change Password</a>                
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" class="clearfix bg-master-lighter dropdown-item">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
        <!-- END User Info-->
    </div>
</div>