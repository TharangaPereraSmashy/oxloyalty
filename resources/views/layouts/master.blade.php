<!doctype html>

<html lang="{{ app()->getLocale() }}">

<head>
    @include('layouts.head')
</head>

<body class="fixed-header sidebar-visible menu-pin">

<!-- BEGIN SIDEBPANEL-->
@include('layouts.nav')
<!-- END SIDEBPANEL-->

<!-- START PAGE-CONTAINER -->
<div class="page-container ">

    <!-- START HEADER -->
    @include('layouts.header')
    <!-- END HEADER -->

    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper ">

        <!-- START PAGE CONTENT -->
        <div class="content ">

            @yield('content')

        </div>
        <!-- END PAGE CONTENT -->

        <!-- START COPYRIGHT -->
        @include('layouts.footer')
        <!-- END COPYRIGHT -->

    </div>
    <!-- END PAGE CONTENT WRAPPER -->

</div>
<!-- END PAGE CONTAINER -->

@include('layouts.bottom')

@yield('script')

</body>

</html>
