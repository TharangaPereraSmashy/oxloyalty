<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
        <!--img src="{{ asset('assets/img/logo/logo_2x-white.png') }}" alt="logo" class="brand" data-src="{{ asset('assets/img/logo/logo_2x-white.png') }}" data-src-retina="{{ asset('assets/img/logo/logo_2x-white.png') }}" width="100" height="30"-->
        <div class="sidebar-header-controls">
            OX Loyalty
            <button type="button" class="btn btn-link hidden-md-down" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
            </button>
        </div>
        <h3>Welcome {{ Auth::user()->name }}
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            <a href="{{ route('changePassword') }}"><i class="fa fa-cog"></i></a>
        </h3>
    </div>
    <!-- END SIDEBAR MENU HEADER-->
    <!-- START SIDEBAR MENU -->
    <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">

            @if(Auth::user()->type=="3")
                <li class="m-t-30">
                    <a href="{{ url('funeral_directors/funeral_director_welcome_page') }}">
                        <span class="title">FD Dashboard</span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
                </li>
            @elseif(Auth::user()->type=="2")
                <li class="m-t-30">
                    <a href="{{ url('staff/staff_welcome_page') }}">
                        <span class="title">Staff Dashboard</span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-home"></i></span>
                </li>
            @else
                <li class="m-t-30">
                    <a href="{{ url('home') }}">
                        <span class="title">Dashboard</span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-home"></i></span>
                </li>
            @endif

            @if(Session::get('read_register'))
                <li>
                    <a href="javascript:;">
                        <span class="title">Users</span>
                        <span class=" arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-user"></i></span>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('registeruser.index') }}">Manage Users</a>
                            <span class="icon-thumbnail">R</span>
                        </li>
                        @if(Session::get('create_register'))
                            <li>
                                <a href="{{ route('registeruser.create') }}">Add User</a>
                                <span class="icon-thumbnail">R</span>
                            </li>
                        @endif
                        <li>
                            <a href="{{ url('profile') }}">User Profile</a>
                            <span class="icon-thumbnail">P</span>
                        </li>
                    </ul>
                </li>
            @endif

            @if(Session::get('read_cemetery'))
                <li>
                    <a href="javascript:;">
                        <span class="title">Cemeteries</span>
                        <span class=" arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-plus-square"></i></span>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('cemeteries.index') }}">Manage Cemeteries</a>
                            <span class="icon-thumbnail">M</span>
                        </li>
                        @if(Session::get('create_cemetery'))
                            <li>
                                <a href="{{ route('cemeteries.create') }}">Add Cemetery</a>
                                <span class="icon-thumbnail">A</span>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(Session::get('read_item'))
                <li>
                    <a href="javascript:;">
                        <span class="title">Items</span>
                        <span class=" arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-cube"></i></span>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('items.index') }}">Manage Items</a>
                            <span class="icon-thumbnail">M</span>
                        </li>
                        @if(Session::get('create_item'))
                            <li>
                                <a href="{{ route('items.create') }}">Add Item</a>
                                <span class="icon-thumbnail">A</span>
                            </li>
                        @endif
                        <li>
                            <a href="{{ route('import.export.view') }}">Import Export Items</a>
                            <span class="icon-thumbnail">IE</span>
                        </li>
                        <li>
                            <a href="{{ route('import.export.loyalty.items.view') }}">Import Export Loyalty Items</a>
                            <span class="icon-thumbnail">IE</span>
                        </li>
                    </ul>
                </li>
            @endif

            {{-- @if(Session::get('read_cemetery_item'))
                <li>
                    <a href="javascript:;">
                        <span class="title">Item Loyalty</span>
                        <span class=" arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-shopping-bag"></i></span>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('cemetery_items.index') }}">Manage Item Loyalty</a>
                            <span class="icon-thumbnail">M</span>
                        </li>
                        @if(Session::get('create_cemetery_item'))
                            <li>
                                <a href="{{ route('cemetery_items.create') }}">Add Item Loyalty</a>
                                <span class="icon-thumbnail">A</span>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(Session::get('read_special_offer'))
                <li>
                    <a href="javascript:;">
                        <span class="title">Special Offers</span>
                        <span class=" arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-gift"></i></span>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('special_offers.index') }}">Manage Special Offers</a>
                            <span class="icon-thumbnail">M</span>
                        </li>
                        @if(Session::get('create_special_offer'))
                            <li>
                                <a href="{{ route('special_offers.create') }}">Add Special Offer</a>
                                <span class="icon-thumbnail">A</span>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif --}}

            @if(Session::get('read_funeral_director'))
                <li>
                    <a href="javascript:;">
                        <span class="title">Funeral Directors</span>
                        <span class=" arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('funeral_directors.index') }}">Manage Funeral Directors</a>
                            <span class="icon-thumbnail">M</span>
                        </li>
                        @if(Session::get('create_funeral_director'))
                            <li>
                                <a href="{{ route('funeral_directors.create') }}">Add Funeral Director</a>
                                <span class="icon-thumbnail">A</span>
                            </li>
                        @endif
                        <li>
                            <a href="{{ route('point_reports.index') }}">Loyalty $ Reports</a>
                            <span class="icon-thumbnail">A</span>
                        </li>
                    </ul>
                </li>
            @endif

            @if(Session::get('read_ld_transfer_request'))
                <li>
                    <a href="{{ url('funeral_directors/search_loyalty_dollar_transfer_requests') }}">
                        <span class="title">L$ Transfer Requests</span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-dollar"></i></span>
                </li>
            @endif

            @if(Session::get('read_ld_revaluation_request'))
                <li>
                    <a href="{{ url('funeral_directors/loyalty_dollar_revaluation_requests') }}">
                        <span class="title">Loyalty $ Revaluation</span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-dollar"></i></span>
                </li>
            @endif
            
            @if(Session::get('read_audit'))
                <li>
                    <a href="{{ route('audits.index') }}">
                        <span class="title">Audits</span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-th-list"></i></span>
                </li>
            @endif

            @if(Session::get('read_configuration'))
                <li>
                    <a href="{{ route('configurations.create') }}">
                        <span class="title">Configurations</span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-th-list"></i></span>
                </li>
            @endif

            @if(Session::get('read_background_process'))
                <li>
                    <a href="javascript:;">
                        <span class="title">Background Process</span>
                        <span class=" arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('view.error.details') }}">Error Details</a>
                            <span class="icon-thumbnail">E</span>
                        </li>
                        <li>
                            <a href="{{ route('view.status.parameters') }}">Status & Parameters</a>
                            <span class="icon-thumbnail">P</span>
                        </li>
                    </ul>
                </li>
            @endif

        </ul>
        <div class="clearfix"></div>
        
    </div>
    <!-- END SIDEBAR MENU -->
</nav>