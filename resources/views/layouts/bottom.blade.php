<!-- BEGIN VENDOR JS -->
<script src="{{ asset('js/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/modernizr/modernizr.custom.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/tether/tether.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/bootstrap/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery-actual/jquery.actual.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/classie/classie.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/switchery/switchery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery-datatable/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/jquery-datatable/dataTables.bootstrap.js') }}" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset('js/pages.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/users.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>
<!-- END CORE TEMPLATE JS -->
