<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFuneralDirectorsColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('funeral_directors', function($table) {
            $table->dropColumn('loyalty_dollar_balance');
            $table->dropColumn('earned_dollars');
            $table->dropColumn('redeemed_dollars');
            $table->dropColumn('expired_dollars');
            $table->dropColumn('adjusted_dollars');
            $table->dropColumn('transfered_dollars');
            $table->dropColumn('reversed_dollars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('funeral_directors', function($table) {
            $table->float('loyalty_dollar_balance', 8, 2)->default(0)->nullable();
            $table->float('earned_dollars', 8, 2)->default(0)->nullable();
            $table->float('redeemed_dollars', 8, 2)->default(0)->nullable();
            $table->float('expired_dollars', 8, 2)->default(0)->nullable();
            $table->float('adjusted_dollars', 8, 2)->default(0)->nullable();
            $table->float('transfered_dollars', 8, 2)->default(0)->nullable();
            $table->float('reversed_dollars', 8, 2)->default(0)->nullable();
        });
    }
}
