<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpRevaluationDocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lp_revaluation_doc', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('revaluation_doc_date');
            $table->integer('request_id')->nullable()->references('id')->on('revaluation_requests');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lp_revaluation_doc');
    }
}
