<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpConsumedPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lp_consumed_points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fd_id')->references('id')->on('funeral_directors');
            $table->string('consumed_source');
            $table->integer('source_ref');
            $table->integer('earned_points_ref')->nullable();   
            $table->integer('points_redeemed');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lp_consumed_points');
    }
}
