<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameLpConsumedPointsColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lp_consumed_points', function(Blueprint $table) {
            $table->renameColumn('points_redeemed', 'points_consumed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lp_consumed_points', function(Blueprint $table) {
            $table->renameColumn('points_consumed', 'points_redeemed');
        });
    }
}
