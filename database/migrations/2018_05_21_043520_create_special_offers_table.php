<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('description');
            $table->integer('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->integer('cemetery_id')->references('id')->on('cemeteries')->onDelete('cascade');
            $table->boolean('status')->default(1);
            $table->string('fd_from_code')->nullable();
            $table->string('fd_to_code')->nullable();
            $table->float('to_earn', 8, 2);
            $table->float('to_redeem', 8, 2);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_offers');
    }
}
