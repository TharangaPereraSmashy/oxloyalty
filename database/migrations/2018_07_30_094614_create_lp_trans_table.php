<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lp_trans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cemetery_id')->nullable();
            $table->string('cemetery_code')->nullable();
            $table->string('doc_type');
            $table->string('doc_number');
            $table->dateTime('doc_date');
            $table->integer('fd_id');
            $table->string('fd_code');
            $table->integer('amount');
            $table->string('liability_account');
            $table->boolean('is_error')->default(0);
            $table->string('error')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lp_trans');
    }
}
