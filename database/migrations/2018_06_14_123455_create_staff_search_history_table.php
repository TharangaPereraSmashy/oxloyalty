<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffSearchHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_search_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('search_type');
            $table->integer('fd_id')->nullable();
            $table->integer('cemetery_id')->nullable();
            $table->integer('item_id')->nullable();
            $table->dateTime('searched_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_search_history');
    }
}
