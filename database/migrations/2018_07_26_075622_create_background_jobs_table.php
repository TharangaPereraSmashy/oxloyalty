<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackgroundJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('background_jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('run_date');
            $table->string('run_type');
            $table->timestamp('run_start')->nullable();
            $table->timestamp('run_end')->nullable();
            $table->integer('run_by')->references('id')->on('users')->nullable();
            $table->boolean('is_error')-> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('background_jobs');
    }
}
