<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedCreditNoteToAllTransactionsHistoryView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( "DROP VIEW all_transactions_history_view" );

        DB::statement( "CREATE VIEW all_transactions_history_view AS 
                        SELECT i.doc_number, i.doc_date, i.item_number, t.* FROM lp_invoices i, transactions_history t
                        WHERE t.trans_ref = i.id
                        AND trans_type = 'REWARD'
                        UNION
                        SELECT r.doc_number, r.doc_date, r.item_number, t.* FROM lp_invoices r, transactions_history t
                        WHERE t.trans_ref = r.id
                        AND trans_type = 'REDEEM'
                        UNION
                        SELECT COALESCE(a.id,'999'), a.adjusted_date, null item_number, t.* FROM lp_adjustments a, transactions_history t
                        WHERE t.trans_ref = a.id
                        AND substr(trans_type,1,10) = 'ADJUSTMENT'
                        UNION
                        SELECT COALESCE(a.id,'999'), a.transfer_doc_date, null item_number, t.* FROM lp_transfer_doc a, transactions_history t
                        WHERE t.trans_ref = a.id
                        AND substr(trans_type,1,8) = 'TRANSFER'
                        UNION
                        SELECT COALESCE(a.id,'999'), a.expiry_doc_date, null item_number, t.* FROM lp_expiry_doc a, transactions_history t
                        WHERE t.trans_ref = a.id
                        AND trans_type = 'EXPIRY'
                        UNION
                        SELECT i.doc_number, i.doc_date, i.item_number, t.* FROM lp_invoices i, transactions_history t
                        WHERE t.trans_ref = i.id
                        AND trans_type = 'CNREWARD'
                        UNION
                        SELECT r.doc_number, r.doc_date, r.item_number, t.* FROM lp_invoices r, transactions_history t
                        WHERE t.trans_ref = r.id
                        AND trans_type = 'CNREDEEM'" );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
