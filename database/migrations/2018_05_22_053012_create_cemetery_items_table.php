<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCemeteryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cemetery_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->integer('cemetery_id')->references('id')->on('cemeteries')->onDelete('cascade');
            $table->boolean('status')->default(1);
            $table->float('to_earn', 8, 2);
            $table->float('to_redeem', 8, 2);
            $table->date('start_date');
            $table->date('end_date');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cemetery_items');
    }
}
