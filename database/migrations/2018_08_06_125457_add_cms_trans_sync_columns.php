<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCmsTransSyncColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_trans_sync', function(Blueprint $table) {

            $table->string('applied_to')->nullable();
            $table->string('applied_to_doc_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_trans_sync', function(Blueprint $table) {

            $table->dropColumn('applied_to');
            $table->dropColumn('applied_to_doc_type');
        });
    }
}
