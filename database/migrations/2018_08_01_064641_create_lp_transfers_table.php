<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lp_transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('tranfer_date')->nullable();
            $table->integer('request_id')->nullable()->references('id')->on('loyalty_dollar_requests');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lp_transfers');
    }
}
