<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpEarnedPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lp_earned_points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fd_id')->references('id')->on('funeral_directors');
            $table->string('earned_source');
            $table->integer('source_ref');  
            $table->integer('points_earned');            
            $table->timestamp('points_start_date')->nullable();
            $table->timestamp('points_end_date')->nullable();
            $table->integer('points_balance');
            $table->timestamp('last_run_date')->nullable();
            $table->boolean('is_expired_by_tommorrow')->default(0);
            $table->boolean('is_expired_by_x')->default(0);
            $table->boolean('is_expired_by_y')->default(0);
            $table->boolean('is_expired')->default(0);
            $table->timestamps();
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lp_earned_points');
    }
}
