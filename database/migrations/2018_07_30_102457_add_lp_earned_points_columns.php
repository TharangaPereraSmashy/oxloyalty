<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLpEarnedPointsColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lp_earned_points', function(Blueprint $table) {
            $table->renameColumn('is_expired_by_tommorrow', 'to_be_expired');
            $table->dropColumn('last_run_date');
            $table->integer('expiry_doc_id')->references('id')->on('lp_expiry_doc')->nullable();
            $table->string('gl_liability_code')->nullable();
            $table->string('gl_cost_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lp_earned_points', function(Blueprint $table) {
            $table->renameColumn('to_be_expired', 'is_expired_by_tommorrow');
            $table->timestamp('last_run_date')->nullable();
            $table->dropColumn('expiry_doc_id');
            $table->dropColumn('gl_liability_code');
            $table->dropColumn('gl_cost_code');
        });
    }
}
