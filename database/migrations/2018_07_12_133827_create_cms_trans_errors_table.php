<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsTransErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_trans_errors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cms_sync_id');
            $table->integer('record_id');
            $table->integer('gp_record_id')->nullable();
            $table->string('doc_type')->nullable();
            $table->string('doc_number')->nullable();
            $table->dateTime('doc_date')->nullable();
            $table->string('funeral_director', 15)->nullable();
            $table->string('cemetery')->nullable();        
            $table->string('item_number')->nullable();
            $table->integer('quantity')->nullable();
            $table->decimal('amount', 19, 5)->nullable();
            $table->integer('rewards')->nullable();
            $table->integer('rewards_redeemed')->nullable();
            $table->dateTime('time_date_stamp')->nullable();          
            $table->string('error_number');
            $table->string('error_type')->nullable();
            $table->string('error');
            $table->integer('is_reprocessed');
            $table->integer('is_ignored');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_trans_errors');
    }
}
