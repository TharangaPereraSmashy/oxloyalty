<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuneralDirectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funeral_directors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('name');
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('address3')->nullable();
            $table->string('suburb')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->string('postcode')->nullable();
            $table->integer('parent_id')->references('id')->on('funeral_directors')->onDelete('cascade')->nullable();
            $table->boolean('is_parent')->default(0);
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('contact_person_name')->nullable();
            $table->boolean('is_on_hold')->default(0);
            $table->string('gl_liability_code')->nullable();
            $table->string('gl_cost_code')->nullable();
            $table->string('logo')->nullable();
            $table->float('loyalty_dollar_balance', 8, 2)->default(0)->nullable();
            $table->float('earned_dollars', 8, 2)->default(0)->nullable();
            $table->float('redeemed_dollars', 8, 2)->default(0)->nullable();
            $table->float('expired_dollars', 8, 2)->default(0)->nullable();
            $table->float('adjusted_dollars', 8, 2)->default(0)->nullable();
            $table->float('transfered_dollars', 8, 2)->default(0)->nullable();
            $table->float('reversed_dollars', 8, 2)->default(0)->nullable();
            $table->dateTime('last_activity_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funeral_directors');
    }
}
