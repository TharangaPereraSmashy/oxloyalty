<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCemeteryItemsFullView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( 'CREATE VIEW cemetery_items_full_view AS 
        SELECT i.id, i.item_id, c.id cemetery_id, i.status, i.to_earn, i.to_redeem, i.start_date, i.end_date, i.deleted_at, i.created_at, i.updated_at 
        FROM cemetery_items i, cemeteries c
        WHERE COALESCE(i.cemetery_id,c.id) = c.id' );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement( 'DROP VIEW cemetery_items_full_view' );
    }
}
