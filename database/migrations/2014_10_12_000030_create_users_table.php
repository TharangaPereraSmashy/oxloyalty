<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('status')->default(1);
            $table->string('phone')->nullable();
            $table->unsignedInteger('type');
            $table->unsignedInteger('profile');
            $table->integer('fd_id')->references('id')->on('funeral_directors')->onDelete('cascade')->nullable();
            $table->integer('default_cemetery')->references('id')->on('cemeteries')->onDelete('cascade')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('type')->references('id')->on('types');
            $table->foreign('profile')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
