<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevaluationRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revaluation_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('requested_date');
            $table->integer('requested_by');
            $table->decimal('new_rate', 10, 2);
            $table->boolean('is_processed')->default(0)->nullable();
            $table->dateTime('processed_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revaluation_requests');
    }
}
