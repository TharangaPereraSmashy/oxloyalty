<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuneralDirectorBalancesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( 'CREATE VIEW funeral_director_balances_view AS 
                        SELECT f.*,
                        ((SELECT coalesce(SUM(points_earned),0) FROM lp_earned_points e  WHERE e.fd_id = f.id)  - (SELECT coalesce(sum(points_consumed),0) FROM lp_consumed_points c WHERE c.fd_id = f.id)) available_points 
                        from funeral_directors f' );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement( 'DROP VIEW funeral_director_balances_view' );
    }
}
