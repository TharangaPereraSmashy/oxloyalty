<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveLpEarnedPointsColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lp_earned_points', function($table) {
            $table->dropColumn('is_expired_by_x');
            $table->dropColumn('is_expired_by_y');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lp_earned_points', function($table) {
            $table->boolean('is_expired_by_x')->default(0);
            $table->boolean('is_expired_by_y')->default(0);
        });
    }
}
