<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lp_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('record_id');
            $table->integer('gp_record_id')->nullable();
            $table->string('doc_type')->nullable();
            $table->string('doc_number')->nullable();
            $table->dateTime('doc_date')->nullable();
            $table->string('funeral_director', 15)->nullable();
            $table->string('cemetery')->nullable();
            $table->string('item_number')->nullable();
            $table->boolean('is_special_offer');
            $table->integer('offer_id')->nullable()->references('id')->on('special_offers');
            $table->integer('cemetery_item_id')->nullable()->references('id')->on('cemetery_items');
            $table->integer('to_earn')->nullable();
            $table->integer('to_redeem')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('rewards')->nullable();
            $table->integer('rewards_redeemed')->nullable();
            $table->dateTime('time_date_stamp')->nullable();    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lp_invoices');
    }
}
