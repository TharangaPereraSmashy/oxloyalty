<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpTransferDocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lp_transfer_doc', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('transfer_doc_date');
            $table->string('direction');
            $table->integer('request_id')->nullable()->references('id')->on('loyalty_dollar_requests');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lp_transfer_doc');
    }
}
