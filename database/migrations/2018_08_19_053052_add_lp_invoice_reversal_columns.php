<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLpInvoiceReversalColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lp_invoices', function(Blueprint $table) {
            $table->string('applied_to')->nullable();
            $table->string('applied_to_doc_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lp_invoices', function(Blueprint $table) {
            $table->dropColumn('applied_to');
            $table->dropColumn('applied_to_doc_type');
        });
    }
}
