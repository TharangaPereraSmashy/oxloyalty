<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpCreditNoteDocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lp_credit_note_doc', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('cn_doc_date');
            $table->string('cn_type');
            $table->integer('record_id');
            $table->integer('gp_record_id')->nullable();
            $table->string('doc_type')->nullable();
            $table->string('doc_number')->nullable();
            $table->dateTime('doc_date')->nullable();
            $table->string('funeral_director', 15)->nullable();
            $table->string('cemetery')->nullable();
            $table->string('item_number')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('rewards')->nullable();
            $table->integer('rewards_redeemed')->nullable();
            $table->string('applied_to')->nullable();
            $table->string('applied_to_doc_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lp_credit_note_doc');
    }
}
