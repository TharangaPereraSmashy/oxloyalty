<?php

use Illuminate\Database\Seeder;
use App\BackgroundJobParameter;

class BackgroundJobParametersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parameters = [
            [   
                'parameter'     =>  'host',
                'value'         =>  '127.0.0.0',
                'description'   =>  'Email Server'
            ],
            [   
                'parameter'     =>  'port',
                'value'         =>  '433',
                'description'   =>  'Port'
            ],
            [   
                'parameter'     =>  'sender',
                'value'         =>  'info@msmashyerp.com',
                'description'   =>  'Sender'
            ],
            [   
                'parameter'     =>  'password',
                'value'         =>  'password',
                'description'   =>  'Password'
            ],
            [   
                'parameter'     =>  'default_recipient',
                'value'         =>  'user1@opusxenta.com',
                'description'   =>  'Default Recipient'
            ],
            [   
                'parameter'     =>  'additional_recipients',
                'value'         =>  'user2@opusxenta.com;user3@opusxenta.com;user4@opusxenta.com',
                'description'   =>  'Additional Recipients'
            ]
        ];     

        BackgroundJobParameter::insert($parameters);
    }
}
