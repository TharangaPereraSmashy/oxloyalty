<?php

use Illuminate\Database\Seeder;
use App\SpecialOffer;

class SpecialOffersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('special_offers')->truncate();

        /* $special_offers = [
            ['code' => 'SPOFR0000001', 'description' => 'Special Offer 1', 'item_id' => '1', 'cemetery_id' => '1', 'status' => '1', 'fd_from_code' => '100', 'fd_to_code' => '105', 'to_earn' => '100', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['code' => 'SPOFR0000002', 'description' => 'Special Offer 2', 'item_id' => '2', 'cemetery_id' => '2', 'status' => '1', 'fd_from_code' => '100', 'fd_to_code' => '105', 'to_earn' => '100', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['code' => 'SPOFR0000003', 'description' => 'Special Offer 3', 'item_id' => '3', 'cemetery_id' => '3', 'status' => '1', 'fd_from_code' => '100', 'fd_to_code' => '105', 'to_earn' => '100', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['code' => 'SPOFR0000004', 'description' => 'Special Offer 4', 'item_id' => '4', 'cemetery_id' => '4', 'status' => '1', 'fd_from_code' => '100', 'fd_to_code' => '105', 'to_earn' => '100', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['code' => 'SPOFR0000005', 'description' => 'Special Offer 5', 'item_id' => '5', 'cemetery_id' => '5', 'status' => '1', 'fd_from_code' => '100', 'fd_to_code' => '105', 'to_earn' => '100', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['code' => 'SPOFR0000006', 'description' => 'Special Offer 6', 'item_id' => '6', 'cemetery_id' => '6', 'status' => '1', 'fd_from_code' => '100', 'fd_to_code' => '105', 'to_earn' => '100', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['code' => 'SPOFR0000007', 'description' => 'Special Offer 7', 'item_id' => '7', 'cemetery_id' => '7', 'status' => '1', 'fd_from_code' => '100', 'fd_to_code' => '105', 'to_earn' => '100', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['code' => 'SPOFR0000008', 'description' => 'Special Offer 8', 'item_id' => '8', 'cemetery_id' => '8', 'status' => '1', 'fd_from_code' => '100', 'fd_to_code' => '105', 'to_earn' => '100', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['code' => 'SPOFR0000009', 'description' => 'Special Offer 9', 'item_id' => '9', 'cemetery_id' => '9', 'status' => '1', 'fd_from_code' => '100', 'fd_to_code' => '105', 'to_earn' => '100', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['code' => 'SPOFR0000010', 'description' => 'Special Offer 10', 'item_id' => '10', 'cemetery_id' => '10', 'status' => '1', 'fd_from_code' => '100', 'fd_to_code' => '105', 'to_earn' => '100', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28']
        ];

        SpecialOffer::insert($special_offers); */
    }
}
