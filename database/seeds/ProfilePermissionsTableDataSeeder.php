<?php

use Illuminate\Database\Seeder;
use App\ProfilePermission;

class ProfilePermissionsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profile_permissions')->truncate();

        $profile_permissions = [
            [
                'pageId'    =>  '1', 
                'create'    =>  TRUE, 
                'read'      =>  TRUE, 
                'update'    =>  TRUE, 
                'delete'    =>  TRUE, 
                'profileId' =>  '1'
            ],
            [
                'pageId'    =>  '4', 
                'create'    =>  TRUE, 
                'read'      =>  TRUE, 
                'update'    =>  TRUE, 
                'delete'    =>  TRUE, 
                'profileId' =>  '1'
            ],
            [
                'pageId'    =>  '5', 
                'create'    =>  TRUE, 
                'read'      =>  TRUE, 
                'update'    =>  TRUE, 
                'delete'    =>  TRUE, 
                'profileId' =>  '1'
            ],
            [
                'pageId'    =>  '6', 
                'create'    =>  TRUE, 
                'read'      =>  TRUE, 
                'update'    =>  TRUE, 
                'delete'    =>  TRUE, 
                'profileId' =>  '1'
            ],
            [
                'pageId'    =>  '7', 
                'create'    =>  TRUE, 
                'read'      =>  TRUE, 
                'update'    =>  TRUE, 
                'delete'    =>  TRUE, 
                'profileId' =>  '1'
            ],
            [
                'pageId'    =>  '8', 
                'create'    =>  TRUE, 
                'read'      =>  TRUE, 
                'update'    =>  TRUE, 
                'delete'    =>  TRUE, 
                'profileId' =>  '1'
            ],
            [
                'pageId'    =>  '9', 
                'create'    =>  TRUE, 
                'read'      =>  TRUE, 
                'update'    =>  TRUE, 
                'delete'    =>  TRUE, 
                'profileId' =>  '1'
            ],
            [
                'pageId'    =>  '10', 
                'create'    =>  FALSE, 
                'read'      =>  TRUE, 
                'update'    =>  TRUE, 
                'delete'    =>  FALSE, 
                'profileId' =>  '1'
            ],
            [
                'pageId'    =>  '11', 
                'create'    =>  FALSE, 
                'read'      =>  TRUE, 
                'update'    =>  TRUE, 
                'delete'    =>  FALSE, 
                'profileId' =>  '1'
            ]
        ];

        ProfilePermission::insert($profile_permissions);
    }
}
