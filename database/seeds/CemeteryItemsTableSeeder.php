<?php

use Illuminate\Database\Seeder;
use App\CemeteryItem;

class CemeteryItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cemetery_items')->truncate();

        /* $cemetery_items = [
            ['item_id' => '1', 'cemetery_id' => '1', 'status' => '1', 'to_earn' => '10', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['item_id' => '2', 'cemetery_id' => '2', 'status' => '1', 'to_earn' => '10', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['item_id' => '3', 'cemetery_id' => '3', 'status' => '1', 'to_earn' => '10', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['item_id' => '4', 'cemetery_id' => '4', 'status' => '1', 'to_earn' => '10', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['item_id' => '5', 'cemetery_id' => '5', 'status' => '1', 'to_earn' => '10', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['item_id' => '6', 'cemetery_id' => '6', 'status' => '1', 'to_earn' => '10', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['item_id' => '7', 'cemetery_id' => '7', 'status' => '1', 'to_earn' => '10', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['item_id' => '8', 'cemetery_id' => '8', 'status' => '1', 'to_earn' => '10', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['item_id' => '9', 'cemetery_id' => '9', 'status' => '1', 'to_earn' => '10', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28'],
            ['item_id' => '10', 'cemetery_id' => '10', 'status' => '1', 'to_earn' => '10', 'to_redeem' => '20', 'start_date' => '2018-05-18', 'end_date' => '2018-05-28']
        ];

        CemeteryItem::insert($cemetery_items); */

        $cemetery_items = [
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '1',
                'status'        =>  '1',
                'to_earn'       =>  '10',
                'to_redeem'     =>  '12',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '2',
                'status'        =>  '1',
                'to_earn'       =>  '15',
                'to_redeem'     =>  '15',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '3',
                'status'        =>  '1',
                'to_earn'       =>  '20',
                'to_redeem'     =>  '18',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '4',
                'status'        =>  '1',
                'to_earn'       =>  '25',
                'to_redeem'     =>  '20',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '5',
                'status'        =>  '1',
                'to_earn'       =>  '10',
                'to_redeem'     =>  '12',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '6',
                'status'        =>  '1',
                'to_earn'       =>  '15',
                'to_redeem'     =>  '15',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '7',
                'status'        =>  '1',
                'to_earn'       =>  '20',
                'to_redeem'     =>  '16',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '8',
                'status'        =>  '1',
                'to_earn'       =>  '25',
                'to_redeem'     =>  '16',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '9',
                'status'        =>  '1',
                'to_earn'       =>  '23',
                'to_redeem'     =>  '17',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '10',
                'status'        =>  '1',
                'to_earn'       =>  '24',
                'to_redeem'     =>  '17',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '11',
                'status'        =>  '1',
                'to_earn'       =>  '25',
                'to_redeem'     =>  '17',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '12',
                'status'        =>  '1',
                'to_earn'       =>  '26',
                'to_redeem'     =>  '17',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '2',
                'item_id'       =>  '13',
                'status'        =>  '1',
                'to_earn'       =>  '28',
                'to_redeem'     =>  '18',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '14',
                'status'        =>  '1',
                'to_earn'       =>  '29',
                'to_redeem'     =>  '18',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '15',
                'status'        =>  '1',
                'to_earn'       =>  '30',
                'to_redeem'     =>  '18',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '16',
                'status'        =>  '1',
                'to_earn'       =>  '31',
                'to_redeem'     =>  '18',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '17',
                'status'        =>  '1',
                'to_earn'       =>  '32',
                'to_redeem'     =>  '18',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '18',
                'status'        =>  '1',
                'to_earn'       =>  '34',
                'to_redeem'     =>  '19',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '19',
                'status'        =>  '1',
                'to_earn'       =>  '35',
                'to_redeem'     =>  '19',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '20',
                'status'        =>  '1',
                'to_earn'       =>  '36',
                'to_redeem'     =>  '19',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '21',
                'status'        =>  '1',
                'to_earn'       =>  '37',
                'to_redeem'     =>  '19',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '22',
                'status'        =>  '1',
                'to_earn'       =>  '38',
                'to_redeem'     =>  '20',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '23',
                'status'        =>  '1',
                'to_earn'       =>  '40',
                'to_redeem'     =>  '20',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '24',
                'status'        =>  '1',
                'to_earn'       =>  '41',
                'to_redeem'     =>  '20',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '25',
                'status'        =>  '1',
                'to_earn'       =>  '42',
                'to_redeem'     =>  '20',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '26',
                'status'        =>  '1',
                'to_earn'       =>  '43',
                'to_redeem'     =>  '20',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '27',
                'status'        =>  '1',
                'to_earn'       =>  '44',
                'to_redeem'     =>  '21',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '28',
                'status'        =>  '1',
                'to_earn'       =>  '45',
                'to_redeem'     =>  '21',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '29',
                'status'        =>  '1',
                'to_earn'       =>  '47',
                'to_redeem'     =>  '21',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ],
            [
                'cemetery_id'   =>  '3',
                'item_id'       =>  '30',
                'status'        =>  '1',
                'to_earn'       =>  '48',
                'to_redeem'     =>  '21',
                'start_date'    =>  '2018-01-01',
                'end_date'      =>  '2018-12-31'
            ]
        ];

        CemeteryItem::insert($cemetery_items);
    }
}
