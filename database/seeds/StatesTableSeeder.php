<?php

use Illuminate\Database\Seeder;
use App\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->delete();

        /* $states = [
            ['state_code' => 'S0001', 'country_id' => '1', 'state_name' => 'State 1'],
            ['state_code' => 'S0002', 'country_id' => '1', 'state_name' => 'State 2'],
            ['state_code' => 'S0003', 'country_id' => '2', 'state_name' => 'State 3'],
            ['state_code' => 'S0004', 'country_id' => '2', 'state_name' => 'State 4'],
            ['state_code' => 'S0005', 'country_id' => '3', 'state_name' => 'State 5'],
            ['state_code' => 'S0006', 'country_id' => '3', 'state_name' => 'State 6'],
            ['state_code' => 'S0007', 'country_id' => '4', 'state_name' => 'State 7'],
            ['state_code' => 'S0008', 'country_id' => '4', 'state_name' => 'State 8'],
            ['state_code' => 'S0009', 'country_id' => '5', 'state_name' => 'State 9'],
            ['state_code' => 'S00010', 'country_id' => '5', 'state_name' => 'State 10'],
        ]; */

        $states = [
            [
                'state_code'    =>  'NSW', 
                'country_id'    =>  '1', 
                'state_name'    =>  'New South Wales'
            ],
            [
                'state_code'    =>  'QLD', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Queensland'
            ],
            [
                'state_code'    =>  'SA', 
                'country_id'    =>  '1', 
                'state_name'    =>  'South Australia'
            ],
            [
                'state_code'    =>  'TAS', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Tasmania'
            ],
            [
                'state_code'    =>  'VIC', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Victoria'
            ],
            [
                'state_code'    =>  'WA', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Western Australia'
            ],
            [
                'state_code'    =>  'ACT', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Australian Capital Territory'
            ],
            [
                'state_code'    =>  'JBT', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Jervis Bay Territory'
            ],
            [
                'state_code'    =>  'NT', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Northern Territory'
            ],
            [
                'state_code'    =>  'AAT', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Australian Antarctic Territory'
            ],
            [
                'state_code'    =>  'ACI-TEMP', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Ashmore and Cartier Islands'
            ],
            [
                'state_code'    =>  'CX', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Christmas Island'
            ],
            [
                'state_code'    =>  'CC', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Cocos (Keeling) Islands'
            ],
            [
                'state_code'    =>  'CSI-TEMP', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Coral Sea Islands'
            ],
            [
                'state_code'    =>  'HIMI', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Heard Island and McDonald Islands'
            ],
            [
                'state_code'    =>  'NF', 
                'country_id'    =>  '1', 
                'state_name'    =>  'Norfolk Island'
            ]
        ];

        State::insert($states);
    }
}
