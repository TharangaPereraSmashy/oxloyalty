<?php

use Illuminate\Database\Seeder;
use App\Item;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->truncate();

        /* $items = [
            ['code' => 'ITEM0000001', 'description' => 'Item 1', 'status' => '1', 'gl_liability_code' => 'item1_gllcode', 'gl_cost_code' => 'item1_glcc'],
            ['code' => 'ITEM0000002', 'description' => 'Item 2', 'status' => '1', 'gl_liability_code' => 'item2_gllcode', 'gl_cost_code' => 'item2_glcc'],
            ['code' => 'ITEM0000003', 'description' => 'Item 3', 'status' => '1', 'gl_liability_code' => 'item3_gllcode', 'gl_cost_code' => 'item3_glcc'],
            ['code' => 'ITEM0000004', 'description' => 'Item 4', 'status' => '1', 'gl_liability_code' => 'item4_gllcode', 'gl_cost_code' => 'item4_glcc'],
            ['code' => 'ITEM0000005', 'description' => 'Item 5', 'status' => '1', 'gl_liability_code' => 'item5_gllcode', 'gl_cost_code' => 'item5_glcc'],
            ['code' => 'ITEM0000006', 'description' => 'Item 6', 'status' => '1', 'gl_liability_code' => 'item6_gllcode', 'gl_cost_code' => 'item6_glcc'],
            ['code' => 'ITEM0000007', 'description' => 'Item 7', 'status' => '1', 'gl_liability_code' => 'item7_gllcode', 'gl_cost_code' => 'item7_glcc'],
            ['code' => 'ITEM0000008', 'description' => 'Item 8', 'status' => '1', 'gl_liability_code' => 'item8_gllcode', 'gl_cost_code' => 'item8_glcc'],
            ['code' => 'ITEM0000009', 'description' => 'Item 9', 'status' => '1', 'gl_liability_code' => 'item9_gllcode', 'gl_cost_code' => 'item9_glcc'],
            ['code' => 'ITEM0000010', 'description' => 'Item 10', 'status' => '1', 'gl_liability_code' => 'item10_gllcode', 'gl_cost_code' => 'item10_glcc']
        ];

        Item::insert($items); */

        $items = [
            [
                'code'              =>  '30008',
                'description'       =>  'Interment Fee Garden Crypts (1st Interment)',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30016',
                'description'       =>  'Interment Fee Grave - 1st Interment',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30035',
                'description'       =>  'Rob Lawn Grave - PREPURCHASE',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30038',
                'description'       =>  'Rob Vault',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30039',
                'description'       =>  'Sand',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30042',
                'description'       =>  'Interment Fee Tombs',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30045',
                'description'       =>  'Interment Fee Vaults (1st Interment)',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30088',
                'description'       =>  'Rob Section Grave - AT NEED',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30089',
                'description'       =>  'Rob Lawn Grave - AT NEED',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30098',
                'description'       =>  'Interment Fee Garden Crypts (Re-open)',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30103',
                'description'       =>  'ROB Package Crypt Level 1 - St Padre Pio',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30107',
                'description'       =>  'Chapel Fee St Padre Pio',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30108',
                'description'       =>  'Chapel Additional Time (per 30min)',
                'status'            =>  '1',
                'gl_cost_code'      =>  '40-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '10009',
                'description'       =>  'Interment Fee Garden Crypts (1st Interment)',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '10018',
                'description'       =>  'Inscription Fee Lawn Grave',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '10019',
                'description'       =>  'Interment Fee Grave - 1st Interment',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '10042',
                'description'       =>  'Rob Lawn Grave - PREPURCHASE',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '100655',
                'description'       =>  'ROB Crypt JP2 Lv4 (Granite)',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '100669',
                'description'       =>  'ROB Package Crypt Level 1 - San Antonio',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '100671',
                'description'       =>  'ROB Package Crypt Level 3 - San Antonio',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '100672',
                'description'       =>  'ROB Package Crypt Level 4 - San Antonio',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '10564',
                'description'       =>  'Chapel Fee Sacred Heart',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '10565',
                'description'       =>  'Sand',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '10572',
                'description'       =>  'Interment Fee Vaults - 1st Interment',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '10626',
                'description'       =>  'Interment Fee Cremated Remains-CCT',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '10627',
                'description'       =>  'Cremation Fee',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '10652',
                'description'       =>  'Rob Lawn Grave',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '20000',
                'description'       =>  'Admin Fee - Payment Plan',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '20048',
                'description'       =>  'Chapel Additional Time (per 30min)',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ],
            [
                'code'              =>  '30004',
                'description'       =>  'ROB Package Memorial Tree  - Way of the Cross',
                'status'            =>  '1',
                'gl_cost_code'      =>  '20-00-4500-00',
                'gl_liability_code' =>  '00-00-2580-00'
            ]
        ];

        Item::insert($items);
    }
}
