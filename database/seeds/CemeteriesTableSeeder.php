<?php

use Illuminate\Database\Seeder;
use App\Cemetery;

class CemeteriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cemeteries')->truncate();

        /* $cemeteries = [
            ['code' => 'CEM0000001', 'name' => 'Cemetery 1', 'address1' => 'cem1_address1', 'address2' => 'cem1_address2', 'address3' => 'cem1_address3', 'suburb' => 'cem1_suburb', 'country' => '1', 'state' => '1', 'postcode' => 'cem1_postcode', 'phone' => '3333333331', 'email' => 'cemetery1@gmail.com', 'external_ref_code' => 'cem1_ref_code'],
            ['code' => 'CEM0000002', 'name' => 'Cemetery 2', 'address1' => 'cem2_address1', 'address2' => 'cem2_address2', 'address3' => 'cem2_address3', 'suburb' => 'cem2_suburb', 'country' => '1', 'state' => '2', 'postcode' => 'cem2_postcode', 'phone' => '3333333332', 'email' => 'cemetery2@gmail.com', 'external_ref_code' => 'cem2_ref_code'],
            ['code' => 'CEM0000003', 'name' => 'Cemetery 3', 'address1' => 'cem3_address1', 'address2' => 'cem3_address2', 'address3' => 'cem3_address3', 'suburb' => 'cem3_suburb', 'country' => '1', 'state' => '3', 'postcode' => 'cem3_postcode', 'phone' => '3333333333', 'email' => 'cemetery3@gmail.com', 'external_ref_code' => 'cem3_ref_code'],
            ['code' => 'CEM0000004', 'name' => 'Cemetery 4', 'address1' => 'cem4_address1', 'address2' => 'cem4_address2', 'address3' => 'cem4_address3', 'suburb' => 'cem4_suburb', 'country' => '1', 'state' => '4', 'postcode' => 'cem4_postcode', 'phone' => '3333333334', 'email' => 'cemetery4@gmail.com', 'external_ref_code' => 'cem4_ref_code'],
            ['code' => 'CEM0000005', 'name' => 'Cemetery 5', 'address1' => 'cem5_address1', 'address2' => 'cem5_address2', 'address3' => 'cem5_address3', 'suburb' => 'cem5_suburb', 'country' => '1', 'state' => '5', 'postcode' => 'cem5_postcode', 'phone' => '3333333335', 'email' => 'cemetery5@gmail.com', 'external_ref_code' => 'cem5_ref_code'],
            ['code' => 'CEM0000006', 'name' => 'Cemetery 6', 'address1' => 'cem6_address1', 'address2' => 'cem6_address2', 'address3' => 'cem6_address3', 'suburb' => 'cem6_suburb', 'country' => '1', 'state' => '6', 'postcode' => 'cem6_postcode', 'phone' => '3333333336', 'email' => 'cemetery6@gmail.com', 'external_ref_code' => 'cem6_ref_code'],
            ['code' => 'CEM0000007', 'name' => 'Cemetery 7', 'address1' => 'cem7_address1', 'address2' => 'cem7_address2', 'address3' => 'cem7_address3', 'suburb' => 'cem7_suburb', 'country' => '1', 'state' => '7', 'postcode' => 'cem7_postcode', 'phone' => '3333333337', 'email' => 'cemetery7@gmail.com', 'external_ref_code' => 'cem7_ref_code'],
            ['code' => 'CEM0000008', 'name' => 'Cemetery 8', 'address1' => 'cem8_address1', 'address2' => 'cem8_address2', 'address3' => 'cem8_address3', 'suburb' => 'cem8_suburb', 'country' => '1', 'state' => '8', 'postcode' => 'cem8_postcode', 'phone' => '3333333338', 'email' => 'cemetery8@gmail.com', 'external_ref_code' => 'cem8_ref_code'],
            ['code' => 'CEM0000009', 'name' => 'Cemetery 9', 'address1' => 'cem9_address1', 'address2' => 'cem9_address2', 'address3' => 'cem9_address3', 'suburb' => 'cem9_suburb', 'country' => '1', 'state' => '9', 'postcode' => 'cem9_postcode', 'phone' => '3333333339', 'email' => 'cemetery9@gmail.com', 'external_ref_code' => 'cem9_ref_code'],
            ['code' => 'CEM0000010', 'name' => 'Cemetery 10', 'address1' => 'cem10_address1', 'address2' => 'cem10_address2', 'address3' => 'cem10_address3', 'suburb' => 'cem10_suburb', 'country' => '1', 'state' => '10', 'postcode' => 'cem10_postcode', 'phone' => '3333333330', 'email' => 'cemetery10@gmail.com', 'external_ref_code' => 'cem10_ref_code']
        ];

        Cemetery::insert($cemeteries); */
            
        $cemeteries = [
            [
                'code'          =>  'KCK',
                'name'          =>  'Kemps Creek',
                'address1'      =>  '230 Western Road',
                'suburb'        =>  'Kemps Creek',
                'country_id'    =>  '1',
                'state_id'      =>  '1'
            ],
            [
                'code'          =>  'LIV',
                'name'          =>  'Liverpool',
                'address1'      =>  '207 Moore Street',
                'suburb'        =>  'Liverpool',
                'country_id'    =>  '1',
                'state_id'      =>  '1'
            ],
            [
                'code'          =>  'ROK',
                'name'          =>  'Rookwood',
                'address1'      =>  'Barnet Avenue',
                'suburb'        =>  'Rookwood',
                'country_id'    =>  '1',
                'state_id'      =>  '1'
            ]
        ];     

        Cemetery::insert($cemeteries);
        
    }
}
