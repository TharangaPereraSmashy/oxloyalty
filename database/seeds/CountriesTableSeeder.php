<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->delete();

        /* $countries = [
            ['country_code' => 'C0001', 'country_name' => 'Country 1'],
            ['country_code' => 'C0002', 'country_name' => 'Country 2'],
            ['country_code' => 'C0003', 'country_name' => 'Country 3'],
            ['country_code' => 'C0004', 'country_name' => 'Country 4'],
            ['country_code' => 'C0005', 'country_name' => 'Country 5']
        ]; */

        $countries = [
            [
                'country_code'  =>  'AU', 
                'country_name'  =>  'Australia'
            ]
        ];

        Country::insert($countries);
    }
}
