<?php

use Illuminate\Database\Seeder;
use App\RevaluationRequest;

class RevaluationRequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $requests = [
            [   
                'requested_date'    =>  date('Y-m-d H:i:s'),
                'requested_by'      =>  1,
                'new_rate'          =>  1,
                'is_processed'      =>  1,
                'processed_date'    =>  date('Y-m-d H:i:s')
            ]
        ];     

        RevaluationRequest::insert($requests);
    }
}
