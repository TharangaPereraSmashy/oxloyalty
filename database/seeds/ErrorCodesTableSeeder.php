<?php

use Illuminate\Database\Seeder;

class ErrorCodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('error_codes')->delete();

        $error_codes = [
            [
                'error_no'      =>  '100', 
                'error_code'    =>  'procinvoice00', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Invoice Number Cannot be Null',
                'error_desc'    =>  '(lp_Invoice.DocNumber IS NULL)'
            ],
            [
                'error_no'      =>  '101', 
                'error_code'    =>  'procinvoice01', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'DocNumber:Invoice Type is Invalid',
                'error_desc'    =>  '(lp_invoice.DocumentType) NOT IN ("INVOICE","CREDIT")'
            ],
            [
                'error_no'      =>  '102', 
                'error_code'    =>  'procinvoice02', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'DocNumber:Invoice Line Already Exists',
                'error_desc'    =>  '(lp_invoice.RecordID EXISTS IN transaction_history.RecordID)'
            ],
            [
                'error_no'      =>  '103', 
                'error_code'    =>  'procinvoice03', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Customer Cannot be Null',
                'error_desc'    =>  '(lp_invoice.FD Account IS NULL)'
            ],
            [
                'error_no'      =>  '104', 
                'error_code'    =>  'procinvoice04', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Invoice date  is not valid to process.',
                'error_desc'    =>  'lp_invoice.DocDate IS NULL) OR (invoice.DocDate <system start date'
            ],
            [
                'error_no'      =>  '105', 
                'error_code'    =>  'procinvoice05', 
                'error_type'    =>  'Info', 
                'error_reason'  =>  'Invoice Customer does not exists',
                'error_desc'    =>  'lp_invoice.FD ACCOUNT) NOT EXISTS IN (FD_ACCOUNT.CODE'
            ],
            [
                'error_no'      =>  '106', 
                'error_code'    =>  'procinvoice06', 
                'error_type'    =>  'Info', 
                'error_reason'  =>  'Invoice Customer is Inactive.Cannot process',
                'error_desc'    =>  'FD Account.Inactive =Y)'
            ],
            [
                'error_no'      =>  '107', 
                'error_code'    =>  'procinvoice07', 
                'error_type'    =>  'Info', 
                'error_reason'  =>  'Invoice Customer is a Parent Customer. No Rewards or Redemptions Allowed.',
                'error_desc'    =>  'lp_invoice.FD Account.IsParent =Y'
            ],
            [
                'error_no'      =>  '108', 
                'error_code'    =>  'procinvoice08', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Invoice qty has no value',
                'error_desc'    =>  'lp_Invoice.qty) IS NULL OR (lp_invoice.qty =0)'
            ],
            [
                'error_no'      =>  '109', 
                'error_code'    =>  'procinvoice09', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Invoice Cemetery Code cannot be null',
                'error_desc'    =>  'lp_invoice.CemeteryCode) IS NULL'
            ],
            [
                'error_no'      =>  '110', 
                'error_code'    =>  'procinvoice10', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Cemetery Code does not exists',
                'error_desc'    =>  'lp_invoice.CemeteryCode) NOT EXISTS IN(item_cemeteries_LP$'
            ],
            [
                'error_no'      =>  '111', 
                'error_code'    =>  'procinvoice11', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Invoice ItemCode Cannot be null',
                'error_desc'    =>  'lp_invoice.ItemCode) IS NULL'
            ],
            [
                'error_no'      =>  '112', 
                'error_code'    =>  'procinvoice12', 
                'error_type'    =>  'Warning', 
                'error_reason'  =>  'Item Code Does not exists',
                'error_desc'    =>  '(lp_invoice.ItemCode) NOT EXISTS IN (item_Cemeteries_LP$)'
            ],
            [
                'error_no'      =>  '113', 
                'error_code'    =>  'procinvoice13', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Invoice Line/s contained ITEMS(Products) with expired status',
                'error_desc'    =>  'Using (ITEM,FD Account,Cemetery) check "Apply Special Offers-Items to FD Account and Cemetery" table for "ACTIVE STATUS"  >>> IF STATUS="N" OR Using (ITEM,CEMETERY) Check (for Item-Cemetery LP$ Table) "ACTIVE STATUS" >>> IF STATUS ="N" OR Using (ITEM.STATUS) = N THEN'
            ],
            [
                'error_no'      =>  '114', 
                'error_code'    =>  'procinvoice14', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Document line item not registered for reward redemption',
                'error_desc'    =>  '(lp_invoice.Rewards =0) AND (invoice.RewardsRedeemed=1)'
            ],
            [
                'error_no'      =>  '115', 
                'error_code'    =>  'procinvoice15', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Loyalty Dollar not defined for the item for the period',
                'error_desc'    =>  ''
            ],
            [
                'error_no'      =>  '116', 
                'error_code'    =>  'procinvoice16', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Invoice item is already processed',
                'error_desc'    =>  ''
            ],
            [
                'error_no'      =>  '117', 
                'error_code'    =>  'procinvoice17', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'No reward record to process the redeem',
                'error_desc'    =>  ''
            ],
            [
                'error_no'      =>  '118', 
                'error_code'    =>  'procinvoice18', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Redeem amount exceeds the total rewards',
                'error_desc'    =>  ''
            ],
            [
                'error_no'      =>  '119', 
                'error_code'    =>  'procinvoice19', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Customer is on Hold. Invoice Line xx  Cannot be redeemed.',
                'error_desc'    =>  'Check IF FD Account.IsOnhold =Y THEN)'
            ],
            [
                'error_no'      =>  '120', 
                'error_code'    =>  'procinvoice20', 
                'error_type'    =>  'Critical', 
                'error_reason'  =>  'Invoice Cannot redeem. No Account activity for more than 180 days can be found',
                'error_desc'    =>  '(FD Account table.Last_invoice_date)> 180 Days (global value : Maximum_Inactivity_Allowed.days)'
            ]
        ];

        DB::table('error_codes')->insert($error_codes);
    }
}
