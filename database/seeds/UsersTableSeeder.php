<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* DB::table('users')->insert([
        [   'name' => 'Admin User',
            'email' => 'user1@smashyerp.com',
            'password' => bcrypt('oxl2018528'),
            'type' => 1,
            'profile' => 1,
            'fd_id' => null
        ],
        [   'name' => 'admin',
            'email' => 'abcd@gmail.com',
            'password' => bcrypt('1234567'),
            'type' => 1,
            'profile' => 1,
            'fd_id' => null
        ],
        [
            'name' => 'Funeral Director 1',
            'email' => 'fd1@gmail.com',
            'password' => bcrypt('1234567'),
            'type' => 3,
            'profile' => 3,
            'fd_id' => 1
        ]]); */

        $users = [
            [   
                'name'              =>  'Admin User',
                'email'             =>  'user1@smashyerp.com',
                'password'          =>  bcrypt('oxl1234'),
                'type'              =>  1,
                'profile'           =>  1,
                'fd_id'             =>  null,
                'default_cemetery'  =>  null
            ],
            [   
                'name'              =>  'Admin',
                'email'             =>  'abcd@gmail.com',
                'password'          =>  bcrypt('1234567'),
                'type'              =>  1,
                'profile'           =>  1,
                'fd_id'             =>  null,
                'default_cemetery'  =>  null
            ],
            [
                'name'              =>  'FD User',
                'email'             =>  'fd1@smashyerp.com',
                'password'          =>  bcrypt('fd12345'),
                'type'              =>  3,
                'profile'           =>  3,
                'fd_id'             =>  131,
                'default_cemetery'  =>  2
            ],
            [
                'name'              =>  'Staff User',
                'email'             =>  'st1@smashyerp.com',
                'password'          =>  bcrypt('st12345'),
                'type'              =>  2,
                'profile'           =>  2,
                'fd_id'             =>  131,
                'default_cemetery'  =>  2
            ]
        ];     

        User::insert($users);

    }
}
