<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TypesTableSeeder::class,
            UserProfileTableSeeder::class,
            UsersTableSeeder::class,
            CemeteriesTableSeeder::class,
            CountriesTableSeeder::class,
            StatesTableSeeder::class,
            PagesTableSeeder::class,
            ItemsTableSeeder::class,
            //SpecialOffersTableDataSeeder::class,
            CemeteryItemsTableSeeder::class,
            ProfilePermissionsTableDataSeeder::class,
            FuneralDirectorsTableSeeder::class,
            SystemParametersTableSeeder::class,
            ErrorCodesTableSeeder::class,
            RevaluationRequestTableSeeder::class,
            BackgroundJobParametersTableSeeder::class
        ]);
    }

}

