<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            [
                'code'          =>  'register',
                'description'   =>  'Register',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ],
            [
                'code'          =>  'profile',
                'description'   =>  'Profile',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ],
            [
                'code'          =>  'permission',
                'description'   =>  'Permission',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ],
            [
                'code'          =>  'cemetery',
                'description'   =>  'Cemetery',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ],
            [
                'code'          =>  'item',
                'description'   =>  'Item',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ],
            /* [
                'code'          =>  'special_offer',
                'description'   =>  'Special Offer',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ],
            [
                'code'          =>  'cemetery_item',
                'description'   =>  'Item Loyalty',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ], */
            [
                'code'          =>  'funeral_director',
                'description'   =>  'Funeral Director',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ],
            [
                'code'          =>  'ld_transfer_request',
                'description'   =>  'Loyalty Dollar Transfer Request',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ],
            [
                'code'          =>  'audit',
                'description'   =>  'Audit',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ],
            [
                'code'          =>  'ld_revaluation_request',
                'description'   =>  'Loyalty Dollar Revaluation Request',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ],
            [
                'code'          =>  'configuration',
                'description'   =>  'Configuration',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ],
            [
                'code'          =>  'background_process',
                'description'   =>  'Background Process',
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ]
        ]);
    }
}
