<?php

use Illuminate\Database\Seeder;
use App\SystemParameter;

class SystemParametersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parameters = [
            [   
                'parameter' =>  'validity_period_days',
                'value'     =>  '90'

            ],
            [   
                'parameter' =>  'maximum_inactivity_days',
                'value'     =>  '3'
            ]
        ];     

        SystemParameter::insert($parameters);

    }
}
