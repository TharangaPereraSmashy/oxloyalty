<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Cemetery extends Model implements AuditableContract
{
    //use SoftDeletes;
    use Auditable;

    protected $table = 'cemeteries';

    protected $fillable = [
        'code',
        'name',
        'address1',
        'address2',
        'address3',
        'suburb',
        'postcode',
        'country_id',
        'state_id',
        'phone',
        'email',
        'is_default_cemetery',
        'external_ref_code'
    ];

    protected $dates = ['deleted_at'];

    public function users()
    {
        return $this->hasMany('App\User', 'default_cemetery');
    }
    
    public function cemeteryItems()
    {
        return $this->hasMany('App\CemeteryItem');
    }

    public function specialOffers()
    {
        return $this->hasMany('App\SpecialOffer');
    }
}
