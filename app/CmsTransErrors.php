<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsTransErrors extends Model
{
    protected $table = 'cms_trans_errors';

    protected $fillable = [
        'is_ignored'
    ];
}
