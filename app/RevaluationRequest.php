<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevaluationRequest extends Model
{
    protected $table = 'revaluation_requests';

    protected $fillable = [
        'requested_date',
        'requested_by',
        'new_rate',
        'is_processed',
        'processed_date'
    ];
}
