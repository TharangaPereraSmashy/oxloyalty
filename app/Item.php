<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Item extends Model implements AuditableContract
{
    //use SoftDeletes;
    use Auditable;

    protected $table = 'items';

    protected $fillable = [
        'code',
        'description',
        'status',
        'gl_liability_code',
        'gl_cost_code'
    ];

    protected $dates = ['deleted_at'];

    public function cemeteryItems()
    {
        return $this->hasMany('App\CemeteryItem');
    }

    public function specialOffers()
    {
        return $this->hasMany('App\SpecialOffer');
    }
    
    /* public static function boot()
    {
        parent::boot();    
    
        static::deleted(function($item)
        {
            $item->cemeteryItems()->delete();
            $item->specialOffers()->delete();
        });
    }    */ 
}
