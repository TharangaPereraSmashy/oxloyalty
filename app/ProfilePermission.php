<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProfilePermission extends Model
{
    public function getAllPermissionsByProfile($profileId) {

        $users = DB::table('profile_permissions')
            ->join('pages', 'pages.id', '=', 'profile_permissions.pageId')
            ->join('user_profiles', 'user_profiles.id', '=', 'profile_permissions.profileId')
            ->where('profile_permissions.profileId', $profileId)
            ->select('profile_permissions.*', 'pages.description as page_description', 'user_profiles.description as profile_description')
            ->get();
        return $users;
    }
}
