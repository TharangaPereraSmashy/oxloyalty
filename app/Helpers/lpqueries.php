<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

Class LpQueries{

    public function getEarnedDollars($id){

        $result =   DB::table('lp_earned_points')->select(DB::raw("coalesce(sum(points_earned),0) as earned"))->where('fd_id', '=', $id)->get();
        
        return $result;

    }

    public function getAvailableDollars($id){

        $balance    =   DB::table('lp_earned_points')->select(DB::raw("coalesce(sum(points_balance),0) as balance"))->where('fd_id', '=', $id)->get();

        $result     =   (int)$balance[0]->balance;
       
        //$result =   DB::table('lp_earned_points')->select(DB::raw("coalesce(sum(points_balance),0) as available"))
        //->where('fd_id', '=', $id)
        //->where('is_expired', '=', 'FALSE')->get();
       
        return $result;

    }

    public function getConsumedDollars($id)
    {
        $result =   DB::table('lp_consumed_points')->select(DB::raw("coalesce(sum(points_consumed),0) as consumed "))->where('fd_id', '=', $id)->get();
        
        return $result;
    }

    public function getToBeExpiredDollars($id){

        $current_date   =   Carbon::today()->toDateString();
        $expire_date    =   Carbon::today()->addDays(30)->toDateString();

        $result =   DB::table('lp_earned_points')->select(DB::raw("coalesce(sum(points_balance),0) as expired"))
        ->where('fd_id', '=', $id)
        ->where('is_expired', '=', 'FALSE')
        ->whereDate('points_end_date', '>', $current_date)
        ->whereDate('points_end_date', '<=', $expire_date)
        ->get();
        
        return $result;

    }

    public function getParentEarnedDollars($id){
        //$result =   DB::select("SELECT coalesce(SUM(points_balance),0) as earned 
        //                        FROM lp_earned_points
        //                        WHERE fd_id IN (SELECT id FROM funeral_directors WHERE parent_id = $id)");
                                
        $result =   DB::table('lp_earned_points')
                    ->select(DB::raw("coalesce(sum(points_earned),0) as earned"))
                    ->whereIn('fd_id',function($query) use ($id){
                        $query->select('id')->from('funeral_directors')->where('parent_id','=',$id);
                    })->get();
                    
        return $result;

    }

    public function getParentConsumedDollars($id)
    {
        $result =   DB::table('lp_consumed_points')
                    ->select(DB::raw("coalesce(sum(points_consumed),0) as consumed"))
                    ->whereIn('fd_id',function($query) use ($id){
                        $query->select('id')->from('funeral_directors')->where('parent_id','=',$id);
                    })->get();
                    
        return $result;
    }

    public function getParentAvailableDollars($id){
        
        $balance    =   DB::table('lp_earned_points')
                        ->select(DB::raw("coalesce(sum(points_balance),0) as balance"))
                        ->whereIn('fd_id',function($query) use ($id){
                            $query->select('id')->from('funeral_directors')->where('parent_id','=',$id);
                        })->get();

        $result     =   (int)$balance[0]->balance;

        //$result =   DB::table('lp_earned_points')
        //            ->select(DB::raw("coalesce(sum(points_balance),0) as available"))
        //            ->where('is_expired', '=', 'FALSE')
        //            ->whereIn('fd_id',function($query) use ($id){
        //                 $query->select('id')->from('funeral_directors')->where('parent_id','=',$id);
        //            })->get();

        return $result;

    }

    public function getParentToBeExpiredDollars($id){

        $current_date   =   Carbon::today()->toDateString();
        $expire_date    =   Carbon::today()->addDays(30)->toDateString();

        $result =   DB::table('lp_earned_points')
                    ->select(DB::raw("coalesce(sum(points_balance),0) as expired"))
                    ->where('is_expired', '=', 'FALSE')
                    ->whereDate('points_end_date', '>', $current_date)
                    ->whereDate('points_end_date', '<=', $expire_date)
                    ->whereIn('fd_id',function($query) use ($id){
                        $query->select('id')->from('funeral_directors')->where('parent_id','=',$id);
                    })->get();

        return $result;

    }

    public function getRewardRecordsForConsumption($fd_id, $points){

        $result =   DB::select("SELECT * FROM 
                    (SELECT t.*, (SUM(points_balance) over(order by t.id)- points_balance) total_points 
                    FROM lp_earned_points t WHERE points_balance > 0 AND  is_expired = FALSE AND fd_id = $fd_id) 
                    AS FDPOINTS WHERE total_points <= $points");

        return $result;

    }




    

    
}