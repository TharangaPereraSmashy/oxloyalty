<?php

namespace App\Helpers;

use App\Configuration;
use App\FuneralDirector;
use App\LoyaltyDollarRequest;
use App\Helpers\LpQueries;
use App\Cemetery;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

Class ProcessUtil{

    public function processAdjustment($fd_id, $points){

        DB::beginTransaction();

        try{

            $adjustment_date = date("Y-m-d h:i:s");
            
            $default_cemetery = Cemetery::select("id","code")->where("is_default_cemetery", TRUE)->first();

            $adjustment_id = DB::table('lp_adjustments')->insertGetId(
                [
                    'fd_id'         =>  $fd_id,
                    'points'        =>  $points,
                    'adjusted_date' =>  $adjustment_date
                ]
            );
            
            if($points>0){

                $this->addPoints("ADJUSTMENT+", $adjustment_id, $adjustment_date, $fd_id, $points);
                $this->createTxHistory("ADJUSTMENT+",$adjustment_id, $adjustment_date, $fd_id, $points);
 
            }else{
               
                $this->deductPoints('ADJUSTMENT-', $adjustment_id, $adjustment_date, $fd_id, abs($points));
                $this->createTxHistory("ADJUSTMENT-",$adjustment_id, $adjustment_date, $fd_id, $points);

            }
                           
            $this->createGlJournalAdjustment("DR", $fd_id, $points, $adjustment_id, $adjustment_date, $default_cemetery->id, $default_cemetery->code); // Debit
           
            $this->createGlJournalAdjustment("CR", $fd_id, $points, $adjustment_id, $adjustment_date, $default_cemetery->id, $default_cemetery->code); // Credit
    
            
            DB::commit();

        }catch (\Exception $e){

            

            DB::rollback();
            dd($e->getMessage());

        }

    }

    private function addPoints($source, $source_id, $source_date, $fd_id, $points){

        $validity_period_days = Configuration::select('parameter','value')->where('parameter','validity_period_days')->get();
        
        DB::table('lp_earned_points')->insert(
            [
                'fd_id'             =>  $fd_id,
                'earned_source'     =>  $source,
                'source_ref'        =>  $source_id,
                'points_earned'     =>  $points,
                'points_start_date' =>  $source_date,
                'points_end_date'   =>  date('Y-m-d', strtotime($source_date. ' + '.$validity_period_days[0]->value.' days')),
                'points_balance'    =>  $points,
                'created_at'        =>  $source_date
            ]
        );

    }

    private function deductPoints($source, $source_id, $source_date, $fd_id, $points){

       try{
        $lpqueries     =   new LpQueries();
        
        $records        =   $lpqueries->getRewardRecordsForConsumption($fd_id, abs($points));
        
        if (empty($records)) {

            $error = \Illuminate\Validation\ValidationException::withMessages ([
                'Availble Balance' => ['Insufficient Balance for a negative adjsutment']
            ]);

             throw $error;
 
        }

        $running_total  =   0;
        foreach($records as $record){
            $running_total      =   $running_total + $record->points_balance;
            $earned_row_id      =   $record->id;
            $earned_row_original_balance =  $record->points_balance;
                         
            if($points >= $running_total){

                $this->fullyConsumeRow($source, $earned_row_id, $earned_row_original_balance,  $fd_id,  $source_id,$source_date); 
            }else{
               
                $earned_row_new_balance = $running_total  - $points;
                $amount_to_counsume = $earned_row_original_balance - $earned_row_new_balance;
                $this->partiallyConsumeRow($source, $earned_row_id,  $amount_to_counsume, $earned_row_new_balance, $fd_id,  $source_id,$source_date);           
            }

        }

        }catch (\Exception $e){
            throw $e;
        }
       

    }

    private function createTxHistory($source, $source_id, $source_date, $fd_id, $points){

        DB::table('transactions_history')->insert(
            [
                'fd_id'         =>  $fd_id,
                'trans_date'    =>  $source_date,
                'trans_type'    =>  $source,
                'trans_ref'     =>  $source_id,
                'points'        =>  $points,
                'created_at'    =>  $source_date
            ]
        );

    }

    private function createGlJournalAdjustment($direction, $fd_id, $points, $adjustment_id, $adjustment_date, $default_cemetery_id, $default_cemetery_code){

        $fd_data        =   FuneralDirector::select('code','gl_liability_code','gl_cost_code')->where("id","=",$fd_id)->get();
        
        $account        =   "";
        $doctype        =   "";
        $debit_amount   =   0;
        $credit_amount  =   0;
        $taxable        =   0;

        if($points >= 0)
        {
            $doctype = "INCRADJ";

            if($direction == "DR"){
                $taxable        =   1;
                $debit_amount   =   abs($points);
                $credit_amount  =   0;
                $account        =   $fd_data[0]->gl_cost_code;

            }elseif($direction == "CR"){
                $taxable        =   1;
                $debit_amount   =   0;
                $credit_amount  =   abs($points);
                $account        =   $fd_data[0]->gl_liability_code;
            }  
        } 
        
        if($points < 0)
        {
            $doctype = "DECRADJ";
            if($direction == "DR"){
                $taxable        =   1; 
                $debit_amount   =   abs($points);
                $credit_amount  =   0;
                $account        =   $fd_data[0]->gl_liability_code;

            }elseif($direction == "CR"){
                $taxable        =   1; 
                $debit_amount   =   0;
                $credit_amount  =   abs($points);
                $account        =    $fd_data[0]->gl_cost_code;
            }  
        }  

        DB::table('gl_journal')->insert(
            [
                'cemetery_id'   =>  $default_cemetery_id,
                'cemetery_code' =>  $default_cemetery_code,
                'doc_type'      =>  $doctype,
                'doc_number'    =>  $adjustment_id,
                'doc_date'      =>  $adjustment_date,
                'fd_id'         =>  $fd_id,
                'fd_code'       =>  $fd_data[0]->code,
                'gl_account'    =>  $account,
                'dr_amount'     =>  $debit_amount,
                'cr_amount'     =>  $credit_amount,
                'taxable'       =>  $taxable,
                'created_at'    => date('Y-m-d h:i:sa')
            ]
        );
        
    }

    private function createGlJournalTransfer($direction, $fd_id, $points, $transfer_doc_id, $transfer_date, $default_cemetery_id, $default_cemetery_code){

        $fd_data        =   FuneralDirector::select('code','gl_liability_code')->where("id","=",$fd_id)->get();
        
        $account        =   "";
        $debit_amount   =   0;
        $credit_amount  =   0;

       if($points >= 0){

            if($direction == "DR"){
                $debit_amount   =   $points;
                $credit_amount  =   0;
                $account        =   $fd_data[0]->gl_liability_code;

            }elseif($direction == "CR"){
                $debit_amount   =   0;
                $credit_amount  =   $points;
                $account        =   $fd_data[0]->gl_liability_code;

            }  

            DB::table('gl_journal')->insert(
                [
                    'cemetery_id'   =>  $default_cemetery_id,
                    'cemetery_code' =>  $default_cemetery_code,
                    'doc_type'      =>  'TRANSFER',
                    'doc_number'    =>  $transfer_doc_id,
                    'doc_date'      =>  $transfer_date,
                    'fd_id'         =>  $fd_id,
                    'fd_code'       =>  $fd_data[0]->code,
                    'gl_account'    =>  $account,
                    'dr_amount'     =>  $debit_amount,
                    'cr_amount'     =>  $credit_amount,
                    'created_at'    =>  date('Y-m-d h:i:sa')
                ]
            );
            
        }  
        
    }


    public function processTransfer($transfer_id){
        
        DB::beginTransaction();

        try{

            $ld_request         =   LoyaltyDollarRequest::find($transfer_id);
            $from_fd_id         =   $ld_request->from_fd_id;
            $to_fd_id           =   $ld_request->to_fd_id;
            $points             =   $ld_request->transfer_amount;
            $transfer_date      =   date("Y-m-d h:i:s");
            $default_cemetery   = Cemetery::select("id","code")->where("is_default_cemetery", TRUE)->first();


            $transfer_doc_id_debit    =   $this->createLpTransferDoc($transfer_id, $transfer_date, "DR");
        
     
            $this->deductPoints("TRANSFER", $transfer_doc_id_debit, $transfer_date, $from_fd_id, $points);

            $history_points = -1 * abs($points);
            
            $this->createTxHistory("TRANSFER-", $transfer_doc_id_debit, $transfer_date, $from_fd_id, $history_points);
            
            $this->createGlJournalTransfer("DR", $from_fd_id, $points, $transfer_doc_id_debit, $transfer_date, $default_cemetery->id, $default_cemetery->code);
            
            //////////////////////////////////////////////////////////////////////////////////////////////

            $transfer_doc_id_credit    =   $this->createLpTransferDoc($transfer_id, $transfer_date, "CR");

            $this->addPoints("TRANSFER", $transfer_doc_id_credit, $transfer_date, $to_fd_id, $points);

            $this->createTxHistory("TRANSFER+", $transfer_doc_id_credit, $transfer_date, $to_fd_id, $points);

            $this->createGlJournalTransfer("CR", $to_fd_id, $points, $transfer_doc_id_debit, $transfer_date, $default_cemetery->id, $default_cemetery->code);
            
            $this->UpdateRequest($transfer_id);
            
            DB::commit();

        }catch (\Exception $e){

            

            DB::rollback();
            dd($e->getMessage());

        }

    }

    private function fullyConsumeRow($source, $earned_row_id,  $ponts_consumed, $fd_id, $doc_id, $doc_date){
       
           
        

        DB::table('lp_earned_points')->where('id', $earned_row_id)->update(['points_balance' => 0]);

        DB::table('lp_consumed_points')->insert(
            [
                'fd_id'             =>  $fd_id,
                'consumed_source'   =>  $source,
                'source_ref'        =>  $doc_id,
                'earned_points_ref' =>  $earned_row_id,
                'points_consumed'   =>  $ponts_consumed,
                'created_at'        =>  $doc_date
            ]
        );

    }

    private function partiallyConsumeRow($source, $earned_row_id,  $ponts_consumed, $earned_row_new_balance,  $fd_id, $doc_id, $doc_date){
        
       

        DB::table('lp_earned_points')->where('id', $earned_row_id)->update(['points_balance' => $earned_row_new_balance ]);

        DB::table('lp_consumed_points')->insert(
            [
                'fd_id'             =>  $fd_id,
                'consumed_source'   =>  $source,
                'source_ref'        =>  $doc_id,
                'earned_points_ref' =>  $earned_row_id,
                'points_consumed'   =>  $ponts_consumed,
                'created_at'        =>  $doc_date
            ]
        );

    }

    private function createLpTransferDoc($transfer_id, $transfer_date, $direction){

        $trans_id = DB::table('lp_transfer_doc')->insertGetId(
            [
                'transfer_doc_date' =>  $transfer_date,
                'direction'         =>  $direction,
                'request_id'        =>  $transfer_id,
                'created_at'        =>  $transfer_date
            ]
        );
        return $trans_id;

    }

    private function UpdateRequest($transfer_id)
    {
        $ld_request                 =   LoyaltyDollarRequest::find($transfer_id);
        $ld_request->status         =   "approved";
        $ld_request->processed_by   =   Auth::user()->id;
        $ld_request->processed_date =   date("Y-m-d H:i:s");
        $ld_request->save();
    }
    
}