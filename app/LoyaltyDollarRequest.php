<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class LoyaltyDollarRequest extends Model implements AuditableContract
{
    use Auditable;
    
    protected $table = 'loyalty_dollar_requests';

    protected $fillable = [
        'user_id',
        'from_fd_id',
        'to_fd_id',
        'transfer_amount',
        'status',
        'requested_date',
        'processed_by',
        'processed_date'
    ];
}
