<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class SpecialOffer extends Model implements AuditableContract
{
    //use SoftDeletes;
    use Auditable;

    protected $table = 'special_offers';

    protected $fillable = [
        'code',
        'description',
        'item_id',
        'cemetery_id',
        'status',
        'fd_from_code',
        'fd_to_code',
        'to_earn',
        'to_redeem',
        'start_date',
        'end_date'
    ];

    protected $dates = ['deleted_at'];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function cemetery()
    {
        return $this->belongsTo('App\Cemetery');
    }
}
