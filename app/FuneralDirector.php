<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class FuneralDirector extends Model implements AuditableContract
{
    //use SoftDeletes;
    use Auditable;

    protected $table = 'funeral_directors';

    protected $fillable = [
        'code',
        'name',
        'address1',
        'address2',
        'suburb',
        'country_id',
        'state_id',
        'postcode',
        'parent_id',
        'is_parent',
        'phone',
        'email',
        'contact_person_name',
        'is_on_hold',
        'gl_liability_code',
        'gl_cost_code',
        'logo'
    ];

    protected $dates = ['deleted_at'];

    public function users()
    {
        return $this->hasMany('App\User', 'fd_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\FuneralDirector', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\FuneralDirector', 'parent_id');
    }
}
