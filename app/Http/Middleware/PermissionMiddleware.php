<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Page;
use App\ProfilePermission;
use Session;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $pagecode)
    {
        if($pagecode == 'login') {
            $this->setPermissions();
            return $next($request);
        } else {
            $this->grantPermissions($pagecode);

            if(Session::get('read_'.$pagecode) == 1) {
                return $next($request);
            } else {
                return redirect()->route('home');
            }
        }
    }

    public function grantPermissions($pagecode)
    {
        $profileId = Auth::User()->profile;
        $page = Page::where('code', $pagecode)->first();

        if ($page != null) {
            $permissions = ProfilePermission::  where('pageId', $page->id) ->
                                                where('profileId', $profileId)->first();

            Session::put('create_'.$pagecode, $permissions->create);
            Session::put('read_'.$pagecode, $permissions->read);
            Session::put('update_'.$pagecode, $permissions->update);
            Session::put('delete_'.$pagecode, $permissions->delete);
        }
    }

    public function setPermissions()
    {
        $profileId = Auth::User()->profile;
        $pages = Page::all();

        foreach ($pages as $page) {
            $permissions = ProfilePermission::  where('pageId', $page->id)->
                                                where('profileId', $profileId)->first();

            if ($permissions != null) {
                Session::put('create_'.$page->code, $permissions->create);
                Session::put('read_'.$page->code, $permissions->read);
                Session::put('update_'.$page->code, $permissions->update);
                Session::put('delete_'.$page->code, $permissions->delete);
            }
        }
    }
}

