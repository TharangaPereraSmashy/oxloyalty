<?php

namespace App\Http\Controllers;

use App\Configuration;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ConfigurationController extends Controller
{
   
    public function index()
    {
        //
    }

    public function create()
    {
        if(session()->get('read_configuration')) {

            $data['config1'] = Configuration::select('parameter','value')->where('parameter','validity_period_days')->get();
            $data['config2'] = Configuration::select('parameter','value')->where('parameter','maximum_inactivity_days')->get();
            
            return view('configuration.update-configuration',$data);

        }else{

            return redirect()->route('home');

        }
    }

    public function store(Request $request)
    {
        if(session()->get('update_configuration')) {

            request()->validate([
                'validity_period_days'      =>  'required',
                'maximum_inactivity_days'   =>  'required'
            ]);

            DB::beginTransaction();

            $parameter1 =   $request->get('parameter1');
            $parameter2 =   $request->get('parameter2');

            $value1     =   $request->get('validity_period_days');
            $value2     =   $request->get('maximum_inactivity_days');

            try {

                Configuration::where('parameter', $parameter1)->update(array('value' => $value1));
                Configuration::where('parameter', $parameter2)->update(array('value' => $value2));

                DB::commit();

                return redirect()->route('configurations.create')
                    ->with('success', 'Configurations Updated Successfully.');

            } catch (\Exception $e) {

                DB::rollback();

                return redirect()->route('configurations.create')
                    ->with('error', 'Error');

            }

        }else{

            return redirect()->route('home');

        }
    }

}
