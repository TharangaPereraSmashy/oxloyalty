<?php

namespace App\Http\Controllers;

use App\CemeteryItem;
use App\CemeteryItemLog;
use App\Cemetery;
use App\Item;

use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CemeteryItemController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(session()->get('read_cemetery_item')) {

            $cemetery_items = CemeteryItem::select('cemetery_items.*', 'items.description AS item', 'items.code AS item_code', 'cemeteries.name AS cemetery')
                ->join('items', 'cemetery_items.item_id', '=', 'items.id')
                ->join('cemeteries', 'cemetery_items.cemetery_id', '=', 'cemeteries.id')
                ->latest()
                ->paginate(10);

            return view('cemetery-item.cemetery-item-search',compact('cemetery_items'))
                ->with('i', (request()->input('page', 1) - 1) * 10);

        }else{

            return redirect()->route('home');

        }
    }

    public function create()
    {
        if(session()->get('create_cemetery_item')) {

            $data['cemeteries'] =   Cemetery::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as cemetery_name"))->get();
            $data['items']      =   Item::select('id', 'code', DB::raw("CONCAT(code,' - ',description) as item_name"))->get();

            return view('cemetery-item.cemetery-item-create', $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function store(Request $request)
    {
        request()->validate([
            'item_id'       =>  'required',
            'cemetery_id'   =>  'required',
            'status'        =>  'required',
            'to_earn'       =>  'required',
            'to_redeem'     =>  'required'
        ]);

        $id             =   "";
        $item_id        =   $request->get('item_id');
        $cemetery_id    =   $request->get('cemetery_id');

        $start_date     =   $request->get('start_date');
        if ($start_date != null)
        $start_date     =   date('Y-m-d', strtotime($start_date));

        $end_date       =   $request->get('end_date');
        if ($end_date != null)
        $end_date        =   date('Y-m-d', strtotime($end_date));

        if($this->cemeteryItemCompositeKeyValidation($id,$item_id,$cemetery_id,$start_date,$end_date)){

            DB::beginTransaction();

            try {

                // Insert into Cemetery Item Table
                $cemetery_item                  = new CemeteryItem();
                $cemetery_item->item_id         = $request->get('item_id');
                $cemetery_item->cemetery_id     = $request->get('cemetery_id');
                $cemetery_item->status          = $request->get('status');
                $cemetery_item->to_earn         = $request->get('to_earn');
                $cemetery_item->to_redeem       = $request->get('to_redeem');
                $cemetery_item->start_date      = $start_date ? $start_date : '1900-01-01';
                $cemetery_item->end_date        = $end_date ? $end_date : '9999-01-01';
                $cemetery_item->save();

                // Insert into Cemetery Item Log Table
                $cemetery_item_log                  = new CemeteryItemLog();
                $cemetery_item_log->item_id         = $request->get('item_id');
                $cemetery_item_log->cemetery_id     = $request->get('cemetery_id');
                $cemetery_item_log->status          = $request->get('status');
                $cemetery_item_log->to_earn         = $request->get('to_earn');
                $cemetery_item_log->to_redeem       = $request->get('to_redeem');
                $cemetery_item_log->start_date      = $start_date ? $start_date : '1900-01-01';
                $cemetery_item_log->end_date        = $end_date ? $end_date : '9999-01-01';
                $cemetery_item_log->save();

                DB::commit();

                return redirect()->route('cemetery_items.index')
                    ->with('success', 'Record Added Successfully.');

            } catch (\Exception $e) {

                DB::rollback();

                return redirect()->route('cemetery_items.create')
                    ->with('error', 'Error');

            }

        }else{

            return redirect()->route('cemetery_items.create')
                ->with('error','Duplicate Record');

        }

    }

    public function show($id)
    {
        if(session()->get('read_cemetery_item')) {

            $data['cemeteries'] =   Cemetery::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as cemetery_name"))->get();
            $data['items']      =   Item::select('id', 'code', DB::raw("CONCAT(code,' - ',description) as item_name"))->get();

            $cemetery_item = CemeteryItem::find($id);

            return view('cemetery-item.cemetery-item-view',compact('cemetery_item'), $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function edit($id)
    {
        if(session()->get('update_emetery_item')) {

            $data['cemeteries'] =   Cemetery::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as cemetery_name"))->get();
            $data['items']      =   Item::select('id', 'code', DB::raw("CONCAT(code,' - ',description) as item_name"))->get();

            $cemetery_item = CemeteryItem::find($id);

            return view('cemetery-item.cemetery-item-edit',compact('cemetery_item'), $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'item_id'       =>  'required',
            'cemetery_id'   =>  'required',
            'status'        =>  'required',
            'to_earn'       =>  'required',
            'to_redeem'     =>  'required'
        ]);

        $item_id        =   $request->get('item_id');
        $cemetery_id    =   $request->get('cemetery_id');

        $start_date     =   $request->get('start_date');
        if ($start_date != null)
        $start_date     =   date('Y-m-d', strtotime($start_date));

        $end_date       =   $request->get('end_date');
        if ($end_date != null)
        $end_date        =   date('Y-m-d', strtotime($end_date));

        if($this->cemeteryItemCompositeKeyValidation($id,$item_id,$cemetery_id,$start_date,$end_date)){

            DB::beginTransaction();

            try {

                // Update Cemetery Item Table
                $cemetery_item                  = CemeteryItem::find($id);
                $cemetery_item->item_id         = $request->get('item_id');
                $cemetery_item->cemetery_id     = $request->get('cemetery_id');
                $cemetery_item->status          = $request->get('status');
                $cemetery_item->to_earn         = $request->get('to_earn');
                $cemetery_item->to_redeem       = $request->get('to_redeem');
                $cemetery_item->start_date      = $start_date ? $start_date : '1900-01-01';
                $cemetery_item->end_date        = $end_date ? $end_date : '9999-01-01';
                $cemetery_item->save();

                // Insert into Cemetery Item Log Table
                $cemetery_item_log                  = new CemeteryItemLog();
                $cemetery_item_log->item_id         = $request->get('item_id');
                $cemetery_item_log->cemetery_id     = $request->get('cemetery_id');
                $cemetery_item_log->status          = $request->get('status');
                $cemetery_item_log->to_earn         = $request->get('to_earn');
                $cemetery_item_log->to_redeem       = $request->get('to_redeem');
                $cemetery_item_log->start_date      = $start_date ? $start_date : '1900-01-01';
                $cemetery_item_log->end_date        = $end_date ? $end_date : '9999-01-01';
                $cemetery_item_log->save();

                DB::commit();

                return redirect()->route('cemetery_items.show',$id)
                    ->with('success','Record Updated Successfully');

            } catch (\Exception $e) {

                DB::rollback();

                return redirect()->route('cemetery_items.index')
                    ->with('error','Error');

            }

        }else{

            return redirect()->route('cemetery_items.edit',$id)
                ->with('error','Duplicate Record');

        }
    }

    public function destroy($id)
    {
        $cemetery_item  =   CemeteryItem::find($id);
        
        $lp_invoices    =   DB::table('lp_invoices')->where('cemetery_item_id',$id)->get();

        if($lp_invoices->count()>0){
            return redirect()->route('cemetery_items.index')
                ->with('error', 'This Item Loyalty has been used in Transactions.');
        }else{
            $status = $cemetery_item->delete();

            if ($status) {
                return redirect()->route('cemetery_items.index')
                ->with('success','Record Deleted Successfully');
            }else{
                return redirect()->route('cemetery_items.index')
                    ->with('error','Error');
            }
        }
    }

    public function searchCemeteryItems(Request $request)
    {
        if(session()->get('read_cemetery_item')) {

            $search_value           =   $request->input('search_value');
            $data['search_value']   =   $search_value;

            $query = CemeteryItem::select('cemetery_items.*', 'items.description AS item', 'items.code AS item_code', 'cemeteries.name AS cemetery')
                ->join('items', 'cemetery_items.item_id', '=', 'items.id')
                ->join('cemeteries', 'cemetery_items.cemetery_id', '=', 'cemeteries.id');

            if (isset($search_value) && $search_value != "")
                $query->orWhere('cemetery_items.to_earn', '=', (int)$search_value )
                    ->orWhere('cemetery_items.to_redeem', '=', (int)$search_value )
                    ->orWhere('items.code', '=', $search_value )
                    ->orWhere('items.description', '=', $search_value )
                    ->orWhere('cemeteries.name', '=', $search_value ); 

            $cemetery_items = $query->latest()->paginate(10)->appends(['search_value' => $search_value]);    

            return view('cemetery-item.cemetery-item-search2',compact('cemetery_items'), $data)
                ->with('i', (request()->input('page', 1) - 1) * 10);    

        }else{

            return redirect()->route('home');

        }
    }

    ////////////////////////////////// Cemetery Item by Item /////////////////////////////////////////////////

    public function createCemeteryItemByItem($item)
    {
        if(session()->get('create_item')) {

            $itemDetails    =   Item::find($item);
            
            if($itemDetails->status == 0){

                $data['status_inactive']    =    "The item status is inactive. Please activate.";

            }

            $data['cemeteries'] =   Cemetery::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as cemetery_name"))->get();
            $data['items']      =   Item::select('id', 'code', DB::raw("CONCAT(code,' - ',description) as item_name"))->where('id', $item)->get();
            $data['item_id']    =   $item;

            return view('cemetery-item.cemetery-item-create-by-item', $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function storeCemeteryItemByItem(Request $request, $item)
    {
        request()->validate([
            'item_id'       =>  'required',
            'cemetery_id'   =>  'required',
            'status'        =>  'required',
            'to_earn'       =>  'required',
            'to_redeem'     =>  'required',
            'start_date'    =>  'required_with:end_date',
            'end_date'      =>  'nullable|required_with:start_date|after:start_date'
        ]);

        $id             =   "";
        $item_id        =   $request->get('item_id');
        $cemetery_id    =   $request->get('cemetery_id');

        $start_date     =   $request->get('start_date');
        if ($start_date != null)
        $start_date     =   date('Y-m-d', strtotime($start_date));

        $end_date       =   $request->get('end_date');
        if ($end_date != null)
        $end_date        =   date('Y-m-d', strtotime($end_date));

        if($this->cemeteryItemCompositeKeyValidation($id,$item_id,$cemetery_id,$start_date,$end_date)){

            DB::beginTransaction();

            try {

                // Insert into Cemetery Item Table
                $cemetery_item                  = new CemeteryItem();
                $cemetery_item->item_id         = $request->get('item_id');
                $cemetery_item->cemetery_id     = $request->get('cemetery_id');
                $cemetery_item->status          = $request->get('status');
                $cemetery_item->to_earn         = $request->get('to_earn');
                $cemetery_item->to_redeem       = $request->get('to_redeem');
                $cemetery_item->start_date      = $start_date ? $start_date : '1900-01-01';
                $cemetery_item->end_date        = $end_date ? $end_date : '9999-01-01';
                $cemetery_item->save();

                // Insert into Cemetery Item Log Table
                $cemetery_item_log              = new CemeteryItemLog();
                $cemetery_item_log->item_id     = $request->get('item_id');
                $cemetery_item_log->cemetery_id = $request->get('cemetery_id');
                $cemetery_item_log->status      = $request->get('status');
                $cemetery_item_log->to_earn     = $request->get('to_earn');
                $cemetery_item_log->to_redeem   = $request->get('to_redeem');
                $cemetery_item_log->start_date  = $start_date ? $start_date : '1900-01-01';
                $cemetery_item_log->end_date    = $end_date ? $end_date : '9999-01-01';
                $cemetery_item_log->save();

                DB::commit();

                return redirect()->route('items.show',$item)
                    ->with('success', 'Record Added Successfully.');

            } catch (\Exception $e) {

                DB::rollback();

                return redirect()->route('create_cemetery_item_by_item', array('item' => $item))
                    ->with('error', 'Error');

            }

        }else{

            return redirect()->route('create_cemetery_item_by_item', array('item' => $item))
                ->with('error','Duplicate Record');

        }

    }

    public function editCemeteryItemByItem($id, $item)
    {
        if(session()->get('update_item')) {

            $data['cemeteries'] =   Cemetery::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as cemetery_name"))->get();
            $data['items']      =   Item::select('id', 'code', DB::raw("CONCAT(code,' - ',description) as item_name"))->where('id', $item)->get();
            $data['item_id']    =   $item;

            $cemetery_item = CemeteryItem::find($id);

            return view('cemetery-item.cemetery-item-edit-by-item',compact('cemetery_item'), $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function updateCemeteryItemByItem(Request $request, $id, $item)
    {
        request()->validate([
            'item_id'       =>  'required',
            'cemetery_id'   =>  'required',
            'status'        =>  'required',
            'to_earn'       =>  'required',
            'to_redeem'     =>  'required',
            'start_date'    =>  'required_with:end_date',
            'end_date'      =>  'nullable|required_with:start_date|after:start_date'
        ]);

        $item_id        =   $request->get('item_id');
        $cemetery_id    =   $request->get('cemetery_id');

        $start_date     =   $request->get('start_date');
        if ($start_date != null)
        $start_date     =   date('Y-m-d', strtotime($start_date));

        $end_date       =   $request->get('end_date');
        if ($end_date != null)
        $end_date        =   date('Y-m-d', strtotime($end_date));

        if($this->cemeteryItemCompositeKeyValidation($id,$item_id,$cemetery_id,$start_date,$end_date)){

            DB::beginTransaction();

            try {

                $cemetery_item  =   CemeteryItem::find($id);

                if($cemetery_item->updated_at != $request->updated_at){

                    DB::rollback();
    
                    return redirect()->route('edit_cemetery_item_by_item', array('id' => $id, 'item' => $item))
                        ->with('error','Record has been modified by another user. Please try again.');
    
                }

                if(strtotime($cemetery_item->start_date) > strtotime($request->start_date)){

                    DB::rollback();
    
                    return redirect()->route('edit_cemetery_item_by_item', array('id' => $id, 'item' => $item))
                        ->with('error','Cannot back date the Start date.');
    
                }

                // Update Cemetery Item Table
                $cemetery_item->item_id         = $request->get('item_id');
                $cemetery_item->cemetery_id     = $request->get('cemetery_id');
                $cemetery_item->status          = $request->get('status');
                $cemetery_item->to_earn         = $request->get('to_earn');
                $cemetery_item->to_redeem       = $request->get('to_redeem');
                $cemetery_item->start_date      = $start_date ? $start_date : '1900-01-01';
                $cemetery_item->end_date        = $end_date ? $end_date : '9999-01-01';
                $cemetery_item->save();

                // Insert into Cemetery Item Log Table
                $cemetery_item_log                  = new CemeteryItemLog();
                $cemetery_item_log->item_id         = $request->get('item_id');
                $cemetery_item_log->cemetery_id     = $request->get('cemetery_id');
                $cemetery_item_log->status          = $request->get('status');
                $cemetery_item_log->to_earn         = $request->get('to_earn');
                $cemetery_item_log->to_redeem       = $request->get('to_redeem');
                $cemetery_item_log->start_date      = $start_date ? $start_date : '1900-01-01';
                $cemetery_item_log->end_date        = $end_date ? $end_date : '9999-01-01';
                $cemetery_item_log->save();

                DB::commit();

                return redirect()->route('items.show',$item)
                    ->with('success','Record Updated Successfully');

            } catch (\Exception $e) {

                DB::rollback();

                return redirect()->route('edit_cemetery_item_by_item', array('id' => $id, 'item' => $item))
                    ->with('error','Error');

            }

        }else{

            return redirect()->route('edit_cemetery_item_by_item', array('id' => $id, 'item' => $item))
                ->with('error','Duplicate Record');

        }
    }

    public function destroyCemeteryItemByItem($id, $item)
    {
        $cemetery_item  =   CemeteryItem::find($id);
        
        $lp_invoices    =   DB::table('lp_invoices')->where('cemetery_item_id',$id)->get();

        if($lp_invoices->count()>0){
            return redirect()->route('items.show',$item)
                ->with('error', 'This Item Loyalty has been used in Transactions.');
        }else{
            $status = $cemetery_item->delete();

            if ($status) {
                return redirect()->route('items.show',$item)
                ->with('success','Record Deleted Successfully');
            }else{
                return redirect()->route('items.show',$item)
                    ->with('error','Error');
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function cemeteryItemCompositeKeyValidation($id,$item_id,$cemetery_id,$start_date,$end_date)
    {
        $query = CemeteryItem::select('*');
        if ($id!="") $query->where('id', '!=', $id );
        $result = $query->get();

        $a=0;
        foreach($result as $res){

            $sd = $result[$a]['start_date'];
            $ed = $result[$a]['end_date'];

            $start_date_validate    = $this->dateRangeValidate($start_date,$sd,$ed);
            $end_date_validate      = $this->dateRangeValidate($end_date,$sd,$ed);

            if((($result[$a]['item_id'] == $item_id) && ($result[$a]['cemetery_id'] == $cemetery_id) && ($start_date_validate||$end_date_validate)))
            {
                return 0;
            }
            $a++;
        }
        return 1;
    }

    public function dateRangeValidate($date,$start_date,$end_date){

        $ts         = strtotime($date);
        $start_ts   = strtotime($start_date);
        $end_ts     = strtotime($end_date);

        if (($ts >= $start_ts) && ($ts <= $end_ts))
        {
            return 1;
        }
        else{
            return 0;
        }
    }

    ////////////////// Import Loyalty Item ///////////////////////////////

    public function importExportLoyaltyItemsView(){

        $files = glob(public_path().'/temp/loyalty_items/*');
        foreach($files as $file){
            if(is_file($file)) unlink($file);
        }

        return view('cemetery-item.import-export-loyalty-items');

    }

    public function importLoyaltyItems(Request $request){

        $request->validate([
            'import_file'   =>  'required|mimes:csv,xlsx,xls'
        ]);

        if($request->hasFile('import_file')){

            $file       =   $request->file('import_file');
            $file_name  =   'TEMP_'.uniqid().time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path().'/temp/loyalty_items/', $file_name);

            $file_path  =   public_path().'/temp/loyalty_items/'.$file_name;

            $excelData  =   \Excel::load($file_path)->get();

            $sheets     =   \Excel::selectSheets()->load($file_path)->get();

            //if ($sheets->count()==1) {

            //    $sheetList[] = $sheets->getTitle();

            //}elseif($sheets->count()>1){

            foreach ($sheets as $sheet) {
                $sheetList[] = $sheet->getTitle();
            }
                
            //}

            $data['file_path']      =   $file_path;
            $data['excel_sheets']   =   $sheetList;

            return view('cemetery-item.select-sheet-loyalty-items', $data);

        }else{

            return redirect()->route('import.export.loyalty.items.view')
                ->with('error', 'Please select a file to import.');  

        }

    } 

    public function selectSheetForLoyaltyItemsImport(Request $request){

        $request->validate([
            'sheet_index'   =>  'required'
        ]);

        $sheet_index    =   $request->input('sheet_index');

        $path           =   $request->input('excel_file_path');

        $excelData      =   \Excel::selectSheetsByIndex($sheet_index)->load($path)->get();
            
        $data['file_path']      =   $path;
        $data['sheet_index']    =   $sheet_index;
        $data['excel_headers']  =   $excelData[0]->getHeading();
        $data['db_columns']     =   Schema::getColumnListing('cemetery_items');

        return view('cemetery-item.start-import-loyalty-items', $data);

    } 

    public function startImportLoyaltyItems(Request $request){

        request()->validate([
            'mapping_field_item_code'       =>  'required',
            'mapping_field_cemetery_code'   =>  'required',
            'mapping_field_status'          =>  'required',
            'mapping_field_to_earn'         =>  'required',
            'mapping_field_to_redeem'       =>  'required',
            'mapping_field_start_date'      =>  'required',
            'mapping_field_end_date'        =>  'required'
        ]);

        DB::beginTransaction();

        try {
            $sheet_index    =   $request->input('sheet_index');
            $path           =   $request->input('excel_file_path');

            $excelData      =   \Excel::selectSheetsByIndex($sheet_index)->load($path)->get();

            $mapping_field_item_code        =   $request->input('mapping_field_item_code');
            $mapping_field_cemetery_code    =   $request->input('mapping_field_cemetery_code');
            $mapping_field_status           =   $request->input('mapping_field_status');
            $mapping_field_to_earn          =   $request->input('mapping_field_to_earn');
            $mapping_field_to_redeem        =   $request->input('mapping_field_to_redeem');
            $mapping_field_start_date       =   $request->input('mapping_field_start_date');
            $mapping_field_end_date         =   $request->input('mapping_field_end_date');

            if($this->IsNullOrEmptyString($mapping_field_item_code)){
                return redirect()->route('import.export.loyalty.items.view')
                    ->with('error','No mapping field for Code');
            }
            if($this->IsNullOrEmptyString($mapping_field_cemetery_code)){
                return redirect()->route('import.export.loyalty.items.view')
                    ->with('error','No mapping field for Description');
            }
            if($this->IsNullOrEmptyString($mapping_field_status)){
                return redirect()->route('import.export.loyalty.items.view')
                    ->with('error','No mapping field for Status');
            }
            if($this->IsNullOrEmptyString($mapping_field_to_earn)){
                return redirect()->route('import.export.loyalty.items.view')
                    ->with('error','No mapping field for Gl Liability Code');
            }
            if($this->IsNullOrEmptyString($mapping_field_to_redeem)){
                return redirect()->route('import.export.loyalty.items.view')
                    ->with('error','No mapping field for Gl Cost Code');
            }
            if($this->IsNullOrEmptyString($mapping_field_start_date)){
                return redirect()->route('import.export.loyalty.items.view')
                    ->with('error','No mapping field for Gl Liability Code');
            }
            if($this->IsNullOrEmptyString($mapping_field_end_date)){
                return redirect()->route('import.export.loyalty.items.view')
                    ->with('error','No mapping field for Gl Cost Code');
            }

            if(count($excelData[0])!=0){

                foreach ($excelData[0] as $key => $value) {

                    $row_no 	    =   $key + 1;
                    $item_id        =   "";
                    $cemetery_id    =   "";
                    $status         =   0;
                    $to_earn        =   "";
                    $to_redeem      =   "";
                    $start_date     =   "";
                    $end_date       =   "";
                    $id             =   "";

                    $map_item_code  =    trim($value->$mapping_field_item_code);
                    $map_cem_code   =    trim($value->$mapping_field_cemetery_code);
                    $map_status     =    trim($value->$mapping_field_status);
                    $map_earn       =    trim($value->$mapping_field_to_earn);
                    $map_redeem     =    trim($value->$mapping_field_to_redeem);
                    $map_start      =    trim($value->$mapping_field_start_date);
                    $map_end        =    trim($value->$mapping_field_end_date);

                    if($this->IsNullOrEmptyString($map_item_code)){
                        if(file_exists($path)) { unlink($path); }
                        return redirect()->route('import.export.loyalty.items.view')
                            ->with('error','Line No : '.$row_no.' - Item Code cannot be null or empty');
                    }else{
                        $item_id    =   Item::where('code',$map_item_code)->pluck('id');
                    }

                    if($this->IsNullOrEmptyString($map_cem_code)){
                        if(file_exists($path)) { unlink($path); }
                        return redirect()->route('import.export.loyalty.items.view')
                            ->with('error','Line No : '.$row_no.' - Cemetery Code cannot be null or empty');
                    }else{
                        $cemetery_id    =   Cemetery::where('code',$map_cem_code)->pluck('id');
                    } 

                    if($this->IsNullOrEmptyString($map_status)){
                        if(file_exists($path)) { unlink($path); }
                        return redirect()->route('import.export.loyalty.items.view')
                            ->with('error','Line No : '.$row_no.' - Status cannot be null or empty');
                    }else{
                        $statusVal  =   strtolower($map_status);
                        if($statusVal=="no" || $statusVal=="false" || $statusVal==0){
                            $status = 0;
                        }elseif($statusVal=="yes" || $statusVal=="true" || $statusVal==1){
                            $status = 1;
                        }else{
                            if(file_exists($path)) { unlink($path); }
                            return redirect()->route('import.export.loyalty.items.view')
                                ->with('error','Line No : '.$row_no.' - Status format Error');
                        }
                    } 

                    if($this->IsNullOrEmptyString($map_earn)){
                        if(file_exists($path)) { unlink($path); }
                        return redirect()->route('import.export.loyalty.items.view')
                            ->with('error','Line No : '.$row_no.' - To Earn cannot be null or empty');
                    }else{
                        $to_earn    =   $map_earn;
                    } 

                    if($this->IsNullOrEmptyString($map_redeem)){
                        if(file_exists($path)) { unlink($path); }
                        return redirect()->route('import.export.loyalty.items.view')
                            ->with('error','Line No : '.$row_no.' - To Redeem cannot be null or empty');
                    }else{
                        $to_redeem  =   $map_redeem;
                    } 

                    if($this->IsNullOrEmptyString($map_start)){
                        if(file_exists($path)) { unlink($path); }
                        return redirect()->route('import.export.loyalty.items.view')
                            ->with('error','Line No : '.$row_no.' - Start Date cannot be null or empty');
                    }else{
                        $start_date =   date('Y-m-d', strtotime($map_start));
                    } 

                    if($this->IsNullOrEmptyString($map_end)){
                        if(file_exists($path)) { unlink($path); }
                        return redirect()->route('import.export.loyalty.items.view')
                            ->with('error','Line No : '.$row_no.' - End Date cannot be null or empty');
                    }else{
                        $end_date   =   date('Y-m-d', strtotime($map_end));
                    } 

                    if ($this->cemeteryItemCompositeKeyValidation($id,$item_id[0],$cemetery_id[0],$start_date,$end_date)) {

                        $loyaltyItems[]    =   [   
                            'item_id'       =>  $item_id[0],
                            'cemetery_id'   =>  $cemetery_id[0],
                            'status'        =>  $status,
                            'to_earn'       =>  $to_earn,
                            'to_redeem'     =>  $to_redeem,
                            'start_date'    =>  $start_date,
                            'end_date'      =>  $end_date
                        ];

                    }else{

                        $loyaltyItems    =   [];

                    }

                }

                if(!empty($loyaltyItems)){

                    CemeteryItem::insert($loyaltyItems);

                    if(file_exists($path)) { unlink($path); }

                    DB::commit();

                    return redirect()->route('import.export.loyalty.items.view')
                        ->with('success', 'Records Imported Successfully.');

                }else{

                    if(file_exists($path)) { unlink($path); }

                    return redirect()->route('import.export.loyalty.items.view')
                        ->with('error', 'Request data does not have any new data to import.');  

                }

            }

        } catch (\Exception $e) {

            if(file_exists($path)) { unlink($path); }

            DB::rollback();

            return redirect()->route('import.export.loyalty.items.view')
                ->with('error',$e->getMessage());

        }

    }

    public function exportLoyaltyItems($type){

        $loyaltyItems   =   CemeteryItem::select('cemetery_items.id', 'items.code AS item_code', 'cemeteries.code AS cemetery_code', 'cemetery_items.status', 'cemetery_items.to_earn', 'cemetery_items.to_redeem', 'cemetery_items.start_date', 'cemetery_items.end_date', 'cemetery_items.deleted_at', 'cemetery_items.created_at', 'cemetery_items.updated_at')
                            ->join('items', 'cemetery_items.item_id', '=', 'items.id')
                            ->join('cemeteries', 'cemetery_items.cemetery_id', '=', 'cemeteries.id')->get()->toArray();

        return \Excel::create('exported_loyalty_items'.uniqid().time().'', function($excel) use ($loyaltyItems) {

            $excel->sheet('exported_loyalty_items_sheet1', function($sheet) use ($loyaltyItems)
            {
                $sheet->fromArray($loyaltyItems);
            });

        })->download($type);

    }

    public function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }

}
