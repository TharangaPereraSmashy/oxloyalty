<?php

namespace App\Http\Controllers;

use App\PointReport;
use App\FuneralDirector;
use PDF;

use App\Helpers\LpQueries;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PointReportController extends Controller
{

    public function index()
    {

        $data['funeral_directors']  =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->get();
        
        return view('loyalty-dollar-report.loyalty-dollar-report-search-admin', $data);

    }

    /* public function viewPointReports(Request $request)
    {
        $data['funeral_directors'] =    FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->get();

        $input = $request->all();

        $funeral_director   =   $input['funeral_director'];
        $action             =   $input['action'];
        $data['action']     =   $action;

        $from_date          =   $input['from_date'];
        if ($from_date != null)
        $from_date          =   date('Y-m-d', strtotime($from_date));

        $to_date            =   $input['to_date'];
        if ($to_date != null)
        $to_date            =   date('Y-m-d', strtotime($to_date));

        switch ($action) {
            case 'reward':
                
                $query  =   DB::table('lp_invoices')
                            ->select('transactions_history.trans_date,transactions_history.trans_type,transactions_history.points Tx_Points,
                            lp_invoices.record_id,lp_invoices.doc_number,lp_invoices.doc_date,lp_invoices.doc_type,
                            lp_invoices.funeral_director,lp_invoices.cemetery,lp_invoices.item_number,lp_invoices.quantity')
                            ->join('transactions_history', 'lp_invoices.id', '=', 'transactions_history.trans_ref')
                            ->where('transactions_history.trans_type', '=', 'REWARD');

                if (isset($funeral_director) && $funeral_director != "")
                    $query->where('lp_invoices.funeral_director', '=', $funeral_director );

                if (isset($from_date) && $from_date != "")
                    $query->where('lp_invoices.doc_date', '>=', $from_date);

                if (isset($to_date) && $to_date != "")
                    $query->where('lp_invoices.doc_date', '<=', $to_date);

                $results = $query->latest()->orderBy('transactions_history.trans_date', 'desc')->paginate(10)->appends(['funeral_director' => $funeral_director, 'from_date' => $from_date, 'to_date' => $to_date]);    

                return view('point-report.point-report-search',compact('results'), $data)
                    ->with('i', (request()->input('page', 1) - 1) * 10);

                break;
    
            case 'redeem':
                
                break;
        }

    } */

    public function viewPointReports(Request $request)
    {
        $data['funeral_directors'] =    FuneralDirector::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->get();

        $input = $request->all();

        $funeral_director   =   $input['funeral_director'];
        if ($funeral_director == null)
        $funeral_director   =   "";
        $data['funeral_director_pdf']   =   $funeral_director;

        $from_date          =   $input['from_date'];
        if ($from_date != null)
        $from_date          =   date('Y-m-d', strtotime($from_date));
        else
        $from_date          =   "";
        $data['from_date_pdf']          =   $from_date;

        $to_date            =   $input['to_date'];
        if ($to_date != null)
        $to_date            =   date('Y-m-d', strtotime($to_date));
        else
        $to_date            =   "";
        $data['to_date_pdf']            =   $to_date;

        $fd   =   FuneralDirector::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as fd_name"), 'is_parent')->where("id", "=", $funeral_director)->get();
        $data['funeral_director_name']    =   $fd[0]->fd_name;
        $is_parent  =   $fd[0]->is_parent;

        $lpquery = new LpQueries();

        if($is_parent==1){
            $data['available_loyalty_dollars']  =   $lpquery->getParentAvailableDollars($funeral_director);
        }else{
            $data['available_loyalty_dollars']  =   $lpquery->getAvailableDollars($funeral_director);
        }

        $query  =   DB::table('all_transactions_history_view')->select('all_transactions_history_view.*', DB::raw("CONCAT(funeral_directors.code,' - ',funeral_directors.name) as fd_name"))
                    ->join('funeral_directors', 'all_transactions_history_view.fd_id', '=', 'funeral_directors.id');
       
        if (isset($funeral_director) && $funeral_director != "") {
            if ($is_parent==1) {
                $query->where('funeral_directors.parent_id', '=', $funeral_director);
            }else{
                $query->where('all_transactions_history_view.fd_id', '=', $funeral_director);
            }
        }

        if (isset($from_date) && $from_date != "")
            $query->where('all_transactions_history_view.trans_date', '>=', $from_date);

        if (isset($to_date) && $to_date != "")
            $query->where('all_transactions_history_view.trans_date', '<=', $to_date);

        $results = $query->latest()->orderBy('all_transactions_history_view.trans_date', 'desc')->paginate(10)->appends(['funeral_director' => $funeral_director, 'from_date' => $from_date, 'to_date' => $to_date]);    

        return view('loyalty-dollar-report.loyalty-dollar-report-search-admin',compact('results'), $data)
            ->with('i', (request()->input('page', 1) - 1) * 10);

    }

    public function generatePointReportsPDF($fd,$from,$to){

        $lpquery = new LpQueries();

        if($fd=="0")$fd="";
        if($from=="0000-00-00")$from="";
        if($to=="0000-00-00")$to="";

        if ($fd!="") {
            $funeral_director   =   FuneralDirector::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as fd_name"), 'is_parent')->where("id", "=", $fd)->get();
            $data['fd_name']    =   $funeral_director[0]->fd_name;
            $is_parent          =   $funeral_director[0]->is_parent;

            if($is_parent==1){
                $data['available_loyalty_dollars']  =   $lpquery->getParentAvailableDollars($fd);
            }else{
                $data['available_loyalty_dollars']  =   $lpquery->getAvailableDollars($fd);
            }
        }else{
            $data['fd_name']    =   "";
            $data['available_loyalty_dollars']  =   "";
        }

        if ($from!="") {
            $data['from']       =   date('d-m-Y', strtotime($from));
        }else{
            $data['from']    =   "";
        }
        
        if ($to!="") {
            $data['to']       =   date('d-m-Y', strtotime($to));
        }else{
            $data['to']    =   "";
        }

        $data['is_parent']      =   $is_parent;
        
        $data['printed_date']   =   date('d-m-Y');

        $query  =   DB::table('all_transactions_history_view')->select('all_transactions_history_view.*', DB::raw("CONCAT(funeral_directors.code,' - ',funeral_directors.name) as fd_name"))
                    ->join('funeral_directors', 'all_transactions_history_view.fd_id', '=', 'funeral_directors.id');

        if (isset($fd) && $fd != "") {
            if ($is_parent==1) {
                $query->where('funeral_directors.parent_id', '=', $fd);
            }else{
                $query->where('all_transactions_history_view.fd_id', '=', $fd);
            }
        }

        if (isset($from) && $from != "")
            $query->where('all_transactions_history_view.trans_date', '>=', $from);

        if (isset($to) && $to != "")
            $query->where('all_transactions_history_view.trans_date', '<=', $to);

        $results = $query->orderBy('all_transactions_history_view.trans_date', 'desc')->get();
  
        $pdf = PDF::loadView('loyalty-dollar-report.loyalty-dollar-report', compact('results'), $data);
        return $pdf->download('loyalty_dollar_report_report.pdf');
  
    }

    public function pointReportsFD($fd_id)
    {
        
        return view('funeral-director.loyalty-dollar-report-search')->with('fd_id', $fd_id);

    }

    public function viewPointReportsFD(Request $request)
    {
        $input = $request->all();
               
        $funeral_director = $input['fd_id'];
      
        $data['funeral_director_pdf']   =   $funeral_director;

        $from_date          =   $input['from_date'];
        if ($from_date != null)
        $from_date          =   date('Y-m-d', strtotime($from_date));
        else
        $from_date          =   "";
        $data['from_date_pdf']  =   $from_date;

        $to_date            =   $input['to_date'];
        if ($to_date != null)
        $to_date            =   date('Y-m-d', strtotime($to_date));
        else
        $to_date            =   "";
        $data['to_date_pdf']    =   $to_date;

        $fd   =   FuneralDirector::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as fd_name"), 'is_parent')->where("id", "=", $funeral_director)->get();
        $data['funeral_director_name']    =   $fd[0]->fd_name;
        $is_parent  =   $fd[0]->is_parent;

        $lpquery = new LpQueries();

        if($is_parent==1){
            $data['available_loyalty_dollars']  =   $lpquery->getParentAvailableDollars($funeral_director);
        }else{
            $data['available_loyalty_dollars']  =   $lpquery->getAvailableDollars($funeral_director);
        }

        $query  =   DB::table('all_transactions_history_view')->select('all_transactions_history_view.*', DB::raw("CONCAT(funeral_directors.code,' - ',funeral_directors.name) as fd_name"))
                    ->join('funeral_directors', 'all_transactions_history_view.fd_id', '=', 'funeral_directors.id');
          
        if ($is_parent==1) {
            $query->where('funeral_directors.parent_id', '=', $funeral_director);
        }else{
            $query->where('all_transactions_history_view.fd_id', '=', $funeral_director);
        }

        if (isset($from_date) && $from_date != "")
            $query->where('all_transactions_history_view.trans_date', '>=', $from_date);
         
        if (isset($to_date) && $to_date != "")
            $query->where('all_transactions_history_view.trans_date', '<=', $to_date);

        $results = $query->latest()->orderBy('all_transactions_history_view.trans_date', 'desc')->paginate(10)->appends(['from_date' => $from_date, 'to_date' => $to_date, 'fd_id' => $funeral_director]);    
        
        return view('funeral-director.loyalty-dollar-report-search',compact('results'), $data)
            ->with('i', (request()->input('page', 1) - 1) * 10)->with('fd_id',$funeral_director) ;

    }

}
