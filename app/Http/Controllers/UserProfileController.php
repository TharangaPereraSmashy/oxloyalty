<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserProfile;
use App\ProfilePermission;
use App\Type;

class UserProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $profiles       =   UserProfile::select('user_profiles.*', 'types.description AS type_name')
                            ->join('types', 'user_profiles.type', '=', 'types.id')->get();
        $data['types']  =   Type::all();
        return view('users/user_profile', compact('profiles'), $data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        request()->validate([
            'type'          =>  'required',
            'description'   =>  'required|string|max:255|unique:user_profiles'
        ]);

        $user               =   new UserProfile();
        $user->description  =   $request->get('description');
        $user->type         =   $request->get('type');
        $user->save();

        return redirect('profile')->with('success', 'Information has been added');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'type'          =>  'required',
            'description'   =>  'required|string|max:255|unique:user_profiles'
        ]);

        $user               =   UserProfile::find($request->get('id'));
        $user->description  =   $request->get('description');
        $user->type         =   $request->get('type');
        $user->save();

        return redirect('profile')->with('success','profile has been edited');
    }

    public function destroy($id)
    {
        $user = UserProfile::find($id);
        
        try{
            $count = ProfilePermission::where('profileId','=', $id)->count();
            
            if ($count > 0)
            {
                return redirect('profile')->with('warning','Profile not empty, it has permissions defined ');
            }
            else
            {
                $user->delete();
                return redirect('profile')->with('success','profile has been deleted');
            }
        }
        catch (Exception $e)
        {
            return redirect('profile')->with('error', $e);
        }
            
    }

    public function getProfileById( $id )
    {
        $user = UserProfile::find($id);
        echo $user;
    }
}
