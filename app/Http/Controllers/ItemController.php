<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemLog;
use App\CemeteryItem;
use App\SpecialOffer;

use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(session()->get('read_item')) {

            $items  =   Item::select('items.*',
                        DB::raw("(SELECT count(*) FROM special_offers
                        WHERE special_offers.item_id = items.id
                        AND special_offers.status = TRUE
                        AND special_offers.deleted_at IS NULL) as sp_count"))->latest()->paginate(10);

            return view('item.item-search',compact('items'))
                ->with('i', (request()->input('page', 1) - 1) * 10);

        }else{

            return redirect()->route('home');

        }
    }

    public function create()
    {
        if(session()->get('create_item')) {

            return view('item.item-create');

        }else{

            return redirect()->route('home');

        }
    }

    public function store(Request $request)
    {
        request()->validate([
            'code'              =>  'required|unique:items',
            'description'       =>  'required',
            'status'            =>  'required',
            'gl_liability_code' =>  'required',
            'gl_cost_code'      =>  'required'
        ]);

        DB::beginTransaction();

        try {

            // Insert into Item Table
            Item::create($request->all());

            // Insert into Item Log Table
            $item_log                       = new ItemLog();
            $item_log->code                 = $request->get('code');
            $item_log->description          = $request->get('description');
            $item_log->gl_liability_code    = $request->get('gl_liability_code');
            $item_log->gl_cost_code         = $request->get('gl_cost_code');
            $item_log->save();

            DB::commit();

            return redirect()->route('items.index')
                ->with('success', 'Record Added Successfully.');

        } catch (\Exception $e) {

            DB::rollback();

            return redirect()->route('items.create')
                ->with('error', 'Error');

        }

    }

    public function show($id)
    {
        if(session()->get('read_item')) {

            $data['cemetery_items'] =   CemeteryItem::select('cemetery_items.*', 'items.description AS item', 'items.code AS item_code', 'cemeteries.name AS cemetery')
                                        ->join('items', 'cemetery_items.item_id', '=', 'items.id')
                                        ->join('cemeteries', 'cemetery_items.cemetery_id', '=', 'cemeteries.id')
                                        ->where("item_id", $id)->get();
                                        
            $data['special_offers'] =   SpecialOffer::select('special_offers.*', 'items.description AS item', 'items.code AS item_code', 'cemeteries.name AS cemetery')
                                        ->join('items', 'special_offers.item_id', '=', 'items.id')
                                        ->join('cemeteries', 'special_offers.cemetery_id', '=', 'cemeteries.id')
                                        ->where("item_id", $id)->get();
            
            $item = Item::find($id);

            return view('item.item-view',compact('item'), $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function edit($id)
    {
        if(session()->get('update_item')) {

        $item = Item::find($id);

            return view('item.item-edit',compact('item'));

        }else{

            return redirect()->route('home');

        }
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'description'       =>  'required',
            'status'            =>  'required',
            'gl_liability_code' =>  'required',
            'gl_cost_code'      =>  'required'
        ]);

        DB::beginTransaction();

        try {

            $item   =   Item::find($id);

            if($item->updated_at != $request->updated_at){

                DB::rollback();

                return redirect()->route('items.edit', $id)
                    ->with('error','Record has been modified by another user. Please try again.');

            }

            $itemLoyalty    =   CemeteryItem::where('item_id', $id)->where('status', 1)->get();
            $specialOffer   =   SpecialOffer::where('item_id', $id)->where('status', 1)->get();

            if( ($request->status == 0 && $item->status != $request->status) || 
                ($request->gl_liability_code != "" && $item->gl_liability_code != $request->gl_liability_code) || 
                ($request->gl_cost_code != "" && $item->gl_cost_code != $request->gl_cost_code) ){

                if(count($itemLoyalty)!=0){

                    DB::rollback();

                    return redirect()->route('items.edit', $id)
                        ->with('error','There are Active Loyalty Items.');

                }elseif(count($specialOffer)!=0){

                    DB::rollback();

                    return redirect()->route('items.edit', $id)
                        ->with('error','There are Active Special Offers.');

                }
            }

            // Update Item Table
            $item->update($request->all());

            // Insert into Item Log Table
            $item_log                       = new ItemLog();
            $item_log->code                 = $request->get('code');
            $item_log->description          = $request->get('description');
            $item_log->gl_liability_code    = $request->get('gl_liability_code');
            $item_log->gl_cost_code         = $request->get('gl_cost_code');
            $item_log->save();

            DB::commit();

            return redirect()->route('items.show', $id)
                ->with('success','Record Updated Successfully');

        } catch (\Exception $e) {

            DB::rollback();

            return redirect()->route('items.edit', $id)
                ->with('error','Error');

        }
    }

    public function destroy($id)
    {
        $item = Item::find($id);

        if($item->cemeteryItems()->exists()){
            return redirect()->route('items.index')
                ->with('error', 'This Item is used in Item Loyalty.');
        }else if($item->specialOffers()->exists()){
            return redirect()->route('items.index')
                ->with('error', 'This Item is used in Special Offers.');
        }else{
            $status = $item->delete();

            if ($status) {
                return redirect()->route('items.index')
                ->with('success', 'Record Deleted Successfully');
            } else {
                return redirect()->route('items.index')
                ->with('error', 'Error');
            }
        }
    }

    public function searchItems(Request $request)
    {
        if(session()->get('read_item')) {

            $search_value           =   $request->input('search_value');
            $data['search_value']   =   $search_value;

            $query  =   Item::select('items.*',
                        DB::raw("(SELECT count(*) FROM special_offers
                        WHERE special_offers.item_id = items.id
                        AND special_offers.status = TRUE
                        AND special_offers.deleted_at IS NULL) as sp_count"));
        

            if (isset($search_value) && $search_value != "")
                $query->orWhere(DB::raw("LOWER(items.code)"), 'LIKE', '%'.strtolower($search_value).'%')
                    ->orWhere(DB::raw("LOWER(items.description)"), 'LIKE', '%'.strtolower($search_value).'%')
                    ->orWhere(DB::raw("LOWER(items.gl_liability_code)"), 'LIKE', '%'.strtolower($search_value).'%')
                    ->orWhere(DB::raw("LOWER(items.gl_cost_code)"), 'LIKE', '%'.strtolower($search_value).'%');


            $items = $query->latest()->paginate(10)->appends(['search_value' => $search_value]);    

            return view('item.item-search2',compact('items'), $data)
                ->with('i', (request()->input('page', 1) - 1) * 10);

        }else{

            return redirect()->route('home');

        }
    }

    public function importExportItemsView(){

        $files = glob(public_path().'/temp/items/*');
        foreach($files as $file){
            if(is_file($file)) unlink($file);
        }

        return view('item.import-export-items');

    }

    public function importItems(Request $request){
        
        request()->validate([
            'import_file'   =>  'required|mimes:csv,xlsx,xls'
        ]);
        

        if($request->hasFile('import_file')){

            $file       =   $request->file('import_file');
            
            $file_name  =   'TEMP_'.uniqid().time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path().'/temp/items/', $file_name);

            $file_path  = public_path().'/temp/items/'.$file_name;

            $excelData  =   \Excel::load($file_path)->get();

            $sheets     =   \Excel::selectSheets()->load($file_path)->get();

            //if ($sheets->count()==1) {

                //$sheetList[] = $sheets->getTitle();

           // }elseif($sheets->count()>1){

            foreach ($sheets as $sheet) {
                $sheetList[] = $sheet->getTitle();
            }
                
            //}
            //dd($sheets);
            
            $data['file_path']      =   $file_path;
            $data['excel_sheets']   =   $sheetList;

            return view('item.select-sheet', $data);

        }else{

            return redirect()->route('import.export.view')
                ->with('error', 'Please select a file to import.');  

        }

    } 

    public function selectSheetForImport(Request $request){

        request()->validate([
            'sheet_index'   =>  'required'
        ]);

        $sheet_index    =   $request->input('sheet_index');

        $path           =   $request->input('excel_file_path');

        $excelData      =   \Excel::selectSheetsByIndex($sheet_index)->load($path)->get();
            
        $data['file_path']      =   $path;
        $data['sheet_index']    =   $sheet_index;
        $data['excel_headers']  =   $excelData[0]->getHeading();
        $data['db_columns']     =   Schema::getColumnListing('items');

        return view('item.start-import-items', $data);

    } 

    /* public function startImportItems(Request $request){

        $rows       =   json_decode($request->input('excel_data_rows'));
        $headers    =   $request->input('excel_header_name');
        $db_columns =   $request->input('db_column_name');

        $existingRecords = Item::pluck('id')->toArray();

        dd(count($rows));

        if($rows->count()){

            $items    =   [];

            $i  = 0;
            foreach ($rows[0] as $key => $value) {

                dd($db_columns[$i]);

                $items[]    =   [   
                    $db_columns[$i] =>  $value->$headers[$i]
                ];

                $i++;

            }

            if(!empty($items)){

                Item::insert($items);

                return redirect()->route('import.export.view')
                    ->with('success', 'Records Imported Successfully.');

            }else{

                return redirect()->route('import.export.view')
                    ->with('error', 'Request data does not have any new data to import.');  

            }

        }

    }  */

    /* public function startImportItems(Request $request){

        DB::beginTransaction();

        try {

            $rows   =   json_decode($request->input('excel_data_rows'));

            $mapping_field_code                 =   $request->input('mapping_field_code');
            $mapping_field_description          =   $request->input('mapping_field_description');
            $mapping_field_status               =   $request->input('mapping_field_status');
            $mapping_field_gl_liability_code    =   $request->input('mapping_field_gl_liability_code');
            $mapping_field_gl_cost_code         =   $request->input('mapping_field_gl_cost_code');

            $existingRecords = Item::pluck('code')->toArray();

            if(count($rows)!=0){

                foreach ($rows as $key => $value) {

                    $code               =   "";
                    $description        =   "";
                    $status             =   0;
                    $gl_liability_code  =   "";
                    $gl_cost_code       =   "";

                    if($mapping_field_code == ""){
                        DB::rollback();
                        return redirect()->route('import.export.view')
                            ->with('error','No mapping field for Code');
                    }elseif($value->$mapping_field_code == ""){
                        DB::rollback();
                        return redirect()->route('import.export.view')
                            ->with('error','Code cannot be null or empty');
                    }else{
                        $code   =   $value->$mapping_field_code;
                    }

                    if($mapping_field_description == ""){
                        DB::rollback();
                        return redirect()->route('import.export.view')
                            ->with('error','No mapping field for Description');
                    }elseif($value->$mapping_field_description == ""){
                        DB::rollback();
                        return redirect()->route('import.export.view')
                            ->with('error','Description cannot be null or empty');
                    }else{
                        $description   =   $value->$mapping_field_description;
                    } 

                    if ($mapping_field_code != "" && !in_array(($value->$mapping_field_code),$existingRecords)) {

                        $items[]    =   [   
                            'code'              =>  $value->$mapping_field_code,
                            'description'       =>  $value->$mapping_field_description,
                            'status'            =>  $value->$mapping_field_status,
                            'gl_liability_code' =>  $value->$mapping_field_gl_liability_code,
                            'gl_cost_code'      =>  $value->$mapping_field_gl_cost_code
                        ];

                    }else{

                        $items    =   [];

                    }

                }

                if(!empty($items)){

                    Item::insert($items);

                    DB::commit();

                    return redirect()->route('import.export.view')
                        ->with('success', 'Records Imported Successfully.');

                }else{

                    return redirect()->route('import.export.view')
                        ->with('error', 'Request data does not have any new data to import.');  

                }

            }

        } catch (\Exception $e) {

            DB::rollback();

            return redirect()->route('import.export.view')
                ->with('error',$e->getMessage());

        }

    }  */

    public function startImportItems(Request $request){

        request()->validate([
            'mapping_field_code'                =>  'required',
            'mapping_field_description'         =>  'required',
            'mapping_field_status'              =>  'required',
            'mapping_field_gl_liability_code'   =>  'required',
            'mapping_field_gl_cost_code'        =>  'required'
        ]);

        DB::beginTransaction();

        try {
            $sheet_index    =   $request->input('sheet_index');
            $path           =   $request->input('excel_file_path');

            $excelData      =   \Excel::selectSheetsByIndex($sheet_index)->load($path)->get();

            $mapping_field_code                 =   $request->input('mapping_field_code');
            $mapping_field_description          =   $request->input('mapping_field_description');
            $mapping_field_status               =   $request->input('mapping_field_status');
            $mapping_field_gl_liability_code    =   $request->input('mapping_field_gl_liability_code');
            $mapping_field_gl_cost_code         =   $request->input('mapping_field_gl_cost_code');

            $existingRecords = Item::pluck('code')->toArray();

            if($this->IsNullOrEmptyString($mapping_field_code)){
                return redirect()->route('import.export.view')
                    ->with('error','No mapping field for Code');
            }
            if($this->IsNullOrEmptyString($mapping_field_description)){
                return redirect()->route('import.export.view')
                    ->with('error','No mapping field for Description');
            }
            if($this->IsNullOrEmptyString($mapping_field_status)){
                return redirect()->route('import.export.view')
                    ->with('error','No mapping field for Status');
            }
            if($this->IsNullOrEmptyString($mapping_field_gl_liability_code)){
                return redirect()->route('import.export.view')
                    ->with('error','No mapping field for Gl Liability Code');
            }
            if($this->IsNullOrEmptyString($mapping_field_gl_cost_code)){
                return redirect()->route('import.export.view')
                    ->with('error','No mapping field for Gl Cost Code');
            }

            if(count($excelData[0])!=0){

                foreach ($excelData[0] as $key => $value) {

                    $row_no             =   $key + 1;
                    $code               =   "";
                    $description        =   "";
                    $status             =   0;
                    $gl_liability_code  =   "";
                    $gl_cost_code       =   "";

                    $map_code       =    trim($value->$mapping_field_code);
                    $map_desc       =    trim($value->$mapping_field_description);
                    $map_status     =    trim($value->$mapping_field_status);
                    $map_gll_code   =    trim($value->$mapping_field_gl_liability_code);
                    $map_glc_code   =    trim($value->$mapping_field_gl_cost_code);

                    if($this->IsNullOrEmptyString($map_code)){
                        if(file_exists($path)) { unlink($path); }
                        return redirect()->route('import.export.view')
                            ->with('error','Line No : '.$row_no.' - Code cannot be null or empty');
                    }else{
                        $code   =   $map_code;
                    }

                    if($this->IsNullOrEmptyString($map_desc)){
                        if(file_exists($path)) { unlink($path); }
                        return redirect()->route('import.export.view')
                            ->with('error','Line No : '.$row_no.' - Description cannot be null or empty');
                    }else{
                        $description   =   $map_desc;
                    } 

                    if($this->IsNullOrEmptyString($map_status)){
                        if(file_exists($path)) { unlink($path); }
                        return redirect()->route('import.export.view')
                            ->with('error','Line No : '.$row_no.' - Status cannot be null or empty');
                    }else{
                        $statusVal  =   strtolower($map_status);
                        if($statusVal=="no" || $statusVal=="false" || $statusVal==0){
                            $status = 0;
                        }elseif($statusVal=="yes" || $statusVal=="true" || $statusVal==1){
                            $status = 1;
                        }else{
                            if(file_exists($path)) { unlink($path); }
                            return redirect()->route('import.export.view')
                                ->with('error','Line No : '.$row_no.' - Status format Error');
                        }
                    } 

                    if($this->IsNullOrEmptyString($map_gll_code)){
                        if(file_exists($path)) { unlink($path); }
                        return redirect()->route('import.export.view')
                            ->with('error','Line No : '.$row_no.' - Gl Liability Code cannot be null or empty');
                    }else{
                        $gl_liability_code  =   $map_gll_code;
                    } 

                    if($this->IsNullOrEmptyString($map_glc_code)){
                        if(file_exists($path)) { unlink($path); }
                        return redirect()->route('import.export.view')
                            ->with('error','Line No : '.$row_no.' - Gl Cost Code cannot be null or empty');
                    }else{
                        $gl_cost_code   =   $map_glc_code;
                    } 

                    if (!in_array($map_code, $existingRecords)) {

                        $items[]    =   [   
                            'code'              =>  $code,
                            'description'       =>  $description,
                            'status'            =>  $status,
                            'gl_liability_code' =>  $gl_liability_code,
                            'gl_cost_code'      =>  $gl_cost_code
                        ];

                    }else{

                        $items    =   [];

                    }

                }

                if(!empty($items)){

                    Item::insert($items);

                    if(file_exists($path)) { unlink($path); }

                    DB::commit();

                    return redirect()->route('import.export.view')
                        ->with('success', 'Records Imported Successfully.');

                }else{

                    if(file_exists($path)) { unlink($path); }

                    return redirect()->route('import.export.view')
                        ->with('error', 'Request data does not have any new data to import.');  

                }

            }

        } catch (\Exception $e) {

            if(file_exists($path)) { unlink($path); }

            DB::rollback();

            return redirect()->route('import.export.view')
                ->with('error',$e->getMessage());

        }

    } 

    public function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }

    public function exportItems($type){

        $items = Item::get()->toArray();

        return \Excel::create('exported_items'.uniqid().time().'', function($excel) use ($items) {

            $excel->sheet('exported_items_sheet1', function($sheet) use ($items)
            {
                $sheet->fromArray($items);
            });

        })->download($type);

    }   

}
