<?php

namespace App\Http\Controllers;

use App\Audit;
use App\CemeteryItem;
use App\Cemetery;
use App\FuneralDirector;
use App\Item;
use App\SpecialOffer;
use App\LoyaltyDollarRequest;
use App\User;

use Illuminate\Http\Request;

class AuditController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(session()->get('read_audit')) {

            $data['components'] =   Audit::distinct()->get(['auditable_type']);

            $audits = Audit::select('audits.*','users.name as user_name')
                ->join('users', 'audits.user_id', '=', 'users.id')
                ->latest()
                ->paginate(10);

            /* $audits->map(function ($audits) {
                //DB::table($audits->auditable_type)->select(GetPK($audits->auditable_type))->find($audits->auditable_id);
                //$luName::select(GetPK($audits->auditable_type))->find($audits->auditable_id);
                if($audits->auditable_type == "cemeteries"){
                    $cemetery                   =   Cemetery::select('code')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $cemetery->code;
                }elseif($audits->auditable_type == "cemetery_items"){
                    $cemetery_item              =   CemeteryItem::select('item_id')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $cemetery_item->item_id;
                }elseif($audits->auditable_type == "funeral_directors"){
                    $funeral_director           =   FuneralDirector::select('code')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $funeral_director->code;
                }elseif($audits->auditable_type == "items"){
                    $item                       =   Item::select('code')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $item->code;
                }elseif($audits->auditable_type == "loyalty_dollar_requests"){
                    $loyalty_dollar_request     =   LoyaltyDollarRequest::select('id')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $loyalty_dollar_request->id;
                }elseif($audits->auditable_type == "special_offers"){
                    $special_offer              =   SpecialOffer::select('code')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $special_offer->code;
                }elseif($audits->auditable_type == "users"){
                    $user                       =   User::select('email')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $user->email;
                }
                //$audits['auditable_code']   =   $audits->auditable_id;
                return $audits;
            }); */

            return view('audit.audit-search',compact('audits'), $data)
                ->with('i', (request()->input('page', 1) - 1) * 10);

        }else{

            return redirect()->route('home');

        }
    }

    public function show($id)
    {
        if(session()->get('read_audit')) {

            $audit = Audit::find($id);
            
            return view('audit.audit-view',compact('audit'));

        }else{

            return redirect()->route('home');

        }
    }

    public function searchAudits(Request $request)
    {
        if(session()->get('read_audit')) {

            $input = $request->all();

            $data['components'] =   Audit::distinct()->get(['auditable_type']);
            $auditable_type     =   $input['auditable_type'];

            //$from_date          =   $input['from_date'];
            if (isset($input['from_date']) && $input['from_date'] != null) {
                $from_date      =   date('Y-m-d', strtotime($input['from_date']));
            }else{
                $from_date      =   null;
            }

            //$to_date            =   $input['to_date'];
            if (isset($input['to_date']) && $input['to_date'] != null) {
                $to_date        =   date('Y-m-d', strtotime($input['to_date']));
            }else{
                $to_date        =   null;
            }

            $query = Audit::select('audits.*','users.name as user_name')
                ->join('users', 'audits.user_id', '=', 'users.id');

            if (isset($auditable_type) && $auditable_type != "")
                $query->where('audits.auditable_type', '=', $auditable_type );

            if (isset($from_date) && $from_date != "")
                $query->where('audits.created_at', '>=', $from_date);

            if (isset($to_date) && $to_date != "")
                $query->where('audits.created_at', '<=', $to_date);

            $audits = $query->latest()->paginate(10)->appends(['auditable_type' => $auditable_type, 'from_date' => $from_date, 'to_date' => $to_date]);    
            
            /* $audits->map(function ($audits) {
                if($audits->auditable_type == "cemeteries"){
                    $cemetery                   =   Cemetery::select('code')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $cemetery->code;
                }elseif($audits->auditable_type == "cemetery_items"){
                    $cemetery_item              =   CemeteryItem::select('item_id')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $cemetery_item->item_id;
                }elseif($audits->auditable_type == "funeral_directors"){
                    $funeral_director           =   FuneralDirector::select('code')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $funeral_director->code;
                }elseif($audits->auditable_type == "items"){
                    $item                       =   Item::select('code')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $item->code;
                }elseif($audits->auditable_type == "loyalty_dollar_requests"){
                    $loyalty_dollar_request     =   LoyaltyDollarRequest::select('id')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $loyalty_dollar_request->id;
                }elseif($audits->auditable_type == "special_offers"){
                    $special_offer              =   SpecialOffer::select('code')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $special_offer->code;
                }elseif($audits->auditable_type == "users"){
                    $user                       =   User::select('email')->find($audits->auditable_id);
                    $audits['auditable_code']   =   $user->email;
                }else{
                    $audits['auditable_code']   =   "";
                }
                return $audits;
            }); */

            return view('audit.audit-search',compact('audits'), $data)
                ->with('i', (request()->input('page', 1) - 1) * 10);

        }else{

            return redirect()->route('home');

        }
    }

}
