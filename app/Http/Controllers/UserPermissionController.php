<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProfilePermission;
use App\Type;
use App\UserProfile;
use App\Page;

class UserPermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:login');
    }

    public function index()
    {
        //
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $permissions = new ProfilePermission();
        $recordExist = ProfilePermission::  where('pageId',$request->get('pageId'))->
                                            where('profileId',$request->get('profileId'))->first();

        if (is_null($recordExist)) {
            $permissions->profileId = $request->get('profileId');
            $permissions->pageId = $request->get('pageId');

            if($request->get('create') == 1) {
                $permissions->create = true;
            }
            else {
                $permissions->create = false;
            }

            if($request->get('read') == 1) {
                $permissions->read = true;
            }
            else {
                $permissions->read = false;
            }

            if($request->get('update') == 1) {
                $permissions->update = true;
            }
            else {
                $permissions->update = false;
            }

            if($request->get('delete') == 1) {
                $permissions->delete = true;
            }
            else {
                $permissions->delete = false;
            }
            $permissions->save();

            return redirect()->route('permission.show', $permissions->profileId)
                ->with('success','Information has been added.');
        } else {
            return redirect()->route('permission.show', $request->get('profileId'))
                ->with('error','Profile permissions already granted.');
        }
    }

    public function show($id)
    {
        $types = Type::all('description','id');
        $pages = Page::all('description','id');
        $profile = UserProfile::find($id)->toArray();

        $permissionsObj = new ProfilePermission();
        $permissions = $permissionsObj->getAllPermissionsByprofile($id)->toArray();

        return view('users.profile_permission',compact('types','pages', 'profile', 'permissions'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
         $permissions = ProfilePermission::find($request->get('id'));

         $recordExist = ProfilePermission:: where('pageId',$request->get('pageId'))->
                                            where('profileId',$request->get('profileId'))->
                                            where('id', '<>',  $request->get('id') )->first();

        if (is_null($recordExist)) {
            $permissions->pageId = $request->get('pageId');

            if($request->get('create') == 1) {
                $permissions->create = true;
            }
            else {
                $permissions->create = false;
            }

            if($request->get('read') == 1) {
                $permissions->read = true;
            }
            else {
                $permissions->read = false;
            }

            if($request->get('update') == 1) {
                $permissions->update = true;
            }
            else {
                $permissions->update = false;
            }

            if($request->get('delete') == 1) {
                $permissions->delete = true;
            }
            else {
                $permissions->delete = false;
            }
            $permissions->save();

            return redirect()->route('permission.show', $request->get('profileId'))
                ->with('success','profile has been edited.');
        } else {
            return redirect()->route('permission.show', $request->get('profileId'))
                ->with('error','Profile permissions already granted.');
        }
    }

    public function destroy($id)
    {
        $permissionsObj = ProfilePermission::find($id);

        if($permissionsObj->delete()) {
            return redirect()->route('permission.show', $permissionsObj->profileId)
                ->with('success','profile permissions has been deleted.');
        }else {
            return redirect()->route('permission.show', $permissionsObj->profileId)
                ->with('error','Error');
        }
    }

    public function getPermissionById($id)
    {
        $permissionsObj = ProfilePermission::find($id);
        echo $permissionsObj;
    }
}
