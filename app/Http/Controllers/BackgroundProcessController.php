<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CmsTransErrors;

use Illuminate\Support\Facades\DB;
use App\BackgroundJobParameter;

class BackgroundProcessController extends Controller
{
    public function viewErrorDetails()
    {
        if(session()->get('read_background_process')) {
            
            $cms_trans_errors           =   CmsTransErrors::select('id', 'record_id', 'doc_number', 'doc_date', 'doc_type', 'error_number', 'error_type', 'error', 'funeral_director', 'cemetery', 'item_number', 'quantity', 'rewards', 'rewards_redeemed')
                                                            ->where('is_reprocessed', 0)
                                                            ->where('is_ignored', 0)->get();

            $ignored_cms_trans_errors   =   CmsTransErrors::select('id', 'record_id', 'doc_number', 'doc_date', 'doc_type', 'error_number', 'error_type', 'error', 'funeral_director', 'cemetery', 'item_number', 'quantity', 'rewards', 'rewards_redeemed')
                                                            ->where('is_reprocessed', 0)
                                                            ->where('is_ignored', 1)->get();

            return view('background-process.view-error-details',compact('cms_trans_errors','ignored_cms_trans_errors'));

        }else{

            return redirect()->route('home');

        }
    }

    public function ignoreCmsTransErrors($id)
    {
        if(session()->get('update_background_process')) {

            $cms_trans_error                =   CmsTransErrors::find($id);
            $cms_trans_error->is_ignored    =   1;
            $cms_trans_error->save();

            return redirect()->route('view.error.details')
                ->with('success','Error Ignored Successfully');
                
        }else{

            return redirect()->route('home');

        }
    }

    public function unignoreCmsTransErrors($id)
    {
        if(session()->get('update_background_process')) {

            $cms_trans_error                =   CmsTransErrors::find($id);
            $cms_trans_error->is_ignored    =   0;
            $cms_trans_error->save();

            return redirect()->route('view.error.details')
                ->with('success','Error Included Successfully');
                
        }else{

            return redirect()->route('home');

        }
    }

    public function viewStatusParameters()
    {
        if(session()->get('read_background_process')) {

            $data['cms_lp_sync']    =   DB::select( "SELECT run_start,run_end,is_error FROM background_jobs WHERE run_type = 'CMSTOLPSYNC' 
            AND run_start = (SELECT MAX(run_start) FROM background_jobs WHERE run_type = 'CMSTOLPSYNC') LIMIT 1" );
            
            $data['fd_sync']        =   DB::select( "SELECT run_start,run_end,is_error FROM background_jobs WHERE run_type = 'FDSYNC' 
            AND run_start = (SELECT MAX(run_start) FROM background_jobs WHERE run_type = 'FDSYNC') LIMIT 1" );

            $data['rewards']        =   DB::select( "SELECT run_start,run_end,is_error FROM background_jobs WHERE run_type = 'REWARD' 
            AND run_start = (SELECT MAX(run_start) FROM background_jobs WHERE run_type = 'REWARD') LIMIT 1" );

            $data['redeems']        =   DB::select( "SELECT run_start,run_end,is_error FROM background_jobs WHERE run_type = 'REDEEM' 
            AND run_start = (SELECT MAX(run_start) FROM background_jobs WHERE run_type = 'REDEEM') LIMIT 1" );

            $data['cn_on_rewards']  =   DB::select( "SELECT run_start,run_end,is_error FROM background_jobs WHERE run_type = 'CNREWARD' 
            AND run_start = (SELECT MAX(run_start) FROM background_jobs WHERE run_type = 'CNREWARD') LIMIT 1" );

            $data['cn_on_redeems']  =   DB::select( "SELECT run_start,run_end,is_error FROM background_jobs WHERE run_type = 'CNREDEEM' 
            AND run_start = (SELECT MAX(run_start) FROM background_jobs WHERE run_type = 'CNREDEEM') LIMIT 1" );

            $data['expiry']        =   DB::select( "SELECT run_start,run_end,is_error FROM background_jobs WHERE run_type = 'EXPIRY' 
            AND run_start = (SELECT MAX(run_start) FROM background_jobs WHERE run_type = 'EXPIRY') LIMIT 1" );

            $data['lp_cms_sync'] =   DB::select( "SELECT run_start,run_end,is_error FROM background_jobs WHERE run_type = 'LPTOCMSSYNC' 
            AND run_start = (SELECT MAX(run_start) FROM background_jobs WHERE run_type = 'LPTOCMSSYNC') LIMIT 1" );

            $data['revaluation']    =   DB::select( "SELECT run_start,run_end,is_error FROM background_jobs WHERE run_type = 'REVAL' 
            AND run_start = (SELECT MAX(run_start) FROM background_jobs WHERE run_type = 'REVAL') LIMIT 1" );

            $data['gl_cms_sync']    =   DB::select( "SELECT run_start,run_end,is_error FROM background_jobs WHERE run_type = 'GLTOCMSSYNC' 
            AND run_start = (SELECT MAX(run_start) FROM background_jobs WHERE run_type = 'GLTOCMSSYNC') LIMIT 1" );

            $data['config1'] = BackgroundJobParameter::select('*')->where('parameter','host')->get();
            $data['config2'] = BackgroundJobParameter::select('*')->where('parameter','port')->get();
            $data['config3'] = BackgroundJobParameter::select('*')->where('parameter','sender')->get();
            $data['config4'] = BackgroundJobParameter::select('*')->where('parameter','password')->get();
            $data['config5'] = BackgroundJobParameter::select('*')->where('parameter','default_recipient')->get();
            $data['config6'] = BackgroundJobParameter::select('*')->where('parameter','additional_recipients')->get();
            
            return view('background-process.view-status-parameters', $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function updateParameters(Request $request)
    {
        if(session()->get('update_background_process')) {

            request()->validate([
                'val1'  =>  'required',
                'val2'  =>  'required',
                'val3'  =>  'required',
                'val4'  =>  'required',
                'val5'  =>  'required',
                'val6'  =>  'required'
            ]);

            DB::beginTransaction();

            try {

                $param1   =   $request->get('param1');
                $param2   =   $request->get('param2');
                $param3   =   $request->get('param3');
                $param4   =   $request->get('param4');
                $param5   =   $request->get('param5');
                $param6   =   $request->get('param6');

                $val1     =   $request->get('val1');
                $val2     =   $request->get('val2');
                $val3     =   $request->get('val3');
                $val4     =   $request->get('val4');
                $val5     =   $request->get('val5');
                $val6     =   $request->get('val6');

                BackgroundJobParameter::where('parameter', $param1)->update(array('value' => $val1));
                BackgroundJobParameter::where('parameter', $param2)->update(array('value' => $val2));
                BackgroundJobParameter::where('parameter', $param3)->update(array('value' => $val3));
                BackgroundJobParameter::where('parameter', $param4)->update(array('value' => $val4));
                BackgroundJobParameter::where('parameter', $param5)->update(array('value' => $val5));
                BackgroundJobParameter::where('parameter', $param6)->update(array('value' => $val6));

                DB::commit();

                return redirect()->route('view.status.parameters')
                    ->with('success', 'Background Job Parameters Updated Successfully.');

            } catch (\Exception $e) {

                DB::rollback();

                return redirect()->route('view.status.parameters')
                    ->with('error', 'Error');

            }

        }else{

            return redirect()->route('home');

        }
    }
}
