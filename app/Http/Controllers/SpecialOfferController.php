<?php

namespace App\Http\Controllers;

use App\SpecialOffer;
use App\Cemetery;
use App\Item;
use App\FuneralDirector;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class SpecialOfferController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(session()->get('read_special_offer')) {

            $special_offers = SpecialOffer::select('special_offers.*', 'items.description AS item', 'items.code AS item_code', 'cemeteries.name AS cemetery')
                ->join('items', 'special_offers.item_id', '=', 'items.id')
                ->join('cemeteries', 'special_offers.cemetery_id', '=', 'cemeteries.id')
                ->latest()
                ->paginate(10);

            return view('special-offer.special-offer-search',compact('special_offers'))
                ->with('i', (request()->input('page', 1) - 1) * 10);

        }else{

            return redirect()->route('home');

        }
    }

    public function create()
    {
        if(session()->get('create_special_offer')) {

            $data['cemeteries']     =   Cemetery::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as cemetery_name"))->get();
            $data['items']          =   Item::select('id', 'code', DB::raw("CONCAT(code,' - ',description) as item_name"))->get();
            $data['fd_from_codes']  =   FuneralDirector::select('code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->get();
            $data['fd_to_codes']    =   FuneralDirector::select('code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->get();

            return view('special-offer.special-offer-create', $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function store(Request $request)
    {
        request()->validate([
            'code'          =>  'required|unique:special_offers',
            'description'   =>  'required',
            'item_id'       =>  'required',
            'cemetery_id'   =>  'required',
            'status'        =>  'required',
            'to_earn'       =>  'required',
            'to_redeem'     =>  'required',
            'start_date'    =>  'required',
            'end_date'      =>  'required'
        ]);

        $input = $request->all();

        $start_date          =   $input['start_date'];
        if ($start_date != null)
        $start_date          =   date('Y-m-d', strtotime($start_date));

        $end_date            =   $input['end_date'];
        if ($end_date != null)
        $end_date            =   date('Y-m-d', strtotime($end_date));

        SpecialOffer::create($input);

        return redirect()->route('special_offers.index')
            ->with('success', 'Record Added Successfully.');
    }

    public function show($id)
    {
        if(session()->get('read_special_offer')) {

            $data['cemeteries']     =   Cemetery::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as cemetery_name"))->get();
            $data['items']          =   Item::select('id', 'code', DB::raw("CONCAT(code,' - ',description) as item_name"))->get();
            $data['fd_from_codes']  =   FuneralDirector::select('code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->get();
            $data['fd_to_codes']    =   FuneralDirector::select('code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->get();

            $special_offer = SpecialOffer::find($id);

            return view('special-offer.special-offer-view',compact('special_offer'), $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function edit($id)
    {
        if(session()->get('update_special_offer')) {

            $data['cemeteries']     =   Cemetery::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as cemetery_name"))->get();
            $data['items']          =   Item::select('id', 'code', DB::raw("CONCAT(code,' - ',description) as item_name"))->get();
            $data['fd_from_codes']  =   FuneralDirector::select('code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->get();
            $data['fd_to_codes']    =   FuneralDirector::select('code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->get();

            $special_offer = SpecialOffer::find($id);

            return view('special-offer.special-offer-edit',compact('special_offer'), $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'description'   =>  'required',
            'item_id'       =>  'required',
            'cemetery_id'   =>  'required',
            'status'        =>  'required',
            'to_earn'       =>  'required',
            'to_redeem'     =>  'required',
            'start_date'    =>  'required',
            'end_date'      =>  'required'
        ]);

        $special_offer      =   SpecialOffer::find($id);  
          
        $input              =   $request->all();

        $start_date         =   $input['start_date'];
        if ($start_date != null)
        $start_date         =   date('Y-m-d', strtotime($start_date));

        $end_date           =   $input['end_date'];
        if ($end_date != null)
        $end_date           =   date('Y-m-d', strtotime($end_date));

        if($special_offer->update($input)){
            return redirect()->route('special_offers.show',$id)
                ->with('success','Record Updated Successfully');
        }else{
            return redirect()->route('special_offers.index')
                ->with('error','Error');
        }
    }

    public function destroy($id)
    {
        $special_offer = SpecialOffer::find($id);

        $lp_invoices    =   DB::table('lp_invoices')->where('offer_id',$id)->get();

        if($lp_invoices->count()>0){
            return redirect()->route('special_offers.index')
                ->with('error', 'This Special Offer has been used in Transactions.');
        }else{
            $status = $special_offer->delete();

            if ($status) {
                return redirect()->route('special_offers.index')
                ->with('success','Record Deleted Successfully');
            }else{
                return redirect()->route('special_offers.index')
                    ->with('error','Error');
            }
        }
    }

    public function searchSpecialOffers(Request $request)
    {
        if(session()->get('read_special_offer')) {

            $search_value           =   $request->input('search_value');
            $data['search_value']   =   $search_value;

            $query = $query = SpecialOffer::select('special_offers.*', 'items.description AS item', 'items.code AS item_code', 'cemeteries.name AS cemetery')
                ->join('items', 'special_offers.item_id', '=', 'items.id')
                ->join('cemeteries', 'special_offers.cemetery_id', '=', 'cemeteries.id');

            if (isset($search_value) && $search_value != "")
                $query->orWhere('special_offers.code', '=', (int)$search_value )
                    ->orWhere('special_offers.description', '=', $search_value )
                    ->orWhere('items.code', '=', $search_value )
                    ->orWhere('items.description', '=', $search_value )
                    ->orWhere('cemeteries.name', '=', $search_value ); 

            $special_offers = $query->latest()->paginate(10)->appends(['search_value' => $search_value]);    

            return view('special-offer.special-offer-search2',compact('special_offers'), $data)
                ->with('i', (request()->input('page', 1) - 1) * 10);

        }else{

            return redirect()->route('home');

        }
    }

    ////////////////////////////////// Special Offer by Item /////////////////////////////////////////////////

    public function createSpecialOfferByItem($item)
    {
        if(session()->get('create_item')) {

            $itemDetails    =   Item::find($item);
            
            if($itemDetails->status == 0){

                $data['status_inactive']    =    "The item status is inactive. Please activate.";

            }

            $data['cemeteries']     =   Cemetery::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as cemetery_name"))->get();
            $data['items']          =   Item::select('id', 'code', DB::raw("CONCAT(code,' - ',description) as item_name"))->where('id', $item)->get();
            $data['fd_from_codes']  =   FuneralDirector::select('code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->orderBy('code', 'asc')->get();
            $data['fd_to_codes']    =   FuneralDirector::select('code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->orderBy('code', 'asc')->get();
            $data['item_id']        =   $item;

            return view('special-offer.special-offer-create-by-item', $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function storeSpecialOfferByItem(Request $request, $item)
    {
        request()->validate([
            'code'          =>  'required|unique:special_offers',
            'description'   =>  'required',
            'item_id'       =>  'required',
            'cemetery_id'   =>  'required',
            'status'        =>  'required',
            'fd_from_code'  =>  'required',
            'fd_to_code'    =>  'required|gte:fd_from_code',
            'to_earn'       =>  'required',
            'to_redeem'     =>  'required',
            'start_date'    =>  'required|date',
            'end_date'      =>  'required|date|after:start_date'
        ]);

        $input          =   $request->all();

        $id             =   "";
        $item_id        =   $input['item_id'];
        $cemetery_id    =   $input['cemetery_id'];
        $from_fd        =   $input['fd_from_code'];
        $to_fd          =   $input['fd_to_code'];

        $input['start_date']    =   date('Y-m-d', strtotime($input['start_date']));
        $start_date             =   $input['start_date'];

        $input['end_date']      =   date('Y-m-d', strtotime($input['end_date']));
        $end_date               =   $input['end_date'];

        if (!$this->specialOfferCompositeKeyValidation($id,$item_id,$cemetery_id,$from_fd,$to_fd,$start_date,$end_date)) {

            return redirect()->route('create_special_offer_by_item', array('item' => $item))
                    ->with('error','Duplicate Record.');

        }

        SpecialOffer::create($input);

        return redirect()->route('items.show',$item)
            ->with('success', 'Record Added Successfully.');
    }

    public function editSpecialOfferByItem($id, $item)
    {
        if(session()->get('update_item')) {

            $data['cemeteries']     =   Cemetery::select('id', 'code', DB::raw("CONCAT(code,' - ',name) as cemetery_name"))->get();
            $data['items']          =   Item::select('id', 'code', DB::raw("CONCAT(code,' - ',description) as item_name"))->where('id', $item)->get();
            $data['fd_from_codes']  =   FuneralDirector::select('code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->orderBy('code', 'asc')->get();
            $data['fd_to_codes']    =   FuneralDirector::select('code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->orderBy('code', 'asc')->get();
            $data['item_id']        =   $item;

            $special_offer = SpecialOffer::find($id);

            return view('special-offer.special-offer-edit-by-item',compact('special_offer'), $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function updateSpecialOfferByItem(Request $request, $id, $item)
    {
        request()->validate([
            'description'   =>  'required',
            'item_id'       =>  'required',
            'cemetery_id'   =>  'required',
            'status'        =>  'required',
            'fd_from_code'  =>  'required',
            'fd_to_code'    =>  'required|gte:fd_from_code',
            'to_earn'       =>  'required',
            'to_redeem'     =>  'required',
            'start_date'    =>  'required|date',
            'end_date'      =>  'required|date|after:start_date'
        ]);

        DB::beginTransaction();

        try {

            $special_offer  =   SpecialOffer::find($id);

            $input          =   $request->all();

            $item_id        =   $input['item_id'];
            $cemetery_id    =   $input['cemetery_id'];
            $from_fd        =   $input['fd_from_code'];
            $to_fd          =   $input['fd_to_code'];

            $start_date     =   $input['start_date'];
            if ($start_date != null) 
            $input['start_date']    =   date('Y-m-d', strtotime($start_date));

            $end_date       =   $input['end_date'];
            if ($end_date != null)  
            $input['end_date']      =   date('Y-m-d', strtotime($end_date));

            if($special_offer->updated_at != $request->updated_at){

                DB::rollback();

                return redirect()->route('edit_special_offer_by_item', array('id' => $id, 'item' => $item))
                    ->with('error','Record has been modified by another user. Please try again.');

            }

            if(strtotime($special_offer->start_date) > strtotime($request->start_date)){

                DB::rollback();

                return redirect()->route('edit_cemetery_item_by_item', array('id' => $id, 'item' => $item))
                    ->with('error','Cannot back date the Start date.');

            }

            if (!$this->specialOfferCompositeKeyValidation($id,$item_id,$cemetery_id,$from_fd,$to_fd,$input['start_date'],$input['end_date'])) {

                return redirect()->route('edit_special_offer_by_item', array('id' => $id, 'item' => $item))
                        ->with('error','Duplicate Record.');
    
            }

            // Update Special Offer Table
            $special_offer->update($input);

            DB::commit();

            return redirect()->route('items.show',$item)
                ->with('success','Record Updated Successfully');

        } catch (\Exception $e) {

            DB::rollback();

            return redirect()->route('edit_special_offer_by_item', array('id' => $id, 'item' => $item))
                ->with('error','Error');

        }    

    }

    public function destroySpecialOfferByItem($id, $item)
    {
        $special_offer = SpecialOffer::find($id);

        $lp_invoices    =   DB::table('lp_invoices')->where('offer_id',$id)->get();

        if($lp_invoices->count()>0){
            return redirect()->route('items.show',$item)
                ->with('error', 'This Special Offer has been used in Transactions.');
        }else{
            $status = $special_offer->delete();

            if ($status) {
                return redirect()->route('items.show',$item)
                ->with('success','Record Deleted Successfully');
            }else{
                return redirect()->route('items.show',$item)
                    ->with('error','Error');
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function specialOfferCompositeKeyValidation($id,$item_id,$cemetery_id,$from_fd,$to_fd,$start_date,$end_date)
    {
        $query  =   SpecialOffer::select('*');
        if ($id!="") $query->where('id', '!=', $id );
        $result =   $query->get();

        $a=0;
        foreach($result as $res){

            $sd     =   $result[$a]['start_date'];
            $ed     =   $result[$a]['end_date'];

            $ffd    =   $result[$a]['fd_from_code'];
            $tfd    =   $result[$a]['fd_to_code'];

            $start_date_validate    =   $this->dateRangeValidate($start_date,$sd,$ed);
            $end_date_validate      =   $this->dateRangeValidate($end_date,$sd,$ed);

            $from_fd_validate       =   $this->fdRangeValidate($from_fd,$ffd,$tfd);
            $to_fd_validate         =   $this->fdRangeValidate($to_fd,$ffd,$tfd);

            if(( ($result[$a]['item_id'] == $item_id) && ($result[$a]['cemetery_id'] == $cemetery_id) && ($start_date_validate||$end_date_validate) && ($from_fd_validate||$to_fd_validate) ))
            {
                return 0;
            }
            $a++;
        }
        return 1;
    }

    public function dateRangeValidate($date,$start_date,$end_date)
    {
        $ts         =   strtotime($date);
        $start_ts   =   strtotime($start_date);
        $end_ts     =   strtotime($end_date);

        if (($ts >= $start_ts) && ($ts <= $end_ts))
        {
            return 1;
        }
        else{
            return 0;
        }
    }

    public function fdRangeValidate($fd,$from_fd,$to_fd)
    {
        if (($fd >= $from_fd) && ($fd <= $to_fd))
        {
            return 1;
        }
        else{
            return 0;
        }
    }

}
