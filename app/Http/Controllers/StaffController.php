<?php

namespace App\Http\Controllers;

use App\FuneralDirector;
use App\Cemetery;
use App\CemeteryItem;
use App\StaffSearchHistory;
use App\Helpers\LpQueries;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StaffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewStaffWelcomePage()
    {
        $user_id                        =   Auth::user()->id;
        $fd_id                          =   Auth::user()->fd_id;
        $cemetery_id                    =   Auth::user()->default_cemetery;

        $data['fd_id']                  =   $fd_id;
        $data['cemetery_id']            =   $cemetery_id;
                  
        if ( !empty ( $fd_id ) ) {
            $data['funeral_directors']  =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->where("parent_id", $fd_id)->orWhere("id", $fd_id)->get();
        }else{
            $data['funeral_directors']  =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->get();
        }

        $data['cemeteries']             =   Cemetery::all();

        if ( !empty ( $cemetery_id ) ) {
            $data['cemetery_items']     =   CemeteryItem::select('cemetery_items.item_id','i.description AS item',
                                            DB::raw("CONCAT(i.code,' - ',i.description) as item_name"))
                                            ->join('items AS i', 'cemetery_items.item_id', '=', 'i.id')
                                            ->where('cemetery_items.cemetery_id', $cemetery_id)
                                            ->get();
        }else{
            $data['cemetery_items']     =   CemeteryItem::select('cemetery_items.item_id', 'i.description AS item',
                                            DB::raw("CONCAT(i.code,' - ',i.description) as item_name"))
                                            ->join('items AS i', 'cemetery_items.item_id', '=', 'i.id')
                                            ->get();
        }
        
        $posts                          =   StaffSearchHistory::select(
                                            'staff_search_history.fd_id AS fdID', 
                                            'funeral_directors.name AS fdName',
                                            'funeral_directors.code AS fdCode')
                                            ->join('funeral_directors', 'staff_search_history.fd_id', '=', 'funeral_directors.id')
                                            ->where('staff_search_history.user_id', $user_id)
                                            ->orderBy('staff_search_history.searched_date', 'DESC')
                                            ->limit(10)
                                            ->get();

        $posts->map(function ($posts) {
            $lpquery = new LpQueries();
            $posts['available_ld']  =   $lpquery->getAvailableDollars($posts->fdID);
            return $posts;
        });

        $data['fd_search_history_logs']     =   $posts;

        $data['item_search_history_logs']   =   StaffSearchHistory::select(
                                                'staff_search_history.item_id AS itemID',
                                                'items.description AS itemName',
                                                'items.code AS itemCode',
                                                'staff_search_history.cemetery_id AS cemID',
                                                'cemeteries.name AS cemName',
                                                'cemetery_items.to_redeem AS toRedeem')
                                                ->join('cemetery_items', 'staff_search_history.item_id', '=', 'cemetery_items.item_id')
                                                ->join('items', 'staff_search_history.item_id', '=', 'items.id')
                                                ->join('cemeteries', 'staff_search_history.cemetery_id', '=', 'cemeteries.id')
                                                ->where('staff_search_history.user_id', $user_id)
                                                ->orderBy('staff_search_history.searched_date', 'DESC')
                                                ->limit(10)
                                                ->get();

        return view('staff.staff-dashboard',  $data);
    }

    public function getPointsByCemeteryItem($fd,$type,$cemetery_id,$item_id)
    {
        $lpquery = new LpQueries();

        $data['available_loyalty_dollars']  =   $lpquery->getAvailableDollars($fd);

        $data['fd_details']  =   FuneralDirector::where('id', $fd)->select('is_on_hold')->get();

        $query =    DB::table('cemetery_items_full_view AS c')
                    ->leftJoin('special_offers AS s', function($join){
                        $join->on('c.item_id', '=', 's.item_id')
                            ->on('c.cemetery_id', '=', 's.cemetery_id');
                    })
                    ->select('c.to_earn AS ci_to_earn','c.to_redeem AS ci_to_redeem', 's.to_earn AS so_to_earn', 's.to_redeem AS so_to_redeem')
                    ->where('c.status', TRUE);
                    
                    if (isset($cemetery_id) && $cemetery_id != "0")
                    $query->where('c.cemetery_id', $cemetery_id);

                    if (isset($item_id) && $item_id != "0")
                    $query->where('c.item_id', $item_id);

        $data['results']   =    $query->first();

        $search_log                 =   new StaffSearchHistory();
        $search_log->user_id        =   Auth::user()->id;
        $search_log->search_type    =   'search';
        $search_log->fd_id          =   $fd;
        $search_log->cemetery_id    =   $cemetery_id;
        $search_log->item_id        =   $item_id;
        $search_log->searched_date  =   date("Y-m-d H:i:s");
        $search_log->save();

        return json_encode($data);
    }

    public function getItemsByCemetery($cemetery)
    {
        $items  =   CemeteryItem::join('items AS i', 'cemetery_items.item_id', '=', 'i.id')
                    ->where("cemetery_items.cemetery_id", $cemetery)
                    ->select(DB::raw("CONCAT(i.code,' - ',i.description) as item"),'cemetery_items.item_id')
                    ->get();

        return json_encode($items);
    }
}
