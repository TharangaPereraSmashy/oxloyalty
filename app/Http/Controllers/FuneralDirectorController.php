<?php

namespace App\Http\Controllers;

use App\CemeteryItem;
use App\FuneralDirector;
use App\Country;
use App\SpecialOffer;
use App\State;
use App\LoyaltyDollarRequest;
use App\RevaluationRequest;

use App\Helpers\LpQueries;
use App\Helpers\ProcessUtil;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cemetery;

class FuneralDirectorController extends Controller
{
    
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(session()->get('read_funeral_director')) {

            $funeral_directors  =   FuneralDirector::leftJoin('funeral_directors as parent_fds', 'funeral_directors.parent_id', '=', 'parent_fds.id')
                                    ->select('funeral_directors.*','parent_fds.code as parent_fd_code')                        
                                    ->orderBy('funeral_directors.code', 'desc')->paginate(10);

            return view('funeral-director.funeral-director-search', compact('funeral_directors'))
                ->with('i', (request()->input('page', 1) - 1) * 10);

        }else{

            return redirect()->route('home');

        }
    }

    public function create()
    {
        if(session()->get('create_funeral_director')) {

            $data['countries']  =   Country::all();
            $data['states']     =   State::all();
            $data['parents']    =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->where("is_parent", 1)->orderBy('code', 'asc')->get();

            return view('funeral-director.funeral-director-create',$data);

        }else{

            return redirect()->route('home');

        }
    }

    public function store(Request $request)
    {
        request()->validate([
            'code'              =>  'required|unique:funeral_directors',
            'name'              =>  'required',
            'email'             =>  'nullable|email',
            'gl_liability_code' =>  'required',
            'gl_cost_code'      =>  'required',
            'logo'              =>  'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        if($request->hasfile('logo')){
            $file           =   $request->file('logo');
            $fd_logo_name   =   'FD_LOGO_'.uniqid().time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path().'/images/funeral_director_logos/', $fd_logo_name);
        }else{
            $fd_logo_name   =   "";
        }

        if($request->get('is_parent') == 1){
            $parent_id  =   null;
        }elseif($request->get('is_parent') == 0){
            $parent_id  =   $request->get('parent_id');
        }

        $funeral_director                       =   new FuneralDirector();
        $funeral_director->code                 =   $request->get('code');
        $funeral_director->name                 =   $request->get('name');
        $funeral_director->address1             =   $request->get('address1');
        $funeral_director->address2             =   $request->get('address2');
        $funeral_director->suburb               =   $request->get('suburb');
        $funeral_director->country_id           =   $request->get('country_id');
        $funeral_director->state_id             =   $request->get('state_id');
        $funeral_director->postcode             =   $request->get('postcode');
        $funeral_director->parent_id            =   $parent_id;
        $funeral_director->is_parent            =   $request->get('is_parent');
        $funeral_director->phone                =   $request->get('phone');
        $funeral_director->email                =   $request->get('email');
        $funeral_director->contact_person_name  =   $request->get('contact_person_name');
        $funeral_director->is_on_hold           =   $request->get('is_on_hold');
        $funeral_director->gl_liability_code    =   $request->get('gl_liability_code');
        $funeral_director->gl_cost_code         =   $request->get('gl_cost_code');
        $funeral_director->logo                 =   $fd_logo_name;
        $funeral_director->save();

        return redirect()->route('funeral_directors.index')
            ->with('success', 'Record Added Successfully.');
    }

    public function show($id)
    {
        $loggedin_fd            =   Auth::user()->fd_id;
        $loggedin_fd_children   =   FuneralDirector::select('id')->where("parent_id", $loggedin_fd)->get()->toArray();
        
        $child_list =   [];

        foreach($loggedin_fd_children as $loggedin_fd_child){
            $child_list[] = implode(',', $loggedin_fd_child);
        }

        if($loggedin_fd=="" || $loggedin_fd==$id || in_array($id, $child_list)){

            $funeral_director   =   FuneralDirector::select('funeral_directors.*','countries.country_name','states.state_name')
                                    ->leftJoin('countries', 'funeral_directors.country_id', '=', 'countries.id')
                                    ->leftJoin('states', 'funeral_directors.state_id', '=', 'states.id')
                                    ->where('funeral_directors.id','=',$id)->first();
            
            $sql1 = "SELECT
            c.item_id AS ci_item_id,
            c.cemetery_id AS ci_cemetery_id,
            c.to_earn AS ci_to_earn,
            c.to_redeem AS ci_to_redeem,
            s.item_id AS so_item_id,
            s.cemetery_id AS so_cemetery_id,
            s.end_date AS expiry_date,
            s.to_earn AS so_to_earn,
            s.to_redeem AS so_to_redeem,
            i.description AS item,
            ce.name AS cemetery
            FROM cemetery_items_full_view c
            INNER JOIN items i
            ON i.id = c.item_id
            INNER JOIN cemeteries ce
            ON ce.id = c.cemetery_id
            LEFT JOIN special_offers s
            ON s.item_id = c.item_id
            AND s.cemetery_id = c.cemetery_id
            AND s.fd_from_code <= '$funeral_director->code'
            AND s.fd_to_code >= '$funeral_director->code'";

            $data['item_loyalty_details']   =   DB::select($sql1);

            $sql2 = "SELECT
            s.item_id AS so_item_id,
            s.cemetery_id AS so_cemetery_id,
            s.end_date AS expiry_date,
            s.to_earn AS so_to_earn,
            s.to_redeem AS so_to_redeem,
            i.description AS item,
            ce.name AS cemetery
            FROM special_offers s
            INNER JOIN items i
            ON i.id = s.item_id
            INNER JOIN cemeteries ce
            ON ce.id = s.cemetery_id
            WHERE s.fd_from_code <= '$funeral_director->code'
            AND s.fd_to_code >= '$funeral_director->code'";

            $data['special_offer_details']   =   DB::select($sql2);

            //$data['earned_loyalty_dollars']     =   DB::table('lp_earned_points')->select(DB::raw("coalesce(sum(points_balance),0) as earned"))->where('fd_id', '=', $id)->where('is_expired', '=', 'FALSE')->where('points_balance', '>', 0)->get();
            
            //$data['consumed_loyalty_dollars']   =   DB::table('lp_consumed_points')->select(DB::raw("coalesce(sum(points_consumed),0) as consumed"))->where('fd_id', '=', $id)->where('points_consumed', '>', 0)->get();

            //$data['expired_loyalty_dollars']    =   DB::table('lp_earned_points')->select(DB::raw("coalesce(sum(points_balance),0) as expired"))->where('fd_id', '=', $id)->where('is_expired', '=', 'TRUE')->where('points_balance', '>', 0)->get();
            
            //$data['available_loyalty_dollars']  =   ((int)$data['earned_loyalty_dollars'][0]->earned - (int)$data['consumed_loyalty_dollars'][0]->consumed);

            $lpquery = new LpQueries();

            //$data['country']    =   Country::where("id", $funeral_director->country_id)->select('country_name')->first();
            //$data['state']      =   State::where("id", $funeral_director->state_id)->select('state_name')->first();

            if($funeral_director->is_parent == 1){

                $data['earned_loyalty_dollars']     =   $lpquery->getParentEarnedDollars($id);
                
                $data['expired_loyalty_dollars']    =   $lpquery->getParentToBeExpiredDollars($id);
                $data['available_loyalty_dollars']  =   $lpquery->getParentAvailableDollars($id);

                $data['from_funeral_directors']     =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->where("parent_id", $id)->orderBy('code', 'asc')->get();
                $data['to_funeral_directors']       =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->where("parent_id", $id)->orderBy('code', 'asc')->get();
                $data['childFDs']                   =   DB::table('funeral_director_balances_view')->select('id','code', 'name', 'phone', 'email', 'available_points')->where("parent_id", $id)->orderBy('code', 'asc')->get();
            
            }elseif($funeral_director->is_parent == 0){

                $data['earned_loyalty_dollars']     =   $lpquery->getEarnedDollars($id);
             
                $data['expired_loyalty_dollars']    =   $lpquery->getToBeExpiredDollars($id);
                $data['available_loyalty_dollars']  =   $lpquery->getAvailableDollars($id);

                $data['from_funeral_directors']     =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->where("id", $id)->orderBy('code', 'asc')->get();
                $data['to_funeral_directors']       =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->where("parent_id", $funeral_director->parent_id)->where("id", '<>', $id)->orderBy('code', 'asc')->get();
                
                $parent_id = $funeral_director->parent_id;
                if (is_null($parent_id)) {
                    $parent_code    =   "";
                }else{
                    $parent         =   FuneralDirector::find($funeral_director->parent_id);
                    $parent_code    =   $parent->code;
                }
                $sql3 = "SELECT
                s.item_id AS so_item_id,
                s.cemetery_id AS so_cemetery_id,
                s.end_date AS expiry_date,
                s.to_earn AS so_to_earn,
                s.to_redeem AS so_to_redeem,
                i.description AS item,
                ce.name AS cemetery
                FROM special_offers s
                INNER JOIN items i
                ON i.id = s.item_id
                INNER JOIN cemeteries ce
                ON ce.id = s.cemetery_id
                WHERE s.fd_from_code <= '$parent_code'
                AND s.fd_to_code >= '$parent_code'";

                $data['parent_special_offer_details']   =   DB::select($sql3);
            }

            return view('funeral-director.funeral-director-dashboard',compact('funeral_director'), $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function edit($id)
    {
        if(session()->get('update_funeral_director')) {

            $funeral_director   =   FuneralDirector::find($id);

            $data['countries']  =   Country::all();
            $data['states']     =   State::where("country_id", $funeral_director->country_id)->get();
            $data['parents']    =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->where("is_parent", 1)->where("id","!=",$id)->orderBy('code', 'asc')->get();

            return view('funeral-director.funeral-director-edit',compact('funeral_director'),$data);

        }else{

            return redirect()->route('home');

        }
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'name'              =>  'required',
            'email'             =>  'nullable|email',
            'gl_liability_code' =>  'required',
            'gl_cost_code'      =>  'required',
            'logo'              =>  'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        DB::beginTransaction();

        try {

            $funeral_director   =   FuneralDirector::find($id);

            if($funeral_director->updated_at != $request->updated_at){

                DB::rollback();

                return redirect()->route('funeral_directors.edit', $id)
                    ->with('error','Record has been modified by another user. Please try again.');

            }

            /* if( ($funeral_director->is_parent != $request->is_parent) && $request->is_parent == 0 && (!isset($request->parent_id) || trim($request->parent_id) === '')){

                DB::rollback();

                return redirect()->route('funeral_directors.edit', $id)
                    ->with('error','Please select a Parent FD.');

            } */

            if($request->hasfile('logo')){
                $file           =   $request->file('logo');
                $fd_logo_name   =   'FD_LOGO_'.uniqid().time().'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/images/funeral_director_logos/', $fd_logo_name);
            }else{
                $fd_logo_name   =   "";
            }
    
            /* if($request->get('is_parent') == 1){
                $parent_id  =   null;
            }elseif($request->get('is_parent') == 0){
                $parent_id  =   $request->get('parent_id');
            } */

            // Update Funeral Director Table
            $funeral_director->name                 =   $request->get('name');
            $funeral_director->address1             =   $request->get('address1');
            $funeral_director->address2             =   $request->get('address2');
            $funeral_director->suburb               =   $request->get('suburb');
            $funeral_director->country_id           =   $request->get('country_id');
            $funeral_director->state_id             =   $request->get('state_id');
            $funeral_director->postcode             =   $request->get('postcode');
            $funeral_director->parent_id            =   $request->get('parent_id');
            $funeral_director->is_parent            =   $request->get('is_parent');
            $funeral_director->phone                =   $request->get('phone');
            $funeral_director->email                =   $request->get('email');
            $funeral_director->contact_person_name  =   $request->get('contact_person_name');
            $funeral_director->is_on_hold           =   $request->get('is_on_hold');
            $funeral_director->gl_liability_code    =   $request->get('gl_liability_code');
            $funeral_director->gl_cost_code         =   $request->get('gl_cost_code');
            $funeral_director->logo                 =   $fd_logo_name;
            $funeral_director->save();

            DB::commit();

            return redirect()->route('funeral_directors.show', $id)
                ->with('success','Record Updated Successfully');

        } catch (\Exception $e) {
            dd($e->getMessage());

            DB::rollback();

            return redirect()->route('funeral_directors.edit', $id)
                ->with('error','Error');

        }
        
    }

    public function destroy($id)
    {
        $funeral_director       =   FuneralDirector::find($id);

        $transactions_history   =   DB::table('transactions_history')->where('fd_id',$id)->get();
        $lp_earned_points       =   DB::table('lp_earned_points')->where('fd_id',$id)->get();
        $lp_consumed_points     =   DB::table('lp_consumed_points')->where('fd_id',$id)->get();
        $lp_invoices            =   DB::table('lp_invoices')->where('funeral_director',$funeral_director->code)->get();

        if($funeral_director->users()->exists()){
            return redirect()->route('funeral_directors.index')
                ->with('error', 'This Funeral Director is used in Users.');
        }else if($funeral_director->children()->exists()){
            return redirect()->route('funeral_directors.index')
                ->with('error', 'This Funeral Director has Child FD Accounts defined.');
        }else if($transactions_history->count()>0){
            return redirect()->route('funeral_directors.index')
                ->with('error', 'This Funeral Director has been used in Transactions.');
        }else if($lp_earned_points->count()>0){
            return redirect()->route('funeral_directors.index')
                ->with('error', 'This Funeral Director has been used in Transactions.');
        }else if($lp_consumed_points->count()>0){
            return redirect()->route('funeral_directors.index')
                ->with('error', 'This Funeral Director has been used in Transactions.');
        }else if($lp_invoices->count()>0){
            return redirect()->route('funeral_directors.index')
                ->with('error', 'This Funeral Director has been used in Transactions.');
        }else{
            $fd_logo_path = public_path().'/images/funeral_director_logos/'.$funeral_director->logo;
            if(file_exists($fd_logo_path)) {
                unlink($fd_logo_path);
            }

            $status = $funeral_director->delete();
            if ($status) {
                return redirect()->route('funeral_directors.index')
                ->with('success','Record Deleted Successfully');
            }else{
                return redirect()->route('funeral_directors.index')
                    ->with('error','Error');
            }
        }
    }

    public function searchFuneralDirectors(Request $request)
    {
        if(session()->get('read_funeral_director')) {

            $search_value           =   $request->input('search_value');
            $data['search_value']   =   $search_value;

            $query  =   FuneralDirector::leftJoin('funeral_directors as parent_fds', 'funeral_directors.parent_id', '=', 'parent_fds.id')
                        ->select('funeral_directors.*','parent_fds.code as parent_fd_code');

            if (isset($search_value) && $search_value != "")
                $query->orWhere(DB::raw("CAST(funeral_directors.code AS text)"), 'LIKE', '%'.strtolower($search_value).'%')
                    ->orWhere(DB::raw("LOWER(funeral_directors.name)"), 'LIKE', '%'.strtolower($search_value).'%')
                    ->orWhere(DB::raw("LOWER(funeral_directors.phone)"), 'LIKE', '%'.strtolower($search_value).'%')
                    ->orWhere(DB::raw("LOWER(funeral_directors.email)"), 'LIKE', '%'.strtolower($search_value).'%'); 

            $funeral_directors = $query->orderBy('funeral_directors.code', 'desc')->paginate(10)->appends(['search_value' => $search_value]);    

            return view('funeral-director.funeral-director-search2',compact('funeral_directors'), $data)
                ->with('i', (request()->input('page', 1) - 1) * 10);

        }else{

            return redirect()->route('home');

        }
    }

    public function setLoyaltyDollarAdjustment(Request $request)
    {

        request()->validate([
            'adjustment_amount'   =>  'required'
        ]);

        $fd_id              =   $request->get('fd_id');
        $adjustment_amount  =   $request->get('adjustment_amount');

        $default_cemetery = Cemetery::select("id","code")->where("is_default_cemetery", TRUE);
            
        if($default_cemetery->count()==0){

            return redirect()->route('funeral_directors.show',$fd_id)
                ->with('error','No Default Cemetery Available');

        }

        try {

            $processutil = new ProcessUtil();

            $processutil->processAdjustment($fd_id, $adjustment_amount);

            return redirect()->route('funeral_directors.show',$fd_id)
                ->with('success','L$ Adjusted');

        } catch (\Exception $e) {

            return redirect()->route('funeral_directors.show',$fd_id)
                ->with('error','Error');

        }    

    }

    public function createLoyaltyDollarTransferRequest(Request $request)
    {
       request()->validate([
            'transfer_amount'   =>  'lte:from_fd_ld_balance'
        ]);

        $ld_request                     =   new LoyaltyDollarRequest();
        $ld_request->user_id            =   Auth::user()->id;
        $ld_request->from_fd_id         =   $request->get('from_fd_id');
        $ld_request->to_fd_id           =   $request->get('to_fd_id');
        $ld_request->transfer_amount    =   $request->get('transfer_amount');
        $ld_request->status             =   "unapproved";
        $ld_request->requested_date     =   date("Y-m-d H:i:s");
        $ld_request->save();

        return redirect()->action('FuneralDirectorController@searchLoyaltyDollarTransferRequests')
            ->with('success','Request Created Successfully');
    }


    public function rejectLoyaltyDollarTransferRequest($id)
    {
        $ld_request                     =   LoyaltyDollarRequest::find($id);
        $ld_request->status             =   "rejected";
        $ld_request->processed_by       =   Auth::user()->id;
        $ld_request->processed_date     =   date("Y-m-d H:i:s");
        $ld_request->save();

        return redirect()->action('FuneralDirectorController@searchLoyaltyDollarTransferRequests');
    }

    public function processLoyaltyDollarTransferRequest(Request $request, $id)
    {
        
    }

    public function approveLoyaltyDollarTransferRequest(Request $request, $id)
    {
       
        $ld_request                 =   LoyaltyDollarRequest::find($id);

        $from_fd_id                 =   $ld_request->from_fd_id;
        $to_fd_id                   =   $ld_request->to_fd_id;
        $transfer_amount            =   $ld_request->transfer_amount;

        $from_fd                    =   FuneralDirector::find($from_fd_id);
        $to_fd                      =   FuneralDirector::find($to_fd_id);

        $lpquery = new LpQueries();
        $from_fd_ld_balance   =   $lpquery->getAvailableDollars($from_fd_id );
                
        $current_from_fd_timestamp  =   $from_fd->updated_at;
        $current_to_fd_timestamp    =   $to_fd->updated_at;
        $old_from_fd_timestamp      =   $request->get('from_fd_timestamp');
        $old_to_fd_timestamp        =   $request->get('to_fd_timestamp');
        
        if($current_from_fd_timestamp != $old_from_fd_timestamp){

            return redirect()->action('FuneralDirectorController@searchLoyaltyDollarTransferRequests')
                ->with('error','Record has been modified by another user. Please try again.');

        }elseif($current_to_fd_timestamp != $old_to_fd_timestamp){

            return redirect()->action('FuneralDirectorController@searchLoyaltyDollarTransferRequests')
                ->with('error','Record has been modified by another user. Please try again.');

        }

        if($transfer_amount > $from_fd_ld_balance){

            return redirect()->action('FuneralDirectorController@searchLoyaltyDollarTransferRequests')
                ->with('error','No LD Balance Available');

        }

        $default_cemetery = Cemetery::select("id","code")->where("is_default_cemetery", TRUE);
            
        if($default_cemetery->count()==0){

            return redirect()->action('FuneralDirectorController@searchLoyaltyDollarTransferRequests')
                ->with('error','No Default Cemetery Available');

        }
       
        $processutil = new ProcessUtil();

        $processutil->processTransfer($id);

        return redirect()->action('FuneralDirectorController@searchLoyaltyDollarTransferRequests')
                ->with('success','Record Updated Successfully');
    }

    public function getLdBalanceByFdId($fd)
    {
        $lpquery = new LpQueries();

        $loyalty_dollar_balance = $lpquery->getAvailableDollars($fd);

        return json_encode($loyalty_dollar_balance);
    }

    public function searchLoyaltyDollarTransferRequests(Request $request)
    {
        if(session()->get('read_ld_transfer_request')) {

            $data['from_funeral_directors'] =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->orderBy('code', 'asc')->get();
            $data['to_funeral_directors']   =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->orderBy('code', 'asc')->get();

            $from_fd_id =   $request->get('from_fd_id');
            $to_fd_id   =   $request->get('to_fd_id');
            $status     =   $request->get('status');

            $query  =   LoyaltyDollarRequest::select('loyalty_dollar_requests.*', DB::raw("CONCAT(fd1.code,' - ',fd1.name,'') as fd1_name"), DB::raw("CONCAT(fd2.code,' - ',fd2.name,'') as fd2_name"), 'fd1.updated_at AS from_fd_timestamp', 'fd2.updated_at AS to_fd_timestamp')
                ->join('funeral_directors AS fd1', 'loyalty_dollar_requests.from_fd_id', '=', 'fd1.id')
                ->join('funeral_directors AS fd2', 'loyalty_dollar_requests.to_fd_id', '=', 'fd2.id');

            if (isset($from_fd_id) && $from_fd_id != "")
                $query->where('loyalty_dollar_requests.from_fd_id', '=', $from_fd_id );

            if (isset($to_fd_id) && $to_fd_id != "")
                $query->where('loyalty_dollar_requests.to_fd_id', '=', $to_fd_id);

            if (isset($status) && $status != "")
                $query->where('loyalty_dollar_requests.status', '=', $status);

            $ld_transfer_requests   =   $query->latest()->paginate(10)->appends(['from_fd_id' => $from_fd_id, 'to_fd_id' => $to_fd_id, 'status' => $status]);

            return view('funeral-director.loyalty-dollar-transfer-request-search',compact('ld_transfer_requests'),$data)
                ->with('i', (request()->input('page', 1) - 1) * 10);

        }else{

            return redirect()->route('home');

        }
    }

    public function loyaltyDollarRevaluationRequests(Request $request)
    {
        if(session()->get('read_ld_revaluation_request')) {

            $data['current_rate']       =   RevaluationRequest::select('new_rate')->where('is_processed', 1)->orderBy('processed_date', 'desc')->first();
            
            $ld_revaluation_requests    =   RevaluationRequest::select('revaluation_requests.*', 'users.name AS user_name')
                                                ->join('users', 'revaluation_requests.requested_by', '=', 'users.id')
                                                ->orderBy('revaluation_requests.requested_date', 'desc')->get();

            return view('funeral-director.loyalty-dollar-revaluation-requests',compact('ld_revaluation_requests'),$data);

        }else{

            return redirect()->route('home');

        }
    }

    public function createLoyaltyDollarRevaluationRequest(Request $request)
    {
        if(session()->get('create_ld_revaluation_request')) {

            if(RevaluationRequest::where('is_processed', 0)->count() > 0){
                return redirect()->action('FuneralDirectorController@loyaltyDollarRevaluationRequests')
                    ->with('error','Unprocessed request Already Exists');
            }

            request()->validate([
                'new_rate'   =>  'required'
            ]);

            $ld_revaluation_request                 =   new RevaluationRequest();
            $ld_revaluation_request->requested_date =   date("Y-m-d H:i:s");
            $ld_revaluation_request->requested_by   =   Auth::user()->id;
            $ld_revaluation_request->new_rate       =   $request->get('new_rate');
            $ld_revaluation_request->save();

            return redirect()->action('FuneralDirectorController@loyaltyDollarRevaluationRequests')
                ->with('success','Request Created Successfully');

        }else{

            return redirect()->route('home');

        }
    }

    public function updateLoyaltyDollarRevaluationRequest(Request $request, $id)
    {
        if(session()->get('update_ld_revaluation_request')) {

            request()->validate([
                'updated_rate'   =>  'required'
            ]);

            $ld_revaluation_request                 =   RevaluationRequest::find($id);
            $ld_revaluation_request->requested_date =   date("Y-m-d H:i:s");
            $ld_revaluation_request->requested_by   =   Auth::user()->id;
            $ld_revaluation_request->new_rate       =   $request->get('updated_rate');
            $ld_revaluation_request->save();

            return redirect()->action('FuneralDirectorController@loyaltyDollarRevaluationRequests')
                ->with('success','Request Updated Successfully');
                
        }else{

            return redirect()->route('home');

        }
    }

    public function deleteLoyaltyDollarRevaluationRequest($id)
    {
        if(session()->get('delete_ld_revaluation_request')) {

            $ld_revaluation_request                 =   RevaluationRequest::find($id);
            $ld_revaluation_request->delete();

            return redirect()->action('FuneralDirectorController@loyaltyDollarRevaluationRequests')
                ->with('success','Request Deleted Successfully');
                
        }else{

            return redirect()->route('home');

        }
    }

    /* public function viewFuneralDirectorWelcomePage()
    {
        $fd_id              =   Auth::user()->fd_id;
        $funeral_director   =   FuneralDirector::find($fd_id);

        $earned_ld          =   DB::table('lp_earned_points')->select(DB::raw("coalesce(sum(points_balance),0) as earned"))->where('fd_id', '=', $fd_id)->where('is_expired', '=', 'FALSE')->where('points_balance', '>', 0)->get();
            
        $consumed_ld        =   DB::table('lp_consumed_points')->select(DB::raw("coalesce(sum(points_consumed),0) as consumed"))->where('fd_id', '=', $fd_id)->where('points_consumed', '>', 0)->get();

        $data['available_loyalty_dollars']  =   ((int)$earned_ld[0]->earned - (int)$consumed_ld[0]->consumed);
        
        $data['results']   =   DB::select( 'SELECT 
        c.item_id AS ci_item_id, 
        c.cemetery_id AS ci_cemetery_id,
        c.to_earn AS ci_to_earn, 
        c.to_redeem AS ci_to_redeem, 
        s.item_id AS so_item_id, 
        s.cemetery_id AS so_cemetery_id, 
        s.end_date AS expiry_date, 
        s.to_earn AS so_to_earn, 
        s.to_redeem AS so_to_redeem,
        i.description AS item,
        ce.name AS cemetery
        FROM cemetery_items_full_view c 
        INNER JOIN items i 
        ON i.id = c.item_id
        INNER JOIN cemeteries ce 
        ON ce.id = c.cemetery_id
        LEFT JOIN special_offers s
        ON s.item_id = c.item_id
        AND s.cemetery_id = c.cemetery_id' );

        return view('funeral-director.funeral-director-welcome',compact('funeral_director'), $data);
    } */

    public function viewFuneralDirectorWelcomePage(){

        $id =   Auth::user()->fd_id;

        $funeral_director   =   FuneralDirector::select('funeral_directors.*','countries.country_name','states.state_name')
                                ->leftJoin('countries', 'funeral_directors.country_id', '=', 'countries.id')
                                ->leftJoin('states', 'funeral_directors.state_id', '=', 'states.id')
                                ->where('funeral_directors.id','=',$id)->first();

        $sql1 = "SELECT
        c.item_id AS ci_item_id,
        c.cemetery_id AS ci_cemetery_id,
        c.to_earn AS ci_to_earn,
        c.to_redeem AS ci_to_redeem,
        s.item_id AS so_item_id,
        s.cemetery_id AS so_cemetery_id,
        s.end_date AS expiry_date,
        s.to_earn AS so_to_earn,
        s.to_redeem AS so_to_redeem,
        i.description AS item,
        ce.name AS cemetery
        FROM cemetery_items_full_view c
        INNER JOIN items i
        ON i.id = c.item_id
        INNER JOIN cemeteries ce
        ON ce.id = c.cemetery_id
        LEFT JOIN special_offers s
        ON s.item_id = c.item_id
        AND s.cemetery_id = c.cemetery_id
        AND s.fd_from_code <= '$funeral_director->code'
        AND s.fd_to_code >= '$funeral_director->code'";

        $data['item_loyalty_details']   =   DB::select($sql1);

        $sql2 = "SELECT
        s.item_id AS so_item_id,
        s.cemetery_id AS so_cemetery_id,
        s.end_date AS expiry_date,
        s.to_earn AS so_to_earn,
        s.to_redeem AS so_to_redeem,
        i.description AS item,
        ce.name AS cemetery
        FROM special_offers s
        INNER JOIN items i
        ON i.id = s.item_id
        INNER JOIN cemeteries ce
        ON ce.id = s.cemetery_id
        WHERE s.fd_from_code <= '$funeral_director->code'
        AND s.fd_to_code >= '$funeral_director->code'";

        $data['special_offer_details']   =   DB::select($sql2);

        //$data['earned_loyalty_dollars']     =   DB::table('lp_earned_points')->select(DB::raw("coalesce(sum(points_balance),0) as earned"))->where('fd_id', '=', $id)->where('is_expired', '=', 'FALSE')->where('points_balance', '>', 0)->get();
        
        //$data['consumed_loyalty_dollars']   =   DB::table('lp_consumed_points')->select(DB::raw("coalesce(sum(points_consumed),0) as consumed"))->where('fd_id', '=', $id)->where('points_consumed', '>', 0)->get();

        //$data['expired_loyalty_dollars']    =   DB::table('lp_earned_points')->select(DB::raw("coalesce(sum(points_balance),0) as expired"))->where('fd_id', '=', $id)->where('is_expired', '=', 'TRUE')->where('points_balance', '>', 0)->get();
        
        //$data['available_loyalty_dollars']  =   ((int)$data['earned_loyalty_dollars'][0]->earned - (int)$data['consumed_loyalty_dollars'][0]->consumed);

        $lpquery = new LpQueries();

        //$data['country']    =   Country::where("id", $funeral_director->country_id)->select('country_name')->first();
        //$data['state']      =   State::where("id", $funeral_director->state_id)->select('state_name')->first();

        if($funeral_director->is_parent == 1){

            $data['earned_loyalty_dollars']     =   $lpquery->getParentEarnedDollars($id);
            
            $data['expired_loyalty_dollars']    =   $lpquery->getParentToBeExpiredDollars($id);
            $data['available_loyalty_dollars']  =   $lpquery->getParentAvailableDollars($id);

            $data['from_funeral_directors']     =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->where("parent_id", $id)->orderBy('code', 'asc')->get();
            $data['to_funeral_directors']       =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->where("parent_id", $id)->orderBy('code', 'asc')->get();
            $data['childFDs']                   =   DB::table('funeral_director_balances_view')->select('id','code', 'name', 'phone', 'email', 'available_points')->where("parent_id", $id)->orderBy('code', 'asc')->get();
        
        }elseif($funeral_director->is_parent == 0){

            $data['earned_loyalty_dollars']     =   $lpquery->getEarnedDollars($id);
            
            $data['expired_loyalty_dollars']    =   $lpquery->getToBeExpiredDollars($id);
            $data['available_loyalty_dollars']  =   $lpquery->getAvailableDollars($id);

            $data['from_funeral_directors']     =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->where("id", $id)->orderBy('code', 'asc')->get();
            $data['to_funeral_directors']       =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as fd_name"))->where("parent_id", $funeral_director->parent_id)->where("id", '<>', $id)->orderBy('code', 'asc')->get();

            $parent_id = $funeral_director->parent_id;
            if (is_null($parent_id)) {
                $parent_code    =   "";
            }else{
                $parent         =   FuneralDirector::find($funeral_director->parent_id);
                $parent_code    =   $parent->code;
            }
            $sql3 = "SELECT
            s.item_id AS so_item_id,
            s.cemetery_id AS so_cemetery_id,
            s.end_date AS expiry_date,
            s.to_earn AS so_to_earn,
            s.to_redeem AS so_to_redeem,
            i.description AS item,
            ce.name AS cemetery
            FROM special_offers s
            INNER JOIN items i
            ON i.id = s.item_id
            INNER JOIN cemeteries ce
            ON ce.id = s.cemetery_id
            WHERE s.fd_from_code <= '$parent_code'
            AND s.fd_to_code >= '$parent_code'";

            $data['parent_special_offer_details']   =   DB::select($sql3);
        }

        return view('funeral-director.funeral-director-dashboard',compact('funeral_director'), $data);

    }

}
