<?php

namespace App\Http\Controllers;

use App\Cemetery;
use App\Country;
use App\State;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CemeteryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(session()->get('read_cemetery')) {

            $cemeteries = Cemetery::latest()->paginate(10);

            return view('cemetery.cemetery-search', compact('cemeteries'))
                ->with('i', (request()->input('page', 1) - 1) * 10);

        }else{

            return redirect()->route('home');

        }
    }

    public function create()
    {
        if(session()->get('create_cemetery')) {

            $data['countries']  = Country::all();
            $data['states']     = State::all();

            return view('cemetery.cemetery-create',$data);

        }else{

            return redirect()->route('home');

        }
    }

    public function store(Request $request)
    {
        request()->validate([
            'code'  =>  'required|unique:cemeteries',
            'name'  =>  'required',
            'email' =>  'nullable|email'
        ]);

        $default_cemetery = Cemetery::where("is_default_cemetery", TRUE);
            
        if($default_cemetery->count()>0 && $request->is_default_cemetery==1){

            return redirect()->route('cemeteries.create')
                ->with('error','Default Cemetery already Available');

        }

        Cemetery::create($request->all());

        return redirect()->route('cemeteries.index')
            ->with('success', 'Record Added Successfully.');
    }

    public function show($id)
    {
        if(session()->get('read_cemetery')) {

            $cemetery = Cemetery::find($id);

            $data['countries']  = Country::all();
            $data['states']     = State::all();

            return view('cemetery.cemetery-view',compact('cemetery'),$data);

        }else{

            return redirect()->route('home');

        }
    }

    public function edit($id)
    {
        if(session()->get('update_cemetery')) {

            $cemetery = Cemetery::find($id);

            $data['countries']  = Country::all();
            $data['states']     = State::where("country_id", $cemetery->country_id)->get();

            return view('cemetery.cemetery-edit',compact('cemetery'),$data);

        }else{

            return redirect()->route('home');

        }
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'name'  =>  'required',
            'email' =>  'nullable|email'
        ]);

        DB::beginTransaction();

        try {

            $cemetery   =   Cemetery::find($id);

            if($cemetery->updated_at != $request->updated_at){

                DB::rollback();

                return redirect()->route('cemeteries.edit', $id)
                    ->with('error','Record has been modified by another user. Please try again.');

            }

            $default_cemetery = Cemetery::where("is_default_cemetery", TRUE)->where('id','!=',$id);
            
            if($default_cemetery->count()>0 && $request->is_default_cemetery==1){

                return redirect()->route('cemeteries.edit', $id)
                    ->with('error','Default Cemetery already Available');

            }

            // Update Cemetery Table
            $cemetery->update($request->all());

            DB::commit();

            return redirect()->route('cemeteries.show', $id)
                ->with('success','Record Updated Successfully');

        } catch (\Exception $e) {

            DB::rollback();

            return redirect()->route('cemeteries.edit', $id)
                ->with('error','Error');

        }
        
    }

    public function destroy($id)
    {
        $cemetery = Cemetery::find($id);

        $lp_invoices    =   DB::table('lp_invoices')->where('cemetery',$cemetery->code)->get();

        if($cemetery->users()->exists()){
            return redirect()->route('cemeteries.index')
                ->with('error', 'This Cemetery is used in Users as Default Cemetery.');
        }else if($cemetery->cemeteryItems()->exists()){
            return redirect()->route('cemeteries.index')
                ->with('error', 'This Cemetery is used in Item Loyalty.');
        }else if($cemetery->specialOffers()->exists()){
            return redirect()->route('cemeteries.index')
                ->with('error', 'This Cemetery is used in Special Offers.');
        }else if($lp_invoices->count()>0){
            return redirect()->route('cemeteries.index')
                ->with('error', 'This Cemetery has been used in Transactions.');
        }else{
            $status = $cemetery->delete();

            if ($status) {
                return redirect()->route('cemeteries.index')
                ->with('success', 'Record Deleted Successfully');
            } else {
                return redirect()->route('cemeteries.index')
                ->with('error', 'Error');
            }
        }
    }

    public function getStatesByCountry($country)
    {
        $states = State::where("country_id", $country)->pluck("state_name","id");

        return json_encode($states);
    }

    public function searchCemeteries(Request $request)
    {
        if(session()->get('read_cemetery')) {

            $search_value           =   $request->input('search_value');
            $data['search_value']   =   $search_value;

            $query = Cemetery::select('*');

            if (isset($search_value) && $search_value != "")
                $query->orWhere(DB::raw("LOWER(code)"), 'LIKE', '%'.strtolower($search_value).'%')
                ->orWhere(DB::raw("LOWER(name)"), 'LIKE', '%'.strtolower($search_value).'%')
                ->orWhere(DB::raw("LOWER(phone)"), 'LIKE', '%'.strtolower($search_value).'%')
                ->orWhere(DB::raw("LOWER(email)"), 'LIKE', '%'.strtolower($search_value).'%'); 

            $cemeteries = $query->latest()->paginate(10)->appends(['search_value' => $search_value]);    

            return view('cemetery.cemetery-search2',compact('cemeteries'), $data)
                ->with('i', (request()->input('page', 1) - 1) * 10);

        }else{

            return redirect()->route('home');

        }
    }

}
