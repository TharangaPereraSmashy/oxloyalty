<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Type;
use App\UserProfile;
use App\Cemetery;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\FuneralDirector;
use Illuminate\Support\Facades\DB;

class RegisterUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(session()->get('read_register')) {

            $users  =   User::latest()->paginate(5);

            return view('users.register_user', compact('users'))
                ->with('i', (request()->input('page', 1) - 1) * 5);

        } else {

            return redirect()->route('home');

        }
    }

    public function create()
    {
        if(session()->get('create_register')) {

            $types      =   Type::all('description', 'id');
            $profiles   =   UserProfile::all('description', 'id');
            $cemeteries =   Cemetery::all('name', 'id');
            $fdirectors =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as name"))->orderBy('code', 'asc')->get();
            return view('users/add_user', compact('profiles', 'types', 'cemeteries','fdirectors'));

        } else {

            return redirect()->route('home');

        }
    }

    public function store(Request $request)
    {
        $user = $this->validate(request(), [
            'name'      =>  'required|max:255',
            'email'     =>  'required|string|email|max:255|unique:users',
            'password'  =>  'required|string|min:6',
            'phone'     =>  'max:20',
            'type'      =>  'required',
            'profile'   =>  'required',
            'fdaccount' =>  'required_if:type,==,3'
        ], 
        [
            'fdaccount.required_if' => 'The FD Account field is required when type is Funeral Director.'
        ]);

        $user                   =   new User();
        $user->name             =   $request->get('name');
        $user->email            =   $request->get('email');
        $user->password         =   Hash::make($request->get('password'));
        $user->phone            =   $request->get('phone');
        $user->status           =   $request->get('status');
        $user->type             =   $request->get('type');
        $user->profile          =   $request->get('profile');
        $user->fd_id            =   $request->get('fdaccount');
        $user->default_cemetery =   $request->get('default_cemetery');
        $user->remember_token   =   str_random(10);
        $user->save();

        return redirect('registeruser')->with('success', 'Information has been added');
    }

    public function show($id)
    {
        if(session()->get('read_register')) {

            $user               =   User::find($id);
            $data['types']      =   Type::all('description','id');
            $data['profiles']   =   UserProfile::all('description','id');
            $data['cemeteries'] =   Cemetery::all('name','id');
            $data['fdirectors'] =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as name"))->orderBy('code', 'asc')->get();

            return view('users.view_user',compact('user'),  $data);

        }else{

            return redirect()->route('home');

        }
    }

    public function edit($id)
    {
        if(session()->get('update_register')) {

            $user               =   User::select('users.*', 'types.description AS type_name', 'user_profiles.description AS profile_name')
                                    ->join('types', 'users.type', '=', 'types.id')
                                    ->join('user_profiles', 'users.profile', '=', 'user_profiles.id')
                                    ->find($id);
            $data['types']      =   Type::all('description','id');
            $data['profiles']   =   UserProfile::all('description','id');
            $data['cemeteries'] =   Cemetery::all('name','id');
            $data['fdirectors'] =   FuneralDirector::select('id','code', DB::raw("CONCAT(code,' - ',name) as name"))->orderBy('code', 'asc')->get();

            return view('users.edit_user',compact('user'),  $data);

        }else{

            return redirect()->route('home');
            
        }
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'name'      =>  'required|max:255',
            //'password'  =>  'required|string|min:6',
            'phone'     =>  'max:20',
            'type'      =>  'required',
            'profile'   =>  'required',
            'fdaccount' =>  'required_if:type,==,3',
            'email'     =>  [
                'required',
                'email',
                 Rule::unique('users')->ignore($request->get('id'), 'id')
            ],
        ], 
        [
            'fdaccount.required_if' => 'The FD Account field is required when type is Funeral Director.'
        ]);

        $user                   =   User::find($id);
        $user->name             =   $request->get('name');
        $user->email            =   $request->get('email');
        //$user->password         =   Hash::make($request->get('password'));
        $user->phone            =   $request->get('phone');
        $user->status           =   $request->get('status');
        $user->type             =   $request->get('type');
        $user->profile          =   $request->get('profile');
        $user->default_cemetery =   $request->get('default_cemetery');
        $user->fd_id            =   $request->get('fdaccount');

        if($user->save()) {

            return redirect()->route('registeruser.show',$id)
                ->with('success','Record Updated Successfully');

        } else {

            return redirect()->route('registeruser.index')
                ->with('error','Error');

        }
    }

    public function destroy($id)
    {
        $user   =   User::find($id);

        if($user->delete()){

            return redirect()->route('registeruser.index')
                ->with('success','Record Deleted Successfully');

        } else {

            return redirect()->route('registeruser.index')
                ->with('error','Error');

        }
    }

    public function getProfilesByType($type)
    {
        $profiles = UserProfile::where("type", $type)->pluck("description","id");

        return json_encode($profiles);
    }

}
