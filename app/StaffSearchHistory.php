<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffSearchHistory extends Model
{
    protected $table = 'staff_search_history';

    protected $fillable = [
        'user_id',
        'search_type',
        'fd_id',
        'cemetery_id',
        'item_id',
        'searched_date'
    ];
}
